#include <benchmark/benchmark.h>

#include <cmath>
#include <math/math.h>

using namespace asd;

static void cos_std(benchmark::State& state) {
    float x = 1.0f;

    for (auto _ : state) {
        benchmark::DoNotOptimize(x = std::cos(x));
    }
}

static void cos_asd(benchmark::State& state) {
    float x = 1.0f;

    for (auto _ : state) {
        benchmark::DoNotOptimize(x = math::cos(x));
    }
}

static void cos_asd_fast(benchmark::State& state) {
    float x = 1.0f;

    for (auto _ : state) {
        benchmark::DoNotOptimize(x = math::fast::cos(x));
    }
}

static void sin_std(benchmark::State& state) {
    float x = 1.0f;

    for (auto _ : state) {
        benchmark::DoNotOptimize(x = std::sin(x));
    }
}

static void sin_asd(benchmark::State& state) {
    float x = 1.0f;

    for (auto _ : state) {
        benchmark::DoNotOptimize(x = math::sin(x));
    }
}

static void sin_asd_fast(benchmark::State& state) {
    float x = 1.0f;

    for (auto _ : state) {
        benchmark::DoNotOptimize(x = math::fast::sin(x));
    }
}

static void sincos_std(benchmark::State& state) {
    float x = 1.0f;

    for (auto _ : state) {
        auto y = std::make_pair(std::sin(x), std::cos(x));
        benchmark::DoNotOptimize(x = y.first + y.second);
    }
}

static void sincos_asd(benchmark::State& state) {
    float x = 1.0f;

    for (auto _ : state) {
        auto y = math::sincos(x);
        benchmark::DoNotOptimize(x = y.first + y.second);
    }
}

static void sincos_asd_fast(benchmark::State& state) {
    float x = 1.0f;

    for (auto _ : state) {
        auto y = math::fast::sincos(x);
        benchmark::DoNotOptimize(x = y.first + y.second);
    }
}

BENCHMARK(cos_std);
BENCHMARK(cos_asd);
BENCHMARK(cos_asd_fast);
BENCHMARK(sin_std);
BENCHMARK(sin_asd);
BENCHMARK(sin_asd_fast);
BENCHMARK(sincos_std);
BENCHMARK(sincos_asd);
BENCHMARK(sincos_asd_fast);

BENCHMARK_MAIN();
