#include <benchmark/benchmark.h>

#include <cmath>
#include <math/math.h>

#include <xmmintrin.h>

// http://www.lomont.org/papers/2003/InvSqrt.pdf
static inline float rsqrt(float x) noexcept {
    float xhalf = 0.5f * x;
    int i = *reinterpret_cast<int *>(&x);   // get bits for floating value
    i = 0x5f3759df - (i >> 1);              // gives initial guess y0
    x = *reinterpret_cast<float *>(&i);     // convert bits back to float

    return x * (1.5f - xhalf * x * x);      // Newton step, repeating increases accuracy
}

static void square_root_std(benchmark::State& state) {
    float x = 0.5f;
    
    for (auto _ : state) {
        benchmark::DoNotOptimize(x = std::sqrtf(x));
    }
}

static void square_root_old(benchmark::State& state) {
    float x = 0.5f;
    
    for (auto _ : state) {
        benchmark::DoNotOptimize(x = x * rsqrt(x));
    }
}

static void square_root_asd(benchmark::State& state) {
    float x = 0.5f;
    
    for (auto _ : state) {
        benchmark::DoNotOptimize(x = asd::math::sqrt(x));
    }
}

static void square_root_asd_fast(benchmark::State& state) {
    float x = 0.5f;

    for (auto _ : state) {
        benchmark::DoNotOptimize(x = asd::math::fast::sqrt(x));
    }
}

static void square_root_simd(benchmark::State& state) {
    float x = 0.5f;
    
    for (auto _ : state) {
        benchmark::DoNotOptimize(x = x * _mm_cvtss_f32(_mm_rsqrt_ss(_mm_load_ss(&x))));
    }
}

static void inv_square_root_std(benchmark::State& state) {
    float x = 0.5f;
    
    for (auto _ : state) {
        benchmark::DoNotOptimize(x = 1.0f / std::sqrtf(x));
    }
}

static void inv_square_root_old(benchmark::State& state) {
    float x = 0.5f;
    
    for (auto _ : state) {
        benchmark::DoNotOptimize(x = rsqrt(x));
    }
}

static void inv_square_root_asd(benchmark::State& state) {
    float x = 0.5f;
    
    for (auto _ : state) {
        benchmark::DoNotOptimize(x = asd::math::rsqrt(x));
    }
}

static void inv_square_root_asd_fast(benchmark::State& state) {
    float x = 0.5f;

    for (auto _ : state) {
        benchmark::DoNotOptimize(x = asd::math::fast::rsqrt(x));
    }
}

static void inv_square_root_simd(benchmark::State& state) {
    float x = 0.5f;
    
    for (auto _ : state) {
        benchmark::DoNotOptimize(x = _mm_cvtss_f32(_mm_rsqrt_ss(_mm_load_ss(&x))));
    }
}

BENCHMARK(square_root_std);
BENCHMARK(square_root_old);
BENCHMARK(square_root_asd);
BENCHMARK(square_root_asd_fast);
BENCHMARK(square_root_simd);

BENCHMARK(inv_square_root_std);
BENCHMARK(inv_square_root_old);
BENCHMARK(inv_square_root_asd);
BENCHMARK(inv_square_root_asd_fast);
BENCHMARK(inv_square_root_simd);

BENCHMARK_MAIN();