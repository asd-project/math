//---------------------------------------------------------------------------

#define CATCH_CONFIG_RUNNER
#include <catch2/catch.hpp>

#include <launch/main.h>
#include <math/point.h>
#include <math/vector.h>
#include <math/io.h>

#include <sstream>

//---------------------------------------------------------------------------

namespace asd
{
    template <class T, class E>
    class fuzzy_equal_check : public Catch::MatcherBase<T> {
        T sample;
        E eps;

    public:
        fuzzy_equal_check(const T & sample, E eps) : sample(sample), eps(eps) {}

        bool match(const T & v) const override {
            return math::fuzzy_eq(sample, v, eps);
        }

        std::string describe() const override {
            std::ostringstream ss;
            ss << "~= " << sample << " (eps: " << eps << ")";
            return ss.str();
        }
    };

    template <class T, class E = float> requires requires (T v, E eps) { { math::fuzzy_eq(v, v, eps) }; }
    fuzzy_equal_check<T, E> is_almost(const T & v, E eps = math::eps) {
        return {v, eps};
    }

    TEST_CASE("Point comparisons", "[points]") {
        constexpr math::float_point_xy xy{1.0f, 2.0f};
        constexpr math::float_point_xz xz{1.0f, 3.0f};
        constexpr math::float_point_xyz xyz{1.0f, 2.0f, 3.0f};
        math::float_vector v{1.0f, 2.0f, 3.0f, 4.0f};

        static_assert(math::eq(xy[0], 1.0f));
        static_assert(math::eq(xy[1], 2.0f));
        static_assert(math::eq(xz[0], 1.0f));
        static_assert(math::eq(xz[1], 3.0f));
        static_assert(math::eq(xyz[0], 1.0f));
        static_assert(math::eq(xyz[1], 2.0f));
        static_assert(math::eq(xyz[2], 3.0f));
        static_assert(xy == math::float_point_xy{1.0f, 2.0f});
        static_assert(xz == math::float_point_xz{1.0f, 3.0f});
        static_assert(xyz == math::float_point_xyz{1.0f, 2.0f, 3.0f});

        REQUIRE(xy == v.xy);
        REQUIRE(xz == v.xz);
        REQUIRE(xyz == v.xyz);
    }

    TEST_CASE("Point operations", "[points]") {
        constexpr math::float_point_xyz a{1.0f, 2.0f, 3.0f};
        constexpr math::float_point_xyz b{0.0f, 1.0f, 2.0f};

        static_assert(math::eq(a - b, {1.0f, 1.0f, 1.0f}));
        static_assert(math::eq(a + b, {1.0f, 3.0f, 5.0f}));
        static_assert(math::eq(a * b, {0.0f, 2.0f, 6.0f}));
        static_assert(math::eq(3 * a, {3.0f, 6.0f, 9.0f}));
        static_assert(math::eq(a / 2, {0.5f, 1.0f, 1.5f}));
        static_assert(math::eq(math::normalize(b), {0.0f, 1.0f / math::sqrt(5.0f), 2.0f / math::sqrt(5.0f)}));

        // division by zero is detected in constexpr context
        // static_assert(a / b == math::float_point_xyz{math::inf, 2.0f, 1.5f});
        // static_assert(a / 0 == math::float_point_xyz{math::inf, math::inf, math::inf});

        REQUIRE(a / b == math::float_point_xyz{math::inf, 2.0f, 1.5f});
        REQUIRE(a / 0 == math::float_point_xyz{math::inf, math::inf, math::inf});
    }

    TEST_CASE("Vector operations", "[vectors]") {
        {
            constexpr math::int_vector a{-256, 0, -192};
            constexpr math::int_vector b{-256, 0, -256};

            REQUIRE(math::floor_div(a, 64) == math::int_vector{-4, 0, -3});
            REQUIRE(math::floor_div(b, 64) == math::int_vector{-4, 0, -4});
        }
        {
            const auto x = GENERATE(take(4, random(0.0f, 100000.0f)));
            const auto y = GENERATE(take(3, random(0.0f, 100000.0f)));
            const auto z = GENERATE(take(2, random(0.0f, 100000.0f)));
            const auto w = GENERATE(take(1, random(0.0f, 100000.0f)));

            const math::vector a{x, y, z, w};

            REQUIRE_THAT(math::sqrt(a), is_almost(a.map(math::op::sqrt)));
            REQUIRE_THAT(math::sqr(a), is_almost(a.map(math::op::sqr)));
        }
    }

    TEST_CASE("Vector trigonometry", "[vectors]") {
        constexpr math::vector a{math::pi * 0.125f, -math::pi * 0.5f, math::pi * 0.667f, 0.0f};

        REQUIRE_THAT(math::sin(a), is_almost(a.map(math::op::sin)));
        REQUIRE_THAT(math::cos(a), is_almost(a.map(math::op::cos)));

        const auto [sine, cosine] = math::sincos(a);
        REQUIRE_THAT(sine, is_almost(a.map(math::op::sin)));
        REQUIRE_THAT(cosine, is_almost(a.map(math::op::cos)));
    }

    TEST_CASE("Quaternion conversions", "[quaternions]") {
        {
            constexpr math::float_vector euler = {math::pi * 0.125f, 0, 0};
            const auto q1 = math::float_quaternion::from_euler(euler);

            REQUIRE_THAT(math::euler_angles(q1), is_almost(euler));
            REQUIRE_THAT(q1, is_almost(math::float_quaternion::from_pitch(math::pi * 0.125f)));
        }
        {
            constexpr math::float_vector euler = {0, math::pi * 0.125f, 0};
            const auto q2 = math::float_quaternion::from_euler(euler);

            REQUIRE_THAT(math::euler_angles(q2), is_almost(euler));
            REQUIRE_THAT(q2, is_almost(math::float_quaternion::from_yaw(math::pi * 0.125f)));
        }
        {
            constexpr math::float_vector euler = {0, 0, math::pi * 0.125f};
            const auto q3 = math::float_quaternion::from_euler(euler);

            REQUIRE_THAT(math::euler_angles(q3), is_almost(euler));
            REQUIRE_THAT(q3, is_almost(math::float_quaternion::from_roll(math::pi * 0.125f)));
        }
        {
            const math::local_float_vector euler = {math::pi * 0.125f, -math::pi * 0.49f, 0};
            const auto q4 = math::float_quaternion::from_euler(euler);
            const auto q4a = math::float_quaternion::from_roll(euler.z) * math::float_quaternion::from_yaw(euler.y) * math::float_quaternion::from_pitch(euler.x);

            INFO(euler);
            INFO(q4);
            INFO(q4a);

            CHECK_THAT(q4, is_almost(q4a));
            CHECK_THAT(math::euler_angles(q4), is_almost(math::euler_angles(q4a)));

            CHECK_THAT(math::euler_angles(q4), is_almost(euler, 1.0e-5f));
            CHECK_THAT(math::euler_angles(q4a), is_almost(euler, 1.0e-5f));
        }
    }

    static entrance open([](int argc, char * argv[]) {
        Catch::Session session;

        if (int return_code = session.applyCommandLine(argc, argv); return_code != 0) {
            return return_code;
        }

        return session.run();
    });
}

//---------------------------------------------------------------------------
