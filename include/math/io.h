//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <math/point.h>
#include <math/size.h>
#include <math/rect.h>
#include <math/vector.h>
#include <math/matrix.h>
#include <math/quaternion.h>

#if __has_include(<spdlog/fmt/ostr.h>)
    #include <spdlog/fmt/ostr.h>
#endif

//---------------------------------------------------------------------------

namespace asd
{
    namespace math
    {
        template <class T>
        struct term
        {
            T && value;
            const char * suffix;
            int sign = 1;

            constexpr bool operator >= (int v) const {
                return value >= v;
            }

            constexpr auto operator - () const {
                return term{std::forward<T>(value), suffix, -sign};
            }

            template <class OutputStream>
            friend OutputStream & operator << (OutputStream & os, const term & term) {
                if (term.sign < 0) {
                    return os << -term.value << term.suffix;
                } else {
                    return os << term.value << term.suffix;
                }
            }
        };

        template <class A, class B>
        struct complex_term
        {
            A a;
            B b;
            int sign = 1;

            constexpr bool operator >= (int) const {
                return sign >= 0;
            }

            constexpr auto operator - () const {
                return complex_term{a, b, -sign};
            }

            template <class OutputStream>
            friend OutputStream & operator << (OutputStream & os, const complex_term & term) {
                if (term.b >= 0) {
                    return os << term.a << " + " << term.b;
                } else {
                    return os << term.a << " - " << -term.b;
                }
            }
        };

        template <class A, class B>
        auto operator + (const term<A> & a, const term<B> & b) noexcept {
            return complex_term<term<A>, term<B>>{a, b};
        }

        template <class A, class B, class C>
        auto operator + (const complex_term<A, B> & ab, const term<C> & c) {
            return complex_term<complex_term<A, B>, term<C>>{ab, c};
        }

        template <class A, class B, class C>
        auto operator + (const term<A> & a, const complex_term<B, C> & bc) {
            return complex_term<term<A>, complex_term<B, C>>{a, bc};
        }

        template <class OutputStream, class T>
        OutputStream & operator << (OutputStream & os, const point<T, point_model_xy> & pt) {
            return os << "(" << term(pt.x, "x") + term(pt.y, "y") << ")";
        }

        template <class OutputStream, class T>
        OutputStream & operator << (OutputStream & os, const point<T, point_model_xz> & pt) {
            return os << "(" << term(pt.x, "x") + term(pt.z, "z") << ")";
        }

        template <class OutputStream, class T>
        OutputStream & operator << (OutputStream & os, const point<T, point_model_xyz> & pt) {
            return os << "(" << term(pt.x, "x") + term(pt.y, "y") + term(pt.z, "z") << ")";
        }

        template <class OutputStream, class T>
        OutputStream & operator << (OutputStream & os, const size<T, point_model_xy> & pt) {
            return os << "(" << term(pt.x, "x") << "*" << term(pt.y, "y") << ")";
        }

        template <class OutputStream, class T>
        OutputStream & operator << (OutputStream & os, const size<T, point_model_xz> & pt) {
            return os << "(" << term(pt.x, "x") << "*" << term(pt.z, "z") << ")";
        }

        template <class OutputStream, class T>
        OutputStream & operator << (OutputStream & os, const size<T, point_model_xyz> & pt) {
            return os << "(" << term(pt.x, "x") << "*" << term(pt.y, "y") << "*" << term(pt.z, "z") << ")";
        }

        template <class OutputStream, class T>
        OutputStream & operator << (OutputStream & os, const rect<T, point_model_xy> & r) {
            return os << "rect(" << term(r.left, "x") + term(r.top, "y") << " + " << term(r.width(), "x") << "*" << term(r.height(), "y") << ")";
        }

        template <class OutputStream, class T>
        OutputStream & operator << (OutputStream & os, const rect<T, point_model_xz> & r) {
            return os << "rect(" << term(r.left, "x") + term(r.top, "z") << " + " << term(r.width(), "x") << "*" << term(r.height(), "z") << ")";
        }

        template <class OutputStream, class T, class S>
        OutputStream & operator << (OutputStream & os, const vector<T, S> & v) {
            return os << "(" << term(v.x, "x") + term(v.y, "y") + term(v.z, "z") + term(v.w, "") << ")";
        }

        template <class OutputStream, class T, class S>
        OutputStream & operator << (OutputStream & os, const quaternion<T, S> & q) {
            return os << "(" << term(q.x, "i") + term(q.y, "j") + term(q.z, "k") + term(q.w, "") << ")";
        }

        template <class OutputStream, class T, class S>
        OutputStream & operator << (OutputStream & out, const math::matrix<T, S> & mat) {
            return out <<
                "| " << std::setw(6) << mat(0, 0) << " " << std::setw(6) << mat(0, 1) << " " << std::setw(6) << mat(0, 2) << " " << std::setw(6) << mat(0, 3) << " |\n" <<
                "| " << std::setw(6) << mat(1, 0) << " " << std::setw(6) << mat(1, 1) << " " << std::setw(6) << mat(1, 2) << " " << std::setw(6) << mat(1, 3) << " |\n" <<
                "| " << std::setw(6) << mat(2, 0) << " " << std::setw(6) << mat(2, 1) << " " << std::setw(6) << mat(2, 2) << " " << std::setw(6) << mat(2, 3) << " |\n" <<
                "| " << std::setw(6) << mat(3, 0) << " " << std::setw(6) << mat(3, 1) << " " << std::setw(6) << mat(3, 2) << " " << std::setw(6) << mat(3, 3) << " |";
        }
    }
}
