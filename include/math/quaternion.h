//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <math/math.h>
#include <simd/simd.h>

#include "matrix.h"

//---------------------------------------------------------------------------

namespace asd::math
{
    template <typename T, typename VectorStorage = default_vector_storage<T>>
    struct alignas(sizeof(T) * 4) quaternion : aligned_alloc
    {
        using underlying_type = typename simd<T, 4>::type;
        using vector_type = vector<T, VectorStorage>;
        using vector_constants = math::vector_constants<T>;
        using constants = math::constants<T>;

        union
        {
            struct
            {
                T x, y, z, w;
            };

            std::array<T, 4> q;
            vector_type v;
        };

        member_cast(q, std::array<T, 4>);

        constexpr quaternion() noexcept : v(vector_constants::positive_w) {}

        constexpr quaternion(const quaternion & q) noexcept : v(q.v) {}

        template <class U, class S> requires(!std::is_same_v<quaternion, quaternion<U, S>>)
        constexpr quaternion(const quaternion<U, S> & q) : v(q.v) {}

        constexpr quaternion(T x, T y, T z, T w) noexcept : v{x, y, z, w} {}

        template <class S>
        explicit constexpr quaternion(const vector<T, S> & v) noexcept : v{v} {}

        template <class U>
            requires tag_convertible<U, quaternion>
        explicit constexpr quaternion(const U & v) noexcept(noexcept(convert<quaternion>(v))) : v(convert<quaternion>(v).v) {}

        //  [sx sy sz c]
        constexpr quaternion(const vector_type & axis, T angle) :
            quaternion(math::shuffle<0, 0, 0, 1>(math::trigon(angle * 0.5f)) * math::blend<0, 0, 0, 1>(axis, vector_constants::positive_w)) {}
        //                         [ s  s  s  c ]                                        [ x  y  z  1 ]

        constexpr quaternion & operator = (const quaternion & q) noexcept {
            v = q.v;
            return *this;
        }

        template <class U>
            requires tag_convertible<U, quaternion>
        constexpr quaternion & operator = (const U & v) noexcept {
            return *this = convert<quaternion>(v);
        }

        constexpr underlying_type simd() const {
            return v;
        }

        constexpr bool operator == (const quaternion & q) const noexcept {
            return v == q.v;
        }

        constexpr bool operator != (const quaternion & q) const noexcept {
            return v != q.v;
        }

        constexpr quaternion operator + () const noexcept {
            return *this;
        }

        constexpr quaternion operator - () const noexcept {
            return quaternion{-v};
        }

        constexpr quaternion & assign(T x, T y, T z, T w) noexcept {
            v.assign(x, y, z, w);
            return *this;
        }

        constexpr quaternion negation() const noexcept {
            return quaternion{-v};
        }

        constexpr quaternion conjugation() const noexcept {
            return quaternion{math::negate<1, 1, 1, 0>(v)};
        }

        constexpr quaternion & invert() noexcept {
            return *this = inverse();
        }

        constexpr quaternion inverse() const noexcept {
            return conjugation().downscale(magnitude());
        }

        constexpr quaternion & operator += (const quaternion & q) noexcept {
            v += q.v;
            return *this;
        }

        constexpr quaternion & operator -= (const quaternion & q) noexcept {
            v -= q.v;
            return *this;
        }

        constexpr quaternion & rotate_by(const quaternion & q) noexcept {
            return *this = q * *this;
        }

        constexpr vector_type apply_to(const vector_type & d) const noexcept {
            const auto xyz = math::clear_w(v);
            const auto t = 2 * math::cross(xyz, d);
            return d + w * t + math::cross(xyz, t);
        }

        constexpr vector_type right() const noexcept {
            return apply_to(vector_constants::right);
        }

        constexpr vector_type up() const noexcept {
            return apply_to(vector_constants::up);
        }

        constexpr vector_type forward() const noexcept {
            return apply_to(vector_constants::forward);
        }

        constexpr quaternion & rotate(const vector_type & axis, T angle) noexcept {
            return rotate_by({axis, angle});
        }

        constexpr quaternion & rotate_x(T angle) noexcept {
            return rotate(vector_constants::positive_x, angle);
        }

        constexpr quaternion & rotate_y(T angle) noexcept {
            return rotate(vector_constants::positive_y, angle);
        }

        constexpr quaternion & rotate_z(T angle) noexcept {
            return rotate(vector_constants::positive_z, angle);
        }

        constexpr quaternion & scale(T s) & noexcept {
            v *= s;
            return *this;
        }

        constexpr quaternion && scale(T s) && noexcept {
            v *= s;
            return std::move(*this);
        }

        constexpr quaternion & downscale(T s) & noexcept {
            v /= s;
            return *this;
        }

        constexpr quaternion && downscale(T s) && noexcept {
            v /= s;
            return std::move(*this);
        }

        constexpr T & operator [](size_t index) {
            return q[index];
        }

        constexpr const T & operator [](size_t index) const {
            return q[index];
        }

        constexpr T norm() const noexcept {
            return math::sum(math::sqr(v));
        }

        constexpr T magnitude() const noexcept {
            return math::sqrt(this->norm());
        }

        // pitch (x), radians
        static constexpr auto from_pitch(T pitch) noexcept {
            auto [sine, cosine] = math::sincos(pitch * 0.5f);
            return quaternion<T, aligned_vector_storage<T>>{sine, 0, 0, cosine};
        }

        // yaw (y), radians
        static constexpr auto from_yaw(T yaw) noexcept {
            auto [sine, cosine] = math::sincos(yaw * 0.5f);
            return quaternion<T, aligned_vector_storage<T>>{0, sine, 0, cosine};
        }

        // roll (z), radians
        static constexpr auto from_roll(T roll) noexcept {
            auto [sine, cosine] = math::sincos(roll * 0.5f);
            return quaternion<T, aligned_vector_storage<T>>{0, 0, sine, cosine};
        }

        // pitch (x), yaw (y), roll (z), radians
        // result is equivalent to quaternion::from_roll(angles[2]) * quaternion::from_yaw(angles[1]) * quaternion::from_pitch(angles[0])
        static constexpr auto from_euler(const vector_type & angles) noexcept {
            // algorithm is adapted from https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles#Euler_angles_to_quaternion_conversion

            auto halfs = angles * vector_constants::half;

            // generate sines and cosines simultaneously
            auto [sine, cosine] = math::sincos(halfs);

            // p - angle between rotation axis and x
            // y - angle between rotation axis and y
            // r - angle between rotation axis and z
            //                     sp sp cp cp
            auto p = math::shuffle<0, 0, 0, 0>(sine, cosine);
            //                     sy sy cy cy
            auto y = math::shuffle<1, 1, 1, 1>(sine, cosine);
            //                     sr sr cr cr
            auto r = math::shuffle<2, 2, 2, 2>(sine, cosine);

            // multiply and add corresponding components
            // x and z are subtracted

            return quaternion<T, aligned_vector_storage<T>>{
                //                                     sp cp cp cp                    cy sy cy cy                    cr cr sr cr
                /*                    */ math::shuffle<0, 2, 2, 2>(p) * math::shuffle<2, 0, 2, 2>(y) * math::shuffle<2, 2, 0, 2>(r) +
                //                                     cp sp sp sp                    sy cy sy sy                    sr sr cr sr
                math::negate<1, 0, 1, 0>(math::shuffle<2, 0, 0, 0>(p) * math::shuffle<0, 2, 0, 0>(y) * math::shuffle<0, 0, 2, 0>(r))
            };
        }

        // 'from' and 'to' are unit vectors
        static constexpr auto from_vectors(const vector_type & from, const vector_type & to)noexcept {
            auto d = math::dot_vector(from, to);

            if (d.x >= 1.0f) { // 0 degrees, no rotation
                return quaternion<T, aligned_vector_storage<T>>{};
            }

            if (d.x < -1.0f + constants::eps) { // 180 degrees, find any acceptable orthogonal vector to represent a rotation
                auto axis = math::cross(vector_constants::positive_x, from);

                if (math::square_magnitude(axis) < constants::eps2) {
                    axis = math::cross(vector_constants::positive_y, from);
                }

                return quaternion<T, aligned_vector_storage<T>>{math::unit(axis), constants::pi};
            }

            return quaternion<T, aligned_vector_storage<T>>{math::unit(math::blend<0, 0, 0, 1>(math::cross(from, to), vector_constants::one + d))}; // orthogonal vector [x y z] and rotation angle [w]
        }
    };

    template <class T>
    using aligned_quaternion = quaternion<T, aligned_vector_storage<T>>;

    using float_quaternion = quaternion<float>;
    using double_quaternion = quaternion<double>;

    // matrix conversion with CTAD
    template <class T, class S>
    matrix(const math::quaternion<T, S> &) -> aligned_matrix<T>;

    template <class T, class SA, class SB>
    constexpr math::matrix<T, SA> convert(meta::type<math::matrix<T, SA>>, const math::quaternion<T, SB> & q) noexcept {
        using vector_constants = math::vector_constants<T>;

        local_vector<T> v = q.v;

        auto s = -math::sqr(v);
        s = math::shuffle<1, 2, 0, 3>(s) + math::shuffle<2, 0, 1, 3>(s);
        auto a = math::shuffle<1, 2, 0, 3>(v) * math::shuffle<2, 0, 1, 3>(v);
        auto b = v * math::negate<1, 0, 0, 0>(math::spread_w(v));

        return {
            vector_constants::positive_x + vector_constants::two_xyz * blend<0, 1, 1, 0>(s, shuffle<2, 2, 1, 1>(a + negate<0, 0, 1, 0>(b))),    // 1.0f - 2.0f * (y * y + z * z),   2.0f * (x * y - w * z),         2.0f * (x * z + w * y),         0.0f
            vector_constants::positive_y + vector_constants::two_xyz * blend<1, 0, 1, 0>(s, shuffle<2, 2, 0, 0>(a + b)),                        // 2.0f * (x * y + w * z),          1.0f - 2.0f * (x * x + z * z),  2.0f * (y * z - w * x),         0.0f
            vector_constants::positive_z + vector_constants::two_xyz * shuffle<1, 0, 2, 3>(a - b, s),                                           // 2.0f * (x * z - w * y),          2.0f * (y * z + w * x),         1.0f - 2.0f * (x * x + y * y),  0.0f
            vector_constants::positive_w
        };
    }

    template <class T, class S>
    constexpr auto pitch(const quaternion<T, S> & q) noexcept {
        local_vector<T> v = q.v;
        auto s = math::sqr(v);

        return math::atan(2.0f * (v.y * v.z + v.w * v.x), s.w - s.x - s.y + s.z);
    }

    template <class T, class S>
    constexpr auto euler_angles(const quaternion<T, S> & q) noexcept {
        local_vector<T> v = q.v;
        auto s = math::sqr(v);

        T pitch = math::atan(2.0f * (v.w * v.x + v.y * v.z), s.w - s.x - s.y + s.z);
        T yaw = math::asin(2.0f * (v.w * v.y - v.x * v.z));
        T roll = math::atan(2.0f * (v.w * v.z + v.x * v.y), s.w + s.x - s.y - s.z);

        return local_vector<T>{pitch, yaw, roll};
    }

    template <class T, class S1, class S2>
    constexpr aligned_quaternion<T> operator + (const quaternion<T, S1> & q, const quaternion<T, S2> & p) {
        return q.v + p.v;
    }

    template <class T, class S1, class S2>
    constexpr aligned_quaternion<T> operator - (const quaternion<T, S1> & q, const quaternion<T, S2> & p) {
        return q.v - p.v;
    }

    template <class T, class S1, class S2>
    constexpr aligned_quaternion<T> operator * (const quaternion<T, S1> & q, const quaternion<T, S2> & p) {
        // q.w*p.x   q.w*p.y   q.w*p.z   q.w*p.w
        //    +         +         +         +
        // q.x*p.w   q.y*p.w   q.z*p.w  -q.x*p.x
        //    +         +         +         +
        // q.y*p.z   q.z*p.x   q.x*p.y  -q.y*p.y
        //    +         +         +         +
        //-q.z*p.y  -q.x*p.z  -q.y*p.x  -q.z*p.z

        local_vector<T> pv = p.v;
        local_vector<T> qv = q.v;

        return aligned_quaternion<T>{
                                        math::spread_w(qv) * pv                             +
            math::negate_w(math::shuffle<0, 1, 2, 0>(qv) * math::shuffle<3, 3, 3, 0>(pv)) +
            math::negate_w(math::shuffle<1, 2, 0, 1>(qv) * math::shuffle<2, 0, 1, 1>(pv)) -
                            math::shuffle<2, 0, 1, 2>(qv) * math::shuffle<1, 2, 0, 2>(pv)
        };
    }

    template <class T, class S1, class S2>
    constexpr aligned_vector<T> operator * (const quaternion<T, S2> & q, const vector<T, S1> & v) {
        return q.apply_to(v);
    }

    template <class T, class S1, class S2>
    constexpr aligned_matrix<T> operator * (const matrix<T, S1> & m, const quaternion<T, S2> & q) {
        return m * math::matrix(q);
    }

    template <class T, class S1, class S2>
    constexpr aligned_matrix<T> operator * (const quaternion<T, S2> & q, const matrix<T, S1> & m) {
        return math::matrix(q) * m;
    }

    template <class TA, class TB, class SA, class SB, class E = float>
    constexpr bool fuzzy_equal(const quaternion<TA, SA> & a, const quaternion<TB, SB> & b, E eps = math::eps) noexcept {
        return
            math::fuzzy_equal(a.x, b.x, eps) &&
            math::fuzzy_equal(a.y, b.y, eps) &&
            math::fuzzy_equal(a.z, b.z, eps) &&
            math::fuzzy_equal(a.w, b.w, eps);
    }

//---------------------------------------------------------------------------

    template
    struct quaternion<float>;

#ifdef ASD_SIMD_DOUBLE
    template
    struct quaternion<double>;
#endif
}
