//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <math/point.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace math
    {
        template <class T, class Model = point_model_xy>
        class size : public point_base<size, T, Model>
        {
        public:
            using base_type = point_base<size, T, Model>;
            using ratio_type = std::conditional_t<std::is_integral<T>::value, double, T>;

            static constexpr auto dimensions = Model::dimensions;

            using point_base<size, T, Model>::point_base;

            template <class U> requires(!std::is_same_v<T, U> && std::is_convertible_v<U, T>)
            explicit constexpr size(const size<U, Model> & sz) : base_type(sz) {}

            template <class U>
            explicit constexpr size(const swizzle_point<U, Model> & pt) noexcept :
                base_type{pt.point()} {}

            template <class U = ratio_type>
            constexpr U ratio() const {
                return U(this->data[0]) / U(this->data[1]);
            }

            template <class U = ratio_type>
            constexpr U inverted_ratio() const {
                return U(this->data[1]) / U(this->data[0]);
            }

            constexpr T area() const {
                return this->data[0] * this->data[1];
            }

            constexpr T & operator [](int axis) {
                return this->data[axis];
            }

            constexpr const T & operator [](int axis) const noexcept {
                return this->data[axis];
            }

            constexpr operator std::array<T, dimensions> & () & noexcept {
                return this->data;
            }

            constexpr operator const std::array<T, dimensions> & () const & noexcept {
                return this->data;
            }

            constexpr operator std::array<T, dimensions> && () && noexcept {
                return std::move(this->data);
            }

            template <class U>
            constexpr size & operator += (const size<U, Model> & pt) {
                return this->apply(math::op::add_inplace, pt);
            }

            template <math::scalar U>
            constexpr size & operator += (U value) {
                return this->apply(math::op::add_inplace, value);
            }

            template <class U>
            constexpr size & operator -= (const size<U, Model> & pt) {
                return this->apply(math::op::sub_inplace, pt);
            }

            template <math::scalar U>
            constexpr size & operator -= (U value) {
                return this->apply(math::op::sub_inplace, value);
            }

            template <class U>
            constexpr size & operator *= (const size<U, Model> & pt) {
                return this->apply(math::op::mul_inplace, pt);
            }

            template <math::scalar U>
            constexpr size & operator *= (U value) {
                return this->apply(math::op::mul_inplace, value);
            }

            template <class U>
            constexpr size & operator /= (const size<U, Model> & pt) {
                return this->apply(math::op::div_inplace, pt);
            }

            template <math::scalar U>
            constexpr size & operator /= (U value) {
                return this->apply(math::op::div_inplace, value);
            }

            template <class U> requires is_compatible_point_like_v<U, size>
            constexpr bool contains(const U & pt) const noexcept {
                return this->map_reduce(math::op::ge, math::op::bool_and, pt);
            }

            template <class U = T>
            friend constexpr bool operator > (const size & a, const size<U, Model> & b) noexcept {
                return a.compare(b) > 0;
            }

            template <class U = T>
            friend constexpr bool operator < (const size & a, const size<U, Model> & b) noexcept {
                return a.compare(b) < 0;
            }

            template <class U = T>
            friend constexpr bool operator >= (const size & a, const size<U, Model> & b) noexcept {
                return a.compare(b) >= 0;
            }

            template <class U = T>
            friend constexpr bool operator <= (const size & a, const size<U, Model> & b) noexcept {
                return a.compare(b) <= 0;
            }

            template <class U = T>
            friend constexpr auto operator + (const size & a, const size<U, Model> & b) noexcept {
                return a.map(math::op::add, b);
            }

            template <class U = T>
            friend constexpr auto operator - (const size & a, const size<U, Model> & b) noexcept {
                return a.map(math::op::sub, b);
            }

            template <class U = T>
            friend constexpr auto operator * (const size & a, const size<U, Model> & b) noexcept {
                return a.map(math::op::mul, b);
            }

            template <class U = T>
            friend constexpr auto operator / (const size & a, const size<U, Model> & b) noexcept {
                return a.map(math::op::div, b);
            }

            template <math::scalar U>
            friend constexpr auto operator + (const size & a, U b) noexcept {
                return a.map(math::op::add, b);
            }

            template <math::scalar U>
            friend constexpr auto operator - (const size & a, U b) noexcept {
                return a.map(math::op::sub, b);
            }

            template <math::scalar U>
            friend constexpr auto operator * (const size & a, U b) noexcept {
                return a.map(math::op::mul, b);
            }

            template <math::scalar U>
            friend constexpr auto operator / (const size & a, U b) noexcept {
                return a.map(math::op::div, b);
            }

            template <math::scalar U>
            friend constexpr auto operator + (U a, const size & b) noexcept {
                return b.map(math::op::add, a);
            }

            template <math::scalar U>
            friend constexpr auto operator - (U a, const size & b) noexcept {
                return b.map(math::op::sub_reverse, a);
            }

            template <math::scalar U>
            friend constexpr auto operator * (U a, const size & b) noexcept {
                return b.map(math::op::mul, a);
            }

            template <math::scalar U>
            friend constexpr auto operator / (U a, const size & b) noexcept {
                return b.map(math::op::div_reverse, a);
            }

            template <math::scalar U>
            friend constexpr auto operator + (const point<T, Model> & a, const size<U, Model> & b) noexcept {
                return a.map(math::op::add, b);
            }

            template <math::scalar U>
            friend constexpr auto operator - (const point<T, Model> & a, const size<U, Model> & b) noexcept {
                return a.map(math::op::sub, b);
            }

            template <math::scalar U>
            friend constexpr auto operator * (const point<T, Model> & a, const size<U, Model> & b) noexcept {
                return a.map(math::op::mul, b);
            }

            template <math::scalar U>
            friend constexpr auto operator / (const point<T, Model> & a, const size<U, Model> & b) noexcept {
                return a.map(math::op::div, b);
            }

            template <math::scalar U>
            friend constexpr auto operator + (const swizzle_point<T, Model> & a, const size<U, Model> & b) noexcept {
                return a.point() + b;
            }

            template <math::scalar U>
            friend constexpr auto operator - (const swizzle_point<T, Model> & a, const size<U, Model> & b) noexcept {
                return a.point() - b;
            }

            template <math::scalar U>
            friend constexpr auto operator * (const swizzle_point<T, Model> & a, const size<U, Model> & b) noexcept {
                return a.point() * b;
            }

            template <math::scalar U>
            friend constexpr auto operator / (const swizzle_point<T, Model> & a, const size<U, Model> & b) noexcept {
                return a.point() / b;
            }
        };

//---------------------------------------------------------------------------

        template <class T>
        size(T x) -> size<T, point_model_xy>;

        template <class T>
        size(T x, T y) -> size<T, point_model_xy>;

        template <class T>
        size(T x, T y, T z) -> size<T, point_model_xyz>;

        template <template <class, class> class PointLike, class T, class Model>
        size(const PointLike<T, Model> &) -> size<T, Model>;

        template <class T, class Model>
        size(const swizzle_point<T, Model> &) -> size<T, Model>;

//---------------------------------------------------------------------------

        template <class T>
        using size_xz = size<T, point_model_xz>;

        using byte_size = size<uint8>;
        using int_size = size<int32>;
        using uint_size = size<uint32>;
        using float_size = size<float>;

        using byte_size_xz = size_xz<uint8>;
        using int_size_xz = size_xz<int32>;
        using uint_size_xz = size_xz<uint32>;
        using float_size_xz = size_xz<float>;

//---------------------------------------------------------------------------

        template <class T, class U, class Model>
        constexpr auto floor_div(const point<T, Model> & a, const size<U, Model> & b) {
            return a.map(math::op::floor_div, b);
        }

        template <class T, class U, class Model>
        constexpr auto mod(const point<T, Model> & a, const size<U, Model> & b) noexcept {
            return a.map(math::op::mod, b);
        }

        template <class T, class U, class Model>
        constexpr auto floor_mod(const point<T, Model> & a, const size<U, Model> & b) noexcept {
            return a.map(math::op::floor_mod, b);
        }
    }
}
