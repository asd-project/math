//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <math/vector.h>
#include <math/quaternion.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace math
    {
        enum class plane_side
        {
            back = -1,
            both = 0,
            front = 1
        };

        template <class T>
        struct plane
        {
            using vector_type = math::vector<T>;
            using aligned_vector_type = math::aligned_vector<T>;

            plane() noexcept : equation(vector_constants<T>::positive_z) {}

            plane(const plane & plane) noexcept = default;

            template <class S = aligned_vector_storage<T>>
            plane(const vector<T, S> & equation) noexcept :
                equation(equation)
            {}

            template <class S = aligned_vector_storage<T>>
            plane(const vector<T, S> & normal, T distance) noexcept :
                equation(normal, distance)
            {}

            template <class S1 = aligned_vector_storage<T>, class S2 = S1, class S3 = S1>
            plane(const vector<T, S1> & a, const vector<T, S2> & b, const vector<T, S3> & c) noexcept :
                equation(math::normalize(math::cross(b - a, c - a)))
            {
                equation.w = math::dot(a, equation.xyz);
            }

            plane & operator = (const plane & plane) noexcept = default;

            aligned_vector_type normal() const noexcept {
                return equation.xyz;
            }

            T offset() const noexcept {
                return equation.w;
            }

            template <class S = aligned_vector_storage<T>>
            T signed_distance(const vector<T, S> & v) const noexcept {
                return math::dot(v, normal()) + equation.w;
            }

            template <class S = aligned_vector_storage<T>>
            T distance(const vector<T, S> & v) const noexcept {
                return abs(signed_distance(v));
            }

            template <class S = aligned_vector_storage<T>>
            aligned_vector_type reflect(const vector<T, S> & position) const noexcept {
                auto n = normal();
                return position - vector_constants<T>::two * (math::dot(position, n) + equation.w) * n;
            }

            template <class S = aligned_vector_storage<T>>
            aligned_vector_type mirror(const vector<T, S> & direction) const noexcept {
                auto n = normal();
                return direction - vector_constants<T>::two * math::dot(direction, n) * n;
            }

            void rotate(const quaternion<T> & quat) const noexcept {
                equation.xyz = quat.apply_to(equation.xyz);
            }

            vector_type equation;
        };

        template<class T, class S = aligned_vector_storage<T>>
        plane_side classify(const plane<T> & p, const vector<T, S> & v) noexcept {
            T d = p.signed_distance(v);
            return (d > constants<T>::eps) ? plane_side::front : (d < -constants<T>::eps) ? plane_side::back : plane_side::both;
        }
    }
}
