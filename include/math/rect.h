//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include "size.h"
#include "range.h"

//---------------------------------------------------------------------------

namespace asd
{
    namespace math
    {
        enum class side
        {
            left,
            top,
            right,
            bottom
        };

        template <class T>
        struct horizontal_range_swizzle_storage
        {
            horizontal_range_swizzle_storage(T min, T max) : min(min), max(max) {}

            struct
            {
                T min;
                T unnamed_0;
                T max;
                T unnamed_1;
            };
        };

        template <class T>
        struct vertical_range_swizzle_storage
        {
            vertical_range_swizzle_storage(T min, T max) : min(min), max(max) {}

            struct
            {
                T unnamed_0;
                T min;
                T unnamed_1;
                T max;
            };
        };

        template <class T, class Model = point_model_xy>
        struct rect
        {
            using coord_type = T;
            using point_type = math::point<coord_type, Model>;
            using size_type = math::size<coord_type, Model>;

            union
            {
                struct
                {
                    T left, top, right, bottom;
                };

                struct
                {
                    point_type min, max;
                };

                range<T, horizontal_range_swizzle_storage<T>> horizontal;
                range<T, vertical_range_swizzle_storage<T>> vertical;
                std::array<T, 4> data;
            };

            constexpr rect() noexcept :
                left(static_cast<T>(0)), top(static_cast<T>(0)), right(static_cast<T>(0)), bottom(static_cast<T>(0)) {}

            constexpr rect(const rect & r) noexcept : data(r.data) {}

            constexpr rect(T left, T top, T right, T bottom) noexcept :
                left(left), top(top), right(right), bottom(bottom) {}

            constexpr rect(const size_type & sz) noexcept :
                left(static_cast<T>(0)), top(static_cast<T>(0)), right(sz.data[0]), bottom(sz.data[1]) {}

            constexpr rect(const point_type & min, const size_type & sz) noexcept :
                left(min.data[0]), top(min.data[1]), right(min.data[0] + sz.data[0]), bottom(min.data[1] + sz.data[1]) {}

            explicit constexpr rect(const size_type & sz, const point_type & max) noexcept :
                left(max.data[0] - sz.data[0]), top(max.data[1] - sz.data[1]), right(max.data[0]), bottom(max.data[1]) {}

            constexpr rect(const swizzle_point<T, Model> & pos, const size_type & size) noexcept :
                min(pos), max(pos + size) {}

            template <class U> requires(!std::is_same_v<T, U>)
            explicit constexpr rect(const rect<U> & r) noexcept :
                left(static_cast<T>(r.left)), top(static_cast<T>(r.top)), right(static_cast<T>(r.right)), bottom(static_cast<T>(r.bottom)) {}

            template <class U> requires(!std::is_same_v<T, U>)
            explicit constexpr rect(const math::size<U, Model> & sz) noexcept :
                left(static_cast<T>(0)), top(static_cast<T>(0)), right(static_cast<T>(sz.data[0])), bottom(static_cast<T>(sz.data[1])) {}

            template <class U, class V> requires(!std::is_same_v<T, U> || !std::is_same_v<T, V>)
            explicit constexpr rect(const math::point<U, Model> & min, const math::size<V, Model> & sz) noexcept :
                left(static_cast<T>(min.data[0])), top(static_cast<T>(min.data[1])), right(static_cast<T>(min.data[0] + sz.data[0])), bottom(static_cast<T>(min.data[1] + sz.data[1])) {}

            template <class U, class V> requires(!std::is_same_v<T, U> || !std::is_same_v<T, V>)
            explicit constexpr rect(const math::size<V, Model> & sz, const math::point<U, Model> & max) noexcept :
                left(static_cast<T>(max.data[0] - sz.data[0])), top(static_cast<T>(max.data[1] - sz.data[1])), right(static_cast<T>(max.data[0])), bottom(static_cast<T>(max.data[1])) {}

            template <class U, class V>
            explicit constexpr rect(const math::point<U, Model> & min, const math::point<V, Model> & max) noexcept :
                min(min), max(max) {}

            template <class U, class V>
            constexpr rect(const swizzle_point<U, Model> & min, const swizzle_point<V, Model> & max) noexcept :
                min(min), max(max) {}

            template <class U, class V>
            explicit constexpr rect(const math::range<U> & h, const math::range<V> & v) noexcept :
                left(static_cast<T>(h.min)), top(static_cast<T>(v.min)), right(static_cast<T>(h.max)), bottom(static_cast<T>(v.max)) {}

            constexpr rect & operator = (const rect & r) noexcept {
                assign(r);
                return *this;
            }

            template <class U> requires(!std::is_same_v<T, U>)
            constexpr rect & operator = (const rect<U, Model> & r) noexcept {
                assign(r);
                return *this;
            }

            constexpr bool empty() const noexcept {
                return math::equal(left, right) || math::equal(top, bottom);
            }

            constexpr coord_type width() const noexcept {
                return right - left;
            }

            constexpr coord_type height() const noexcept {
                return bottom - top;
            }

            constexpr coord_type area() const noexcept {
                return width() * height();
            }

            constexpr const point_type & pos() const noexcept {
                return min;
            }

            constexpr point_type center() const noexcept {
                return point_type(math::avg(left, right), math::avg(top, bottom));
            }

            constexpr size_type size() const noexcept {
                return size_type(width(), height());
            }

            constexpr void set_width(T value) noexcept {
                right = left + value;
            }

            constexpr void set_height(T value) noexcept {
                bottom = top + value;
            }

            constexpr void set_pos(T left, T top) noexcept {
                this->right += left - this->left;
                this->bottom += top - this->top;
                this->left = left;
                this->top = top;
            }

            constexpr void set_pos(const point_type & pos) noexcept {
                this->max += pos - this->min;
                this->min = pos;
            }

            constexpr void set_max_pos(T right, T bottom) noexcept {
                this->left += right - this->right;
                this->top += bottom - this->bottom;
                this->right = right;
                this->bottom = bottom;
            }

            constexpr void set_max_pos(const point_type & pos) noexcept {
                this->min += pos - this->max;
                this->max = pos;
            }

            constexpr void set_size(T width, T height) noexcept {
                right = left + width;
                bottom = top + height;
            }

            constexpr void set_size(const size_type & s) noexcept {
                max = min + s;
            }

            constexpr void set_placement(T left, T top, T width, T height) noexcept {
                this->left = left;
                this->top = top;
                this->right = left + width;
                this->bottom = top + height;
            }

            constexpr void set_placement(const point_type & pos, const size_type & s) noexcept {
                this->min = pos;
                this->max = pos + s;
            }

            constexpr void assign(T left, T top, T right, T bottom) noexcept {
                this->left = left;
                this->top = top;
                this->right = right;
                this->bottom = bottom;
            }

            template <class U>
            constexpr void assign(const rect<U> & r) noexcept {
                data = r.data;
            }

            template <class U>
            constexpr void intersect(const rect<U, Model> & r) noexcept {
                if (r.left > left) {
                    left = r.left;
                }

                if (r.top > top) {
                    top = r.top;
                }

                if (r.right < right) {
                    right = r.right;
                }

                if (r.bottom < bottom) {
                    bottom = r.bottom;
                }

                if (width() < 0) {
                    right = left;
                }

                if (height() < 0) {
                    bottom = top;
                }
            }

            template <class U>
            constexpr void include(const rect<U, Model> & r) noexcept {
                if (r.left < left) {
                    left = r.left;
                }

                if (r.top < top) {
                    top = r.top;
                }

                if (r.right > right) {
                    right = r.right;
                }

                if (r.bottom > bottom) {
                    bottom = r.bottom;
                }
            }

            template <class U>
            constexpr int compare(const rect<U, Model> & r) const noexcept {
                return left == r.left && top == r.top && right == r.right && bottom == r.bottom ? 0 : icomp(center(), r.center());
            }

            template <class U>
            constexpr int compare(const point<U, Model> & pt) const noexcept {
                return contains(pt) ? 0 : icomp(center(), pt);
            }

            constexpr bool contains(const rect<T, Model> & r) const noexcept {
                return r.left >= left && r.top >= top && r.right <= right && r.bottom <= bottom;
            }

            constexpr bool contains(const point<T, Model> & pt) const noexcept {
                return between(static_cast<T>(pt.data[0]), left, right) && between(static_cast<T>(pt.data[1]), top, bottom);
            }

            template <class U> requires(!std::is_same_v<T, U>)
            constexpr bool contains(const rect<U, Model> & r) const noexcept {
                return contains(r.min) && contains(r.max);
            }

            template <class U> requires(!std::is_same_v<T, U>)
            constexpr bool contains(const point<U, Model> & pt) const noexcept {
                return between(static_cast<T>(pt.data[0]), left, right) && between(static_cast<T>(pt.data[1]), top, bottom);
            }

            template <class U>
            constexpr bool intersects(const rect<U, Model> & r) const noexcept {
                return r.left < right && r.top < bottom && r.right > left && r.bottom > top;
            }

            constexpr rect & resize_by(T dx, T dy) noexcept {
                left -= dx;
                top -= dy;
                right += dx;
                bottom += dy;

                return *this;
            }

            constexpr rect & resize_by(T dl, T dt, T dr, T db) noexcept {
                left -= dl;
                top -= dt;
                right += dr;
                bottom += db;

                return *this;
            }

            constexpr rect & resize_by(T delta) noexcept {
                return resize_by(delta, delta);
            }

            constexpr rect & resize_by(const size_type & delta) noexcept {
                return resize_by(delta.data[0], delta.data[1]);
            }

            constexpr rect & resize_by(const std::array<T, 4> & offsets) noexcept {
                return resize_by(offsets.left, offsets.top, offsets.right, offsets.bottom);
            }

            constexpr rect resized_by(T dx, T dy) const noexcept {
                return rect(*this).resize_by(dx, dy);
            }

            constexpr rect & resized_by(T dl, T dt, T dr, T db) noexcept {
                return rect(*this).resize_by(dl, dt, dr, db);
            }

            constexpr rect resized_by(T delta) const noexcept {
                return rect(*this).resize_by(delta);
            }

            constexpr rect resized_by(const size_type & delta) const noexcept {
                return rect(*this).resize_by(delta);
            }

            constexpr rect resized_by(const std::array<T, 4> & offsets) const noexcept {
                return rect(*this).resize_by(offsets);
            }

            constexpr rect & resize(T w, T h) noexcept {
                return resize_by((w - width()) / 2, (h - height()) / 2);
            }

            constexpr rect & resize(const size_type & s) noexcept {
                return resize_by((s - size()) / 2);
            }

            constexpr rect resized(T w, T h) const noexcept {
                return resized_by((w - width()) / 2, (h - height()) / 2);
            }

            constexpr rect resized(const size_type & s) const noexcept {
                return resized_by((s - size()) / 2);
            }

            constexpr void add(T first, T second) noexcept {
                left += first;
                right += first;
                top += second;
                bottom += second;
            }

            constexpr void subtract(T first, T second) noexcept {
                left -= first;
                right -= first;
                top -= second;
                bottom -= second;
            }

            template <class U>
            constexpr void add(const point<U, Model> & pt) noexcept {
                left += pt.data[0];
                right += pt.data[0];
                top += pt.data[1];
                bottom += pt.data[1];
            }

            template <class U>
            constexpr void subtract(const point<U, Model> & pt) noexcept {
                left -= pt.data[0];
                right -= pt.data[0];
                top -= pt.data[1];
                bottom -= pt.data[1];
            }

            constexpr rect & move_to(T first, T second) noexcept {
                add(first - left, second - top);
                return *this;
            }

            template <class U>
            constexpr rect & move_to(const point<U, Model> & pt) noexcept {
                add(pt.data[0] - left, pt.data[1] - top);
                return *this;
            }

            constexpr rect & reset_pos() noexcept {
                subtract(left, top);
                return *this;
            }

            constexpr rect & centralize() noexcept {
                subtract(min + size() / static_cast<T>(2));
                return *this;
            }

            constexpr rect & set_minimum(T first, T second) noexcept {
                if (left < first) {
                    right += first - left;
                    left = first;
                }

                if (top < second) {
                    bottom += second - top;
                    top = second;
                }

                return *this;
            }

            constexpr rect & set_maximum(T first, T second) noexcept {
                if (right > first) {
                    left -= right - first;
                    right = first;
                }

                if (bottom > second) {
                    top -= bottom - second;
                    bottom = second;
                }

                return *this;
            }

            template <class U>
            constexpr rect & set_minimum(const point<U, Model> & pt) noexcept {
                return set_minimum(pt.data[0], pt.data[1]);
            }

            template <class U>
            constexpr rect & set_maximum(const point<U, Model> & pt) noexcept {
                return set_maximum(pt.data[0], pt.data[1]);
            }

            template <class U>
            constexpr rect & operator &=(const rect<U, Model> & r) noexcept {
                intersect(r);
                return *this;
            }

            template <class U>
            constexpr rect & operator |=(const rect<U, Model> & r) noexcept {
                include(r);
                return *this;
            }

            template <class U>
            constexpr bool operator ==(const rect<U, Model> & r) const noexcept {
                return compare(r) == 0;
            }

            template <class U>
            constexpr bool operator !=(const rect<U, Model> & r) const noexcept {
                return compare(r) != 0;
            }

            template <class U>
            constexpr bool operator >=(const rect<U, Model> & r) const noexcept {
                return compare(r) >= 0;
            }

            template <class U>
            constexpr bool operator <=(const rect<U, Model> & r) const noexcept {
                return compare(r) <= 0;
            }

            template <class U>
            constexpr bool operator >(const rect<U, Model> & r) const noexcept {
                return compare(r) > 0;
            }

            template <class U>
            constexpr bool operator <(const rect<U, Model> & r) const noexcept {
                return compare(r) < 0;
            }

            constexpr T & operator [] (side i) noexcept {
                return data[static_cast<int>(i)];
            }

            constexpr const T & operator [] (side i) const noexcept {
                return data[static_cast<int>(i)];
            }

            constexpr operator std::array<T, 4> &() noexcept {
                return data;
            }

            constexpr operator const std::array<T, 4> &() const noexcept {
                return data;
            }

            constexpr rect operator + () const noexcept {
                return *this;
            }

            constexpr rect operator - () const noexcept {
                return {-left, -top, -right, -bottom};
            }

            template <class U>
            constexpr rect & operator += (const math::point<U, Model> & pt) noexcept {
                add(pt);
                return *this;
            }

            template <class U>
            constexpr rect & operator -= (const math::point<U, Model> & pt) noexcept {
                subtract(pt);
                return *this;
            }

            template <math::scalar U>
            constexpr rect & operator *= (U value) noexcept {
                left *= value;
                right *= value;
                top *= value;
                bottom *= value;

                return *this;
            }

            template <math::scalar U>
            constexpr rect & operator /= (U value) noexcept {
                left /= value;
                right /= value;
                top /= value;
                bottom /= value;

                return *this;
            }

            template <class U>
            constexpr rect & operator *= (const math::size<U, Model> & s) noexcept {
                left *= s.data[0];
                right *= s.data[0];
                top *= s.data[1];
                bottom *= s.data[1];

                return *this;
            }

            template <class U>
            constexpr rect & operator /= (const math::size<U, Model> & s) noexcept {
                left /= s.data[0];
                right /= s.data[0];
                top /= s.data[1];
                bottom /= s.data[1];

                return *this;
            }

            template <class F, class R = decltype(std::declval<F>()(std::declval<T>()))>
            constexpr rect<R, Model> map(F f) const noexcept {
                return rect<R, Model>{min.map(f), max.map(f)};
            }

            template <class R, class F>
            constexpr rect<R, Model> map(F f) const noexcept {
                return rect<R, Model>{min.template map<R>(f), max.template map<R>(f)};
            }

            template <class F, class U, class R = decltype(std::declval<F>()(std::declval<T>(), std::declval<U>()))>
            constexpr rect<R, Model> map(F f, const rect<U, Model> & r) const noexcept {
                return rect<R, Model>{min.map(f, r.min), max.map(f, r.max)};
            }

            template <class R, class F, class U>
            constexpr rect<R, Model> map(F f, const rect<U, Model> & r) const noexcept {
                return rect<R, Model>{min.template map<R>(f, r.min), max.template map<R>(f, r.max)};
            }

            template <class F, class U, class R = decltype(std::declval<F>()(std::declval<T>(), std::declval<U>()))>
            constexpr rect<R, Model> map(F f, const math::point<U, Model> & v) const noexcept {
                return rect<R, Model>{min.map(f, v), max.map(f, v)};
            }

            template <class R, class F, class U>
            constexpr rect<R, Model> map(F f, const math::point<U, Model> & v) const noexcept {
                return rect<R, Model>{min.template map<R>(f, v), max.template map<R>(f, v)};
            }

            template <class F, class U, class R = decltype(std::declval<F>()(std::declval<T>(), std::declval<U>()))>
            constexpr rect<R, Model> map(F f, const math::size<U, Model> & v) const noexcept {
                return rect<R, Model>{min.map(f, v), max.map(f, v)};
            }

            template <class R, class F, class U>
            constexpr rect<R, Model> map(F f, const math::size<U, Model> & v) const noexcept {
                return rect<R, Model>{min.template map<R>(f, v), max.template map<R>(f, v)};
            }

            template <class F, math::scalar U, class R = decltype(std::declval<F>()(std::declval<T>(), std::declval<U>()))>
            constexpr rect<R, Model> map(F f, U v) const noexcept {
                return rect<R, Model>{min.map(f, v), max.map(f, v)};
            }

            template <class R, class F, math::scalar U>
            constexpr rect<R, Model> map(F f, U v) const noexcept {
                return rect<R, Model>{min.template map<R>(f, v), max.template map<R>(f, v)};
            }
        };

        template <class T>
        rect(T left, T top, T right, T bottom) -> rect<T, point_model_xy>;

        template <class T, class Model>
        rect(const size<T, Model> & sz) -> rect<T, Model>;

        template <class T, class U, class Model>
        rect(const point<T, Model> & pos, const size<U, Model> & sz) -> rect<std::common_type_t<U, T>, Model>;

        template <class T, class U, class Model>
        rect(const point<T, Model> & min, const point<U, Model> & max) -> rect<std::common_type_t<U, T>, Model>;

//---------------------------------------------------------------------------

        template <class T, class U, class Model>
        constexpr auto operator + (const rect<T, Model> & r, const math::point<U, Model> & pt) noexcept {
            return r.map(math::op::add, pt);
        }

        template <class T, class U, class Model>
        constexpr auto operator - (const rect<T, Model> & r, const math::point<U, Model> & pt) noexcept {
            return r.map(math::op::sub, pt);
        }

        template <class T, class U, class Model>
        constexpr auto operator + (const math::point<U, Model> & pt, const rect<T, Model> & r) noexcept {
            return r.map(math::op::add, pt);
        }

        template <class T, math::scalar U, class Model>
        constexpr auto operator * (const rect<T, Model> & r, U value) noexcept {
            return r.map(math::op::mul, value);
        }

        template <class T, math::scalar U, class Model>
        constexpr auto operator / (const rect<T, Model> & r, U value) noexcept {
            return r.map(math::op::div, value);
        }

        template <class T, math::scalar U, class Model>
        constexpr auto operator * (U value, const rect<T, Model> & r) noexcept {
            return r.map(math::op::mul, value);
        }

        template <class T, class U, class Model>
        constexpr auto operator * (const rect<T, Model> & r, const math::size<U, Model> & s) noexcept {
            return r.map(math::op::mul, s);
        }

        template <class T, class U, class Model>
        constexpr auto operator / (const rect<T, Model> & r, const math::size<U, Model> & s) noexcept {
            return r.map(math::op::div, s);
        }

        template <class T, class U, class Model>
        constexpr auto operator * (const math::size<U, Model> & s, const rect<T, Model> & r) noexcept {
            return r.map(math::op::mul, s);
        }

        template <class T, class U, class Model>
        constexpr auto operator & (const rect<T, Model> & r1, const rect<U, Model> & r2) noexcept {
            return rect<T>(r1) &= r2;
        }

        template <class T, class U, class Model>
        constexpr auto operator | (const rect<T, Model> & r1, const rect<U, Model> & r2) noexcept {
            return rect<T>(r1) |= r2;
        }

//---------------------------------------------------------------------------

        template <class T, class Model>
        constexpr point<T, Model> wrap(const point<T, Model> & a, const rect<T, Model> & b) {
            return b.min + math::mod(a - b.min, b.size());
        }

        template <class T, class Model>
        constexpr size<T, Model> wrap(const size<T, Model> & a, const rect<T, Model> & b) {
            return b.min + math::mod(a - b.min, b.size());
        }

        template <class T, class Model, math::scalar U>
        constexpr auto floor_div(const rect<T, Model> & a, U b) {
            return a.map(math::op::floor_div, b);
        }

        template <class T, class Model>
        constexpr rect<T, Model> floor_div(const rect<T, Model> & a, const size<T, Model> & b) {
            return a.map(math::op::floor_div, b);
        }

        template <class T1, class T2, class Model> requires(!std::is_same_v<T1, T2>)
        constexpr auto floor_div(const rect<T1, Model> & a, const size<T2, Model> & b) {
            return a.map(math::op::floor_div, b);
        }

        template <class R, class T1, class T2, class Model> requires(!std::is_same_v<T1, T2>)
        constexpr auto floor_div(const rect<T1, Model> & a, const size<T2, Model> & b) {
            return a.template map<R>(math::op::floor_div, b);
        }

        template <class T, class Model>
        constexpr point<T, Model> floor_wrap(const point<T, Model> & a, const rect<T, Model> & b) {
            return b.min + math::floor_mod(a - b.min, b.size());
        }

        template <class T, class Model>
        constexpr size<T, Model> floor_wrap(const size<T, Model> & a, const rect<T, Model> & b) {
            return b.min + math::floor_mod(a - b.min, b.size());
        }

        template <class T, class Model>
        constexpr rect<T, Model> clamp(const rect<T, Model> & r, const point<T, Model> & low, const point<T, Model> & high) noexcept {
            return {math::clamp(r.min, low, high), math::clamp(r.max, low, high)};
        }

        template <class T, class Model>
        constexpr rect<T, Model> trunc(const rect<T, Model> & p) noexcept {
            return p.map(math::op::trunc);
        }

        template <class T, class Model>
        constexpr rect<T, Model> floor(const rect<T, Model> & p) noexcept {
            return p.map(math::op::floor);
        }

        template <class T, class Model>
        constexpr rect<T, Model> ceil(const point<T, Model> & p) noexcept {
            return p.map(math::op::ceil);
        }

        template <class T, class Model>
        constexpr rect<T, Model> round(const rect<T, Model> & p) noexcept {
            return p.map(math::op::round);
        }

        template <class R, class T, class Model> requires(!std::is_same_v<R, T>)
        constexpr auto trunc(const rect<T, Model> & p) noexcept {
            return p.template map<R>(math::op::trunc);
        }

        template <class R, class T, class Model> requires(!std::is_same_v<R, T>)
        constexpr auto floor(const rect<T, Model> & p) noexcept {
            return p.template map<R>(math::op::floor);
        }

        template <class R, class T, class Model> requires(!std::is_same_v<R, T>)
        constexpr auto ceil(const rect<T, Model> & p) noexcept {
            return p.template map<R>(math::op::ceil);
        }

        template <class R, class T, class Model> requires(!std::is_same_v<R, T>)
        constexpr auto round(const rect<T, Model> & p) noexcept {
            return p.template map<R>(math::op::round);
        }

//---------------------------------------------------------------------------

        template <class T, class Model>
        constexpr rect<T, Model> centered_rect(const point<T, Model> & pos, const math::size<T, Model> & s) noexcept {
            const auto hs = s / static_cast<T>(2);
            return {pos.data[0] - hs.data[0], pos.data[1] - hs.data[1], pos.data[0] + hs.data[0], pos.data[1] + hs.data[1]};
        }

        template <class T, class Model>
        constexpr rect<T, Model> centered_square(const point<T, Model> & pos, T size) noexcept {
            const T hs = size / 2;
            return {pos.data[0] - hs, pos.data[1] - hs, pos.data[0] + hs, pos.data[1] + hs};
        }

//---------------------------------------------------------------------------

        template <class T>
        using rect_xz = rect<T, point_model_xz>;

        using byte_rect = rect<uint8>;
        using int_rect = rect<int>;
        using uint_rect = rect<uint32>;
        using long_rect = rect<long>;
        using float_rect = rect<float>;

        using byte_rect_xz = rect_xz<uint8>;
        using int_rect_xz = rect_xz<int>;
        using uint_rect_xz = rect_xz<uint32>;
        using long_rect_xz = rect_xz<long>;
        using float_rect_xz = rect_xz<float>;
    }
}
