//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <math/matrix.h>
#include <math/plane.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace math
    {
        template <class T>
        struct frustum
        {
            frustum() = default;

            template <class S>
            frustum(const matrix<T, S> & vp) {
                auto m = math::transpose(vp);

                for (int i = 0; i < 3; ++i) {
                    planes[i * 2 + 0] = math::normalize(m[3] + m[i]);
                    planes[i * 2 + 1] = math::normalize(m[3] - m[i]);
                }
            }

            std::array<plane<T>, 6> planes; // left, right, bottom, top, near, far
        };
        
        using float_frustum = frustum<float>;
        using double_frustum = frustum<double>;

        template <class T, class S>
        bool contains(const frustum<T> & f, const math::vector<T, S> & v) {
            return
                math::classify(f.planes[0], v) != math::plane_side::back &&
                math::classify(f.planes[1], v) != math::plane_side::back &&
                math::classify(f.planes[2], v) != math::plane_side::back &&
                math::classify(f.planes[3], v) != math::plane_side::back &&
                math::classify(f.planes[4], v) != math::plane_side::back &&
                math::classify(f.planes[5], v) != math::plane_side::back;
        }
    }
}
