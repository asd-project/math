//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include "vector.h"

//---------------------------------------------------------------------------

#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wignored-attributes"
#endif

namespace asd
{
    namespace math
    {
        template <class T, class VectorStorage = default_vector_storage<T>>
        struct alignas(sizeof(T) * 4) matrix
        {
            using vector_type = vector<T, VectorStorage>;
            using aligned_vector_type = vector<T, aligned_vector_storage<T>>;
            using row_type = vector_type;

            using aligned_matrix = matrix<T, aligned_vector_storage<T>>;

            union
            {
                struct
                {
                    T m00, m01, m02, m03;
                    T m10, m11, m12, m13;
                    T m20, m21, m22, m23;
                    T m30, m31, m32, m33;
                };

                struct
                {
                    T xx, xy, xz, xw;
                    T yx, yy, yz, yw;
                    T zx, zy, zz, zw;
                    T wx, wy, wz, ww;
                };

                struct
                {
                    row_type x, y, z, w;
                };

                std::array<T, 16> m;

                std::array<vector_type, 4> v;
                std::array<row_type, 4> rows;
                std::array<std::array<T, 4>, 4> data;
            };

            member_cast(m, std::array<T, 16>);
            member_cast(rows, std::array<row_type, 4>);
            member_cast(data, std::array<std::array<T, 4>, 4>);

            constexpr matrix() noexcept : x(vector_constants<T>::positive_x), y(vector_constants<T>::positive_y), z(vector_constants<T>::positive_z), w(vector_constants<T>::positive_w) {}

            constexpr matrix(const matrix & matrix) noexcept : rows{matrix.rows} {}

            template <class S> requires(!std::is_same_v<matrix, matrix<T, S>>)
            explicit(std::is_same_v<aligned_matrix, matrix<T, S>>) constexpr matrix(const matrix<T, S> & matrix) noexcept : x{matrix.x}, y{matrix.y}, z{matrix.z}, w{matrix.w} {}

            constexpr matrix(const std::array<row_type, 4> & rows) noexcept : rows(rows) {}

            constexpr matrix(const row_type & x, const row_type & y, const row_type & z, const row_type & w) noexcept : x{x}, y{y}, z{z}, w{w} {}

            constexpr matrix(const row_type & x, const row_type & y, const row_type & z) noexcept : x{x}, y{y}, z{z}, w{vector_constants<T>::positive_w} {}

            constexpr matrix(const std::array<T, 16> & m) noexcept : m(m) {}

            constexpr matrix(const std::array<std::array<T, 4>, 4> & data) noexcept : data(data) {}

            template <class U>
                requires tag_convertible<U, matrix>
            explicit constexpr matrix(const U & v) noexcept(noexcept(convert<matrix>(v))) : matrix(convert<matrix>(v)) {}

            constexpr matrix(
                T xx, T xy, T xz, T xw,
                T yx, T yy, T yz, T yw,
                T zx, T zy, T zz, T zw,
                T wx, T wy, T wz, T ww
            ) noexcept :
                x(xx, xy, xz, xw),
                y(yx, yy, yz, yw),
                z(zx, zy, zz, zw),
                w(wx, wy, wz, ww) {}

            constexpr matrix & operator = (const matrix & matrix) noexcept {
                m = matrix.m;
                return *this;
            }

            template <class S> requires(!std::is_same_v<matrix, matrix<T, S>>)
            constexpr matrix & operator = (const matrix<T, S> & matrix) noexcept {
                m = matrix.m;
                return *this;
            }

            template <class U>
                requires tag_convertible<U, matrix>
            constexpr matrix & operator = (const U & v) noexcept {
                return *this = convert<matrix>(v);
            }

            template <class S>
            constexpr bool operator == (const matrix<T, S> & matrix) const noexcept {
                return m == matrix.m;
            }

            constexpr matrix & clear() {
                m.fill(0);
                return *this;
            }

            template <class S>
            constexpr matrix & operator +=(const matrix<T, S> & matrix) {
                rows[0] += matrix[0];
                rows[1] += matrix[1];
                rows[2] += matrix[2];
                rows[3] += matrix[3];

                return *this;
            }

            template <class S>
            constexpr matrix & operator -=(const matrix<T, S> & matrix) {
                rows[0] -= matrix[0];
                rows[1] -= matrix[1];
                rows[2] -= matrix[2];
                rows[3] -= matrix[3];

                return *this;
            }

            template <class S>
            constexpr matrix & operator *=(const matrix<T, S> & matrix) {
                return *this = operator *(*this, matrix);
            }

            constexpr matrix & operator *=(T value) {
                rows[0] *= value;
                rows[1] *= value;
                rows[2] *= value;
                rows[3] *= value;

                return *this;
            }

            constexpr matrix & operator /=(T value) {
                rows[0] /= value;
                rows[1] /= value;
                rows[2] /= value;
                rows[3] /= value;

                return *this;
            }

            constexpr row_type & operator [](int row) & {
                return rows[row];
            }

            constexpr const row_type & operator [](int row) const & {
                return rows[row];
            }

            constexpr row_type && operator [](int row) && {
                return std::move(rows[row]);
            }

            constexpr T & operator ()(int i) {
                return m[i];
            }

            constexpr T operator ()(int i) const {
                return m[i];
            }

            constexpr T & operator ()(int row, int column) {
                return data[row][column];
            }

            constexpr T operator ()(int row, int column) const {
                return data[row][column];
            }

            // can cast aligned storage to common without copy
            template <class S = common_vector_storage<T>, class Self = matrix> requires(std::is_same_v<Self, aligned_matrix>)
            constexpr operator const matrix<T, S> & () const {
                return *static_cast<const matrix<T, S> *>(static_cast<const void *>(&this->data));
            }

            template <class S = common_vector_storage<T>, class Self = matrix> requires(std::is_same_v<Self, aligned_matrix>)
            constexpr operator matrix<T, S> & () {
                return *static_cast<matrix<T, S> *>(static_cast<void *>(&this->data));
            }

            constexpr T norm() const;
            constexpr aligned_vector_type determinant() const;

            constexpr aligned_vector_type transform_point(const vector_type & b) const;
            constexpr aligned_vector_type transform_direction(const vector_type & b) const;

            template <class S>
            constexpr matrix & apply(const matrix<T, S> & m);

            constexpr matrix & scale(const vector_type & s);
            constexpr matrix & scale(T s);

            constexpr matrix & rotate_x(T);
            constexpr matrix & rotate_y(T);
            constexpr matrix & rotate_z(T);
            constexpr matrix & rotate(const vector_type & axis, T angle);
            constexpr matrix & rotate(const vector_type & rotation);

            constexpr matrix & mirror_x();
            constexpr matrix & mirror_y();
            constexpr matrix & mirror_z();

            constexpr matrix & translate(const vector_type & v);

            static constexpr aligned_matrix rotation_x(T angle);
            static constexpr aligned_matrix rotation_y(T angle);
            static constexpr aligned_matrix rotation_z(T angle);
            static constexpr aligned_matrix rotation(const vector_type & v, T angle);
            static constexpr aligned_matrix rotation(const vector_type & r);
            static constexpr aligned_matrix scaling(const vector_type & s);
            static constexpr aligned_matrix scaling(T s);
            static constexpr aligned_matrix translation(const vector_type & t);

            static constexpr aligned_matrix ortho(T x0, T x1, T y0, T y1, T z0, T z1);
            static constexpr aligned_matrix ortho_tr(T x0, T x1, T y0, T y1, T z0, T z1);
            static constexpr aligned_matrix perspective(T fov, T aspect, T z0, T z1);
            static constexpr aligned_matrix perspective_tr(T fov, T aspect, T z0, T z1);
            static constexpr aligned_matrix frustum(T x0, T x1, T y0, T y1, T z0, T z1);
            static constexpr aligned_matrix look_at(const vector_type & position, const vector_type & center, const vector_type & up = vector_constants<T>::up);
            static constexpr aligned_matrix look_to(const vector_type & position, const vector_type & direction, const vector_type & up = vector_constants<T>::up);
        };

        using float_matrix = matrix<float>;
        using double_matrix = matrix<double>;

        template <class T>
        using aligned_matrix = matrix<T, aligned_vector_storage<T>>;

        template <class T>
        using local_matrix = matrix<T, aligned_vector_storage<T>>;

        static_assert(sizeof(matrix<float>) == 16 * 4, "Size of matrix is incorrect");
        static_assert(sizeof(aligned_matrix<float>) == 16 * 4, "Size of aligned matrix is incorrect");

        template <class T>
        struct is_matrix : std::false_type
        {};

        template <class T, class S>
        struct is_matrix<matrix<T, S>> : std::true_type
        {};

        template <class T>
        constexpr bool is_matrix_v = is_matrix<T>::value;

        template <class T, class S1, class S2>
        constexpr aligned_matrix<T> operator + (const matrix<T, S1> & m1, const matrix<T, S2> & m2) {
            return aligned_matrix<T>(m1) += m2;
        }

        template <class T, class S1, class S2>
        constexpr aligned_matrix<T> operator - (const matrix<T, S1> & m1, const matrix<T, S2> & m2) {
            return aligned_matrix<T>(m1) -= m2;
        }

        template <class T, class S1, class S2>
        constexpr aligned_matrix<T> operator * (const matrix<T, S1> & a, const matrix<T, S2> & b) {
            return {
                a[0] * math::spread_x(b[0]) + a[1] * math::spread_y(b[0]) + a[2] * math::spread_z(b[0]) + a[3] * math::spread_w(b[0]),
                a[0] * math::spread_x(b[1]) + a[1] * math::spread_y(b[1]) + a[2] * math::spread_z(b[1]) + a[3] * math::spread_w(b[1]),
                a[0] * math::spread_x(b[2]) + a[1] * math::spread_y(b[2]) + a[2] * math::spread_z(b[2]) + a[3] * math::spread_w(b[2]),
                a[0] * math::spread_x(b[3]) + a[1] * math::spread_y(b[3]) + a[2] * math::spread_z(b[3]) + a[3] * math::spread_w(b[3])
            };
        }

        template <class T, class S>
        constexpr matrix<T, S> operator * (const matrix<T, S> & m, T value) {
            return matrix<T, S>(m) *= value;
        }

        template <class T, class S>
        constexpr matrix<T, S> operator * (T value, const matrix<T, S> & m) {
            return matrix<T, S>(m) *= value;
        }

        template <class T, class S1, class S2>
        constexpr aligned_vector<T> operator * (const matrix<T, S1> & m, const vector<T, S2> & vec) {
            return m.transform_point(vec);
        }

        template <class T, class S>
        constexpr T matrix<T, S>::norm() const {
            return std::max({
                math::max_axis_distance(v[0]),
                math::max_axis_distance(v[1]),
                math::max_axis_distance(v[2]),
                math::max_axis_distance(v[3])
            });
        }

        template <class T, class S>
        constexpr aligned_vector<T> matrix<T, S>::determinant() const {
            local_matrix<T> tmp1, tmp2;

            tmp1[0] = math::shuffle<1, 0, 0, 0>(rows[2]);
            tmp1[1] = math::shuffle<2, 2, 1, 1>(rows[2]);
            tmp1[2] = math::shuffle<3, 3, 3, 2>(rows[2]);
            tmp1[3] = math::shuffle<1, 0, 0, 0>(rows[3]);
            tmp2[0] = math::shuffle<2, 2, 1, 1>(rows[3]);
            tmp2[1] = math::shuffle<3, 3, 3, 2>(rows[3]);

            tmp2[2] = tmp1[1] * tmp2[1] - tmp1[2] * tmp2[0];
            tmp2[3] = tmp1[0] * tmp2[1] - tmp1[2] * tmp1[3];
            tmp2[1] = tmp1[0] * tmp2[0] - tmp1[1] * tmp1[3];

            tmp1[0] = math::shuffle<1, 0, 0, 0>(rows[1]);
            tmp1[1] = math::shuffle<2, 2, 1, 1>(rows[1]);
            tmp1[2] = math::shuffle<3, 3, 3, 2>(rows[1]);

            return math::dot_vector(math::negate<0, 1, 0, 1>(rows[0]), tmp1[0] * tmp2[2] - tmp1[1] * tmp2[3] + tmp1[2] * tmp2[1]);
        }

        template <class T, class S>
        constexpr aligned_vector<T> matrix<T, S>::transform_point(const vector<T, S> & v) const {
            return {
                sum(rows[0] * v) + m03,
                sum(rows[1] * v) + m13,
                sum(rows[2] * v) + m23
            };
        }

        template <class T, class S>
        constexpr aligned_vector<T> matrix<T, S>::transform_direction(const vector<T, S> & v) const {
            return {
                sum(rows[0] * v),
                sum(rows[1] * v),
                sum(rows[2] * v)
            };
        }

        template <class T, class S>
        template <class S1>
        constexpr matrix<T, S> & matrix<T, S>::apply(const matrix<T, S1> & m) {
            return *this *= m;
        }

        template <class T, class S>
        constexpr matrix<T, S> & matrix<T, S>::scale(const vector<T, S> & s) {
            return apply(scaling(s));
        }

        template <class T, class S>
        constexpr matrix<T, S> & matrix<T, S>::scale(T s) {
            return apply(scaling(s));
        }

        template <class T, class S>
        constexpr matrix<T, S> & matrix<T, S>::rotate_x(T angle) {
            return apply(rotation_x(angle));
        }

        template <class T, class S>
        constexpr matrix<T, S> & matrix<T, S>::rotate_y(T angle) {
            return apply(rotation_y(angle));
        }

        template <class T, class S>
        constexpr matrix<T, S> & matrix<T, S>::rotate_z(T angle) {
            return apply(rotation_z(angle));
        }

        template <class T, class S>
        constexpr matrix<T, S> & matrix<T, S>::rotate(const vector<T, S> & axis, T angle) {
            return apply(rotation(axis, angle));
        }

        template <class T, class S>
        constexpr matrix<T, S> & matrix<T, S>::rotate(const vector<T, S> & v) {
            return apply(rotation(v));
        }

        template <class T, class S>
        constexpr matrix<T, S> & matrix<T, S>::mirror_x() {
            return apply(matrix<T, S>{
                vector_constants<T>::negative_x,
                vector_constants<T>::positive_y,
                vector_constants<T>::positive_z
            });
        }

        template <class T, class S>
        constexpr matrix<T, S> & matrix<T, S>::mirror_y() {
            return apply(matrix<T, S>{
                vector_constants<T>::positive_x,
                vector_constants<T>::negative_y,
                vector_constants<T>::positive_z
            });
        }

        template <class T, class S>
        constexpr matrix<T, S> & matrix<T, S>::mirror_z() {
            return apply(matrix<T, S>{
                vector_constants<T>::positive_x,
                vector_constants<T>::positive_y,
                vector_constants<T>::negative_z
            });
        }

        template <class T, class S>
        constexpr matrix<T, S> & matrix<T, S>::translate(const vector<T, S> & v) {
            return apply(translation(v));
        }

        template <class T, class S>
        constexpr aligned_matrix<T> matrix<T, S>::rotation_x(T angle) {
            auto v = trigon(angle);

            return {
                vector_constants<T>::positive_x,    // 1,  0,  0
                math::shuffle<3, 1, 2, 3>(v),       // 0,  c, -s
                math::shuffle<3, 0, 1, 3>(v)        // 0,  s,  c
            };
        }

        template <class T, class S>
        constexpr aligned_matrix<T> matrix<T, S>::rotation_y(T angle) {
            auto v = trigon(angle);

            return {
                math::shuffle<1, 3, 0, 3>(v),       // c,  0,  s
                vector_constants<T>::positive_y,    // 0,  1,  0
                math::shuffle<2, 3, 1, 3>(v)        //-s,  0,  c
            };
        }

        template <class T, class S>
        constexpr aligned_matrix<T> matrix<T, S>::rotation_z(T angle) {
            auto v = trigon(angle);

            return {
                math::shuffle<1, 2, 3, 3>(v),       // c, -s,  0
                math::shuffle<0, 1, 3, 3>(v),       // s,  c,  0
                vector_constants<T>::positive_z
            };
        }

        template <class T, class S>
        constexpr aligned_matrix<T> matrix<T, S>::rotation(const vector<T, S> & v, T angle) {
            auto sc = trigon(angle);
            auto cc = math::shuffle<1, 1, 1, 3>(sc);
            auto nc = (vector_constants<T>::one_xyz - cc) * v;
            auto ss = math::shuffle<0, 0, 0, 3>(sc) * v;

            sc = math::shuffle<1, 0, 0, 3>(nc) * math::shuffle<2, 2, 1, 3>(v);
            cc += v * nc;

            nc = sc + ss;
            sc -= ss;

            return {
                math::shuffle<0, 2, 1, 3>(math::shuffle<0, 0, 2, 2>(cc, nc), sc), // cc.x, nc.z, sc.y, 0,
                math::shuffle<0, 2, 0, 3>(math::shuffle<2, 2, 1, 1>(sc, cc), nc), // sc.z, cc.y, nc.x, 0,
                math::shuffle<0, 2, 2, 3>(math::shuffle<1, 1, 0, 0>(nc, sc), cc)  // nc.y, sc.x, cc.z, 0,
            };
        }

        template <class T, class S>
        constexpr aligned_matrix<T> matrix<T, S>::rotation(const vector<T, S> & euler) {
            local_vector<T> sine, cosine;
            math::sincos(euler, sine, cosine);

            auto x = math::blend<0, 1, 1, 0>(math::shuffle<0, 0, 0, 0>(sine, cosine), vector_constants<T>::positive_y); // [ sx, 1, 0, cx ]
            auto y = math::blend<0, 1, 1, 0>(math::shuffle<1, 1, 1, 1>(sine, cosine), vector_constants<T>::positive_y); // [ sy, 1, 0, cy ]
            auto z = math::blend<0, 1, 1, 0>(math::shuffle<2, 2, 2, 2>(sine, cosine), vector_constants<T>::positive_y); // [ sz, 1, 0, cz ] (tmp)
            z = math::shuffle<3, 0, 1, 2>(z);                                                                           // [ cz, sz, 1, 0 ]

            auto yz = math::negate_z(math::shuffle<0, 0, 3, 2>(y)) * z;    // [ sy * cz, sy * sz, -cy, 0 ]
            auto mz = math::negate_x(math::shuffle<1, 0, 2, 3>(z));        // [ -sz, cz, 1, 0 ]

            return {
                math::shuffle<3, 3, 0, 0>(y) * z,                           //  cy * cz,               | cy * sz,               | sy,     | 0
                math::shuffle<3, 3, 2, 2>(x) * mz + math::spread_x(x) * yz, // -cx * sz + sx * sy * cz | cx * cz + sx * sy * sz |-sx * cy | 0
                math::shuffle<0, 0, 2, 2>(x) * mz - math::spread_w(x) * yz  // -sx * sz - cx * sy * cz | sx * cz - cx * sy * sz | cx * cy | 0
            };
        }

        template <class T, class S>
        constexpr aligned_matrix<T> matrix<T, S>::scaling(const vector<T, S> & s) {
            return {
                math::mask_x(s),
                math::mask_y(s),
                math::mask_z(s)
            };
        }

        template <class T, class S>
        constexpr aligned_matrix<T> matrix<T, S>::scaling(T s) {
            return scaling(vector<T, S>{s});
        }

        template <class T, class S>
        constexpr aligned_matrix<T> matrix<T, S>::translation(const vector<T, S> & t) {
            return {
                math::blend<0, 0, 0, 1>(vector_constants<T>::positive_x, math::spread_x(t)),
                math::blend<0, 0, 0, 1>(vector_constants<T>::positive_y, math::spread_y(t)),
                math::blend<0, 0, 0, 1>(vector_constants<T>::positive_z, math::spread_z(t))
            };
        }

        template <class T, class S>
        constexpr aligned_matrix<T> matrix<T, S>::ortho(T x0, T x1, T y0, T y1, T z0, T z1) {
            const local_vector<T> max{x1, y1, z1, 1};
            const local_vector<T> min{x0, y0, z0, 0};

            auto d = math::invert(max - min);
            const auto t = -(max + min) * d;
            d *= vector_constants<T>::two;

            return {
                math::blend<0, 0, 0, 1>(math::mask_x(d), math::spread_x(t)),    // 2/w |  0  |  0  | -(x0+x1)/w
                math::blend<0, 0, 0, 1>(math::mask_y(d), math::spread_y(t)),    //  0  | 2/h |  0  | -(y0+y1)/h
                math::blend<0, 0, 0, 1>(math::mask_z(d), math::spread_z(t)),    //  0  |  0  | 2/l | -(z0+z1)/l
                vector_constants<T>::positive_w                                 //  0  |  0  |  0  |    1
            };
        }

        template <class T, class S>
        constexpr aligned_matrix<T> matrix<T, S>::ortho_tr(T x0, T x1, T y0, T y1, T z0, T z1) {
            const local_vector<T> max{x1, y1, z1, 1};
            const local_vector<T> min{x0, y0, z0, 0};

            auto d = math::invert(max - min);
            const auto t = -(max + min) * d;
            d *= vector_constants<T>::two;

            return {
                math::mask_x(d),                                                //    2/w    |     0     |     0    | 0
                math::mask_y(d),                                                //     0     |    2/h    |     0    | 0
                math::mask_z(d),                                                //     0     |     0     |    2/l   | 0
                math::blend<0, 0, 0, 1>(t, vector_constants<T>::positive_w)     //-(x0+x1)/w |-(y0+y1)/h |-(z0+z1)/l| 1
            };
        }

        template <class T, class S>
        constexpr aligned_matrix<T> matrix<T, S>::
        perspective(T fov, T aspect, T z0, T z1) {
            const T f = 1 / std::tan(radians(fov) / 2);
            const T z = z1 / (z1 - z0);
            const local_vector<T> v = {f * aspect, f, z, -z0 * z};

            return {
                math::mask_x(v),                    // f/a | 0 | 0 |  0
                math::mask_y(v),                    //  0  | f | 0 |  0
                math::mask<0, 0, 1, 1>(v),          //  0  | 0 | z |-z0*z
                vector_constants<T>::positive_z     //  0  | 0 | 1 |  0
            };
        }

        template <class T, class S>
        constexpr aligned_matrix<T> matrix<T, S>::perspective_tr(T fov, T aspect, T z0, T z1) {
            const T f = 1 / std::tan(radians(fov) / 2);
            const T z = z1 / (z1 - z0);
            const local_vector<T> v = {f * aspect, f, z, -z0 * z};

            return {
                math::mask_x(v),                                                // f*a | 0 |  0  | 0
                math::mask_y(v),                                                //  0  | f |  0  | 0
                math::blend<1, 1, 0, 1>(v, vector_constants<T>::positive_w),    //  0  | 0 |  z  | 1
                math::mask_z(math::spread_w(v)),                                //  0  | 0 |-z0*z| 0
            };
        }

        template <class T, class S>
        constexpr aligned_matrix<T> matrix<T, S>::frustum(T x0, T x1, T y0, T y1, T z0, T z1) {
            const T n = 2 * z1;
            const local_vector<T> v{1 / (x1 - x0), 1 / (y1 - y0), 1 / (z1 - z0), 0};

            return {
                math::spread_x(v) * vector<T>{n, 0, x1 + x0, 0},
                math::spread_y(v) * vector<T>{0, n, y1 + y0, 0},
                math::spread_z(v) * vector<T>{0, 0, z1 + z0, n * z0},
                vector_constants<T>::negative_z
            };
        }

        template <class T, class S>
        constexpr aligned_matrix<T> matrix<T, S>::look_to(const vector<T, S> & position, const vector<T, S> & direction, const vector<T, S> & up) {
            auto f = math::unit(direction);
            auto s = math::unit(math::cross(up, f));
            auto u = math::unit(math::cross(f, s));

            return {
                math::blend<0, 0, 0, 1>(s, -math::dot_vector(s, position)),
                math::blend<0, 0, 0, 1>(u, -math::dot_vector(u, position)),
                math::blend<0, 0, 0, 1>(f, -math::dot_vector(f, position))
            };
        }

        template <class T, class S>
        constexpr aligned_matrix<T> matrix<T, S>::look_at(const vector<T, S> & position, const vector<T, S> & center, const vector<T, S> & up) {
            return look_to(position, center - position, up);
        }

        template <class T, class S>
        aligned_matrix<T> invert(const matrix<T, S> & m) noexcept {
            aligned_matrix<T> out;
            local_vector<T> r[4], temp;

            temp = math::shuffle<0, 1, 0, 1>(m[0], m[1]);
            r[1] = math::shuffle<0, 1, 0, 1>(m[2], m[3]);
            r[0] = math::shuffle<0, 2, 0, 2>(temp, r[1]);
            r[1] = math::shuffle<1, 3, 1, 3>(r[1], temp);
            temp = math::shuffle<2, 3, 2, 3>(m[0], m[1]);
            r[3] = math::shuffle<2, 3, 2, 3>(m[2], m[3]);
            r[2] = math::shuffle<0, 2, 0, 2>(temp, m[3]);
            r[3] = math::shuffle<1, 3, 1, 3>(r[3], temp);

            temp = r[2] * r[3];
            temp = math::shuffle<1, 0, 3, 2>(temp) - math::shuffle<3, 2, 1, 0>(temp);
            out[0] = -r[1] * temp;
            out[1] = -r[0] * temp;
            out[1] = math::shuffle<2, 3, 0, 1>(out[1]);

            temp = r[1] * r[2];
            temp = math::shuffle<1, 0, 3, 2>(temp) - math::shuffle<3, 2, 1, 0>(temp);
            out[0] += r[3] * temp;
            out[3] = r[0] * temp;
            out[3] = math::shuffle<2, 3, 0, 1>(out[3]);

            temp = math::shuffle<2, 3, 0, 1>(r[1]) * r[3];
            temp = math::shuffle<1, 0, 3, 2>(temp) - math::shuffle<3, 2, 1, 0>(temp);
            r[2] = math::shuffle<2, 3, 0, 1>(r[2]);
            out[0] += r[2] * temp;
            out[2] = r[0] * temp;
            out[2] = math::shuffle<2, 3, 0, 1>(out[2]);

            temp = r[0] * r[1];
            temp = math::shuffle<1, 0, 3, 2>(temp) - math::shuffle<3, 2, 1, 0>(temp);
            out[2] -= r[3] * temp;
            out[3] += r[2] * temp;

            temp = r[0] * r[3];
            temp = math::shuffle<1, 0, 3, 2>(temp) - math::shuffle<3, 2, 1, 0>(temp);
            out[1] -= r[2] * temp;
            out[2] += r[1] * temp;

            temp = r[0] * r[2];
            temp = math::shuffle<1, 0, 3, 2>(temp) - math::shuffle<3, 2, 1, 0>(temp);
            out[1] += r[3] * temp;
            out[3] -= r[1] * temp;

            auto det = math::invert(math::dot_vector(r[0], out[0]));
            out[0] *= det;
            out[1] *= det;
            out[2] *= det;
            out[3] *= det;

            return out;
        }

        template <class T, class S>
        aligned_matrix<T> transpose(const matrix<T, S> & m) noexcept {
            auto v0 = math::shuffle<0, 1, 0, 1>(m[0], m[1]);
            auto v1 = math::shuffle<2, 3, 2, 3>(m[0], m[1]);
            auto v2 = math::shuffle<0, 1, 0, 1>(m[2], m[3]);
            auto v3 = math::shuffle<2, 3, 2, 3>(m[2], m[3]);

            return {
                math::shuffle<0, 2, 0, 2>(v0, v2),
                math::shuffle<1, 3, 1, 3>(v0, v2),
                math::shuffle<0, 2, 0, 2>(v1, v3),
                math::shuffle<1, 3, 1, 3>(v1, v3)
            };
        }

        template
        struct matrix<float>;
    }
}

#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif
