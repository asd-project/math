//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <container/allocator/aligned_alloc.h>
#include <meta/convert.h>

#include <math/math.h>
#include <math/point.h>
#include <simd/simd.h>

//---------------------------------------------------------------------------

namespace asd::math
{
    template <bool Aligned>
    struct vector_model
    {
        using grid_type = cartesian_grid;
        static constexpr size_t dimensions = 4;
    };

    using common_vector_model = vector_model<false>;
    using aligned_vector_model = vector_model<true>;

    template <class T, class Storage>
    struct vector;

    template <int... Sequence>
    struct swizzle_sequence
    {};

    template <class T, class S, bool Assignable = S::assignable>
    struct swizzle_vector
    {};

    template <int X, int Y>
    struct swizzle_sequence<X, Y>
    {
        static constexpr bool assignable = (X != Y);

        template <int Component>
        using nth_component = std::integral_constant<int, (X == Component) ? 0 : (Y == Component) ? 1 : -1>;

        enum : int
        {
            HX = nth_component<0>::value,
            HY = nth_component<1>::value,
            HZ = nth_component<2>::value,
            HW = nth_component<3>::value,

            MX = X >= 0 ? 1 : 0,
            MY = Y >= 0 ? 1 : 0,
            MZ = 0,
            MW = 0,
            SX = X >= 0 ? X : 0,
            SY = Y >= 0 ? Y : 0,
            SZ = 0,
            SW = 0,

            BX = HX >= 0 ? 1 : 0,
            BY = HY >= 0 ? 1 : 0,
            BZ = HZ >= 0 ? 1 : 0,
            BW = HW >= 0 ? 1 : 0,
            AX = HX >= 0 ? HX : 0,
            AY = HY >= 0 ? HY : 0,
            AZ = HZ >= 0 ? HZ : 0,
            AW = HW >= 0 ? HW : 0
        };
    };

    template <int X, int Y, int Z>
    struct swizzle_sequence<X, Y, Z>
    {
        static constexpr bool assignable = ((X != Y) && (X != Z) && (Y != Z));

        template <int Component>
        using nth_component = std::integral_constant<int, (X == Component) ? 0 : (Y == Component) ? 1 : (Z == Component) ? 2 : -1>;

        enum : int
        {
            HX = nth_component<0>::value,
            HY = nth_component<1>::value,
            HZ = nth_component<2>::value,
            HW = nth_component<3>::value,

            MX = X >= 0 ? 1 : 0,
            MY = Y >= 0 ? 1 : 0,
            MZ = Z >= 0 ? 1 : 0,
            MW = 0,
            SX = X >= 0 ? X : 0,
            SY = Y >= 0 ? Y : 0,
            SZ = Z >= 0 ? Z : 0,
            SW = 0,

            BX = HX >= 0 ? 1 : 0,
            BY = HY >= 0 ? 1 : 0,
            BZ = HZ >= 0 ? 1 : 0,
            BW = HW >= 0 ? 1 : 0,
            AX = HX >= 0 ? HX : 0,
            AY = HY >= 0 ? HY : 0,
            AZ = HZ >= 0 ? HZ : 0,
            AW = HW >= 0 ? HW : 0
        };
    };

    template <int X, int Y, int Z, int W>
    struct swizzle_sequence<X, Y, Z, W>
    {
        static constexpr bool assignable = ((X != Y) && (X != Z) && (X != W) && (Y != Z) && (Y != W) && (Z != W));

        template <int Component>
        using nth_component = std::integral_constant<int, (X == Component) ? 0 : (Y == Component) ? 1 : (Z == Component) ? 2 : (W == Component) ? 3 : -1>;

        enum : int
        {
            HX = nth_component<0>::value,
            HY = nth_component<1>::value,
            HZ = nth_component<2>::value,
            HW = nth_component<3>::value,

            MX = X >= 0 ? 1 : 0,
            MY = Y >= 0 ? 1 : 0,
            MZ = Z >= 0 ? 1 : 0,
            MW = W >= 0 ? 1 : 0,
            SX = X >= 0 ? X : 0,
            SY = Y >= 0 ? Y : 0,
            SZ = Z >= 0 ? Z : 0,
            SW = W >= 0 ? W : 0,

            BX = HX >= 0 ? 1 : 0,
            BY = HY >= 0 ? 1 : 0,
            BZ = HZ >= 0 ? 1 : 0,
            BW = HW >= 0 ? 1 : 0,
            AX = HX >= 0 ? HX : 0,
            AY = HY >= 0 ? HY : 0,
            AZ = HZ >= 0 ? HZ : 0,
            AW = HW >= 0 ? HW : 0
        };
    };

    template <class T, int... Mask>
    using swizzle_pack = swizzle_vector<T, swizzle_sequence<Mask...>>;

    template <class T>
    using common_vector_storage = point_storage<T, common_vector_model>;

    template <class T>
    using aligned_vector_storage = point_storage<T, aligned_vector_model>;

    template <class T>
    using default_vector_storage = common_vector_storage<T>;

    template <class T, class Storage = default_vector_storage<T>>
    struct alignas(sizeof(T) * 4) vector : Storage, aligned_alloc
    {
        static_assert(math::is_scalar_v<T>, "T should be scalar");

        using S = Storage;

        using value_type = T;
        using iterator = typename std::array<T, 4>::iterator;
        using const_iterator = typename std::array<T, 4>::const_iterator;
        using reference = T &;
        using const_reference = const T &;
        using pointer = T *;
        using difference_type = typename std::array<T, 4>::difference_type;
        using size_type = uint8_t;

        using underlying_type = typename simd<T, 4>::type;
        using aligned_vector = vector<T, aligned_vector_storage<T>>;
        using base_type = Storage;
        using constants = math::constants<vector<T>>;

        constexpr vector() noexcept :
            vector(constants::identity) {}

        constexpr vector(const underlying_type & v) noexcept :
            base_type(v) {}

        constexpr vector(const vector & v) noexcept :
            base_type(v) {}

        template <class S> requires(!std::is_same_v<vector, vector<T, S>>)
        explicit(std::is_same_v<aligned_vector, vector<T, S>>) constexpr vector(const vector<T, S> & v) noexcept :
            base_type(v.data) {}

        template <class U, class S> requires(!std::is_same_v<T, U> || std::is_same_v<Storage, S>)
        explicit constexpr vector(const vector<U, S> & v) :
            base_type(static_cast<T>(v.x), static_cast<T>(v.y), static_cast<T>(v.z), static_cast<T>(v.w)) {}

        template <class S>
        constexpr vector(const vector<T, S> & v, T w) noexcept : base_type(v.data) {
            this->w = w;
        }

        template <class S>
        constexpr vector(const vector<T, S> & v, T z, T w) noexcept : base_type(v.data) {
            this->z = z;
            this->w = w;
        }

        template <class S>
        constexpr vector(T x, const vector<T, S> & v) : base_type(simd<T, 4>::template shuffle<3, 0, 1, 2>(v)) {
            this->x = x;
        }

        template <class S>
        constexpr vector(T x, T y, const vector<T, S> & v) : base_type(simd<T, 4>::template shuffle<2, 3, 0, 1>(v)) {
            this->x = x;
            this->y = y;
        }

        template <class S>
        constexpr vector(T x, const vector<T, S> & v, T w) : base_type(simd<T, 4>::template shuffle<3, 0, 1, 2>(v)) {
            this->x = x;
            this->w = w;
        }

        template <class U, class S> requires(!std::is_same_v<T, U> && std::is_same_v<S, aligned_vector_storage<T>>)
        explicit constexpr vector(const vector<U, S> & v) : base_type(simd_convert<underlying_type>(v.simd_storage)) {}

        template <class U, class S> requires(!std::is_same_v<T, U> && std::is_same_v<S, aligned_vector_storage<T>>)
        constexpr vector(const vector<U, S> & v, T w) : base_type(simd_convert<underlying_type>(v.simd_storage)) {
            this->w = w;
        }

        template <class U, class S> requires(!std::is_same_v<T, U> && std::is_same_v<S, aligned_vector_storage<T>>)
        constexpr vector(const vector<U, S> & v, T z, T w) : base_type(simd_convert<underlying_type>(v.simd_storage)) {
            this->z = z;
            this->w = w;
        }

        template <class U, class S> requires(!std::is_same_v<T, U>)
        constexpr vector(T a, const vector<U, S> & v) : base_type(simd_convert<underlying_type>(simd<T, 4>::template shuffle<3, 0, 1, 2>(v))) {
            this->x = a;
        }

        template <class U, class S> requires (!std::is_same_v<T, U>)
        constexpr vector(T a, T b, const vector<U, S> & v) :
            base_type(simd_convert<underlying_type>(simd<T, 4>::template shuffle<2, 3, 0, 1>(v)))
        {
            this->x = a;
            this->y = b;
        }

        template <math::scalar U, class S> requires (!std::is_same_v<T, U>)
        constexpr vector(T a, const vector<U, S> & v, T b) :
            base_type(simd_convert<underlying_type>(simd<T, 4>::template shuffle<3, 0, 1, 2>(v)))
        {
            this->x = a;
            this->w = b;
        }

        template <math::scalar U = T> requires (!std::is_same_v<underlying_type, std::array<U, 4>>)
        explicit constexpr vector(const std::array<U, 4> & v) noexcept :
            base_type(v) {}

        template <math::scalar U = T>
        explicit constexpr vector(const std::array<U, 3> & v, T w = 0) noexcept :
            base_type(v[0], v[1], v[2], w) {}

        template <math::scalar U = T>
        explicit constexpr vector(const std::array<U, 2> & v, T z = 0, T w = 0) noexcept :
            base_type(v[0], v[1], z, w) {}

        template <point_like U> requires (std::is_same_v<typename U::model_type, point_model_xy> && std::is_same_v<typename U::value_type, value_type>)
        constexpr vector(const U & pt, T z = T(0), T w = T(0)) noexcept :
            base_type(pt.data[0], pt.data[1], z, w) {}

        template <point_like U> requires (std::is_same_v<typename U::model_type, point_model_xz> && std::is_same_v<typename U::value_type, value_type>)
        constexpr vector(const U & pt, T w = T(0)) noexcept :
            base_type(pt.data[0], T(0), pt.data[1], w) {}

        template <point_like U> requires (std::is_same_v<typename U::model_type, point_model_xyz> && std::is_same_v<typename U::value_type, value_type>)
        constexpr vector(const U & pt, T w = T(0)) noexcept :
            base_type(pt.data[0], pt.data[1], pt.data[2], w) {}

        template <point_like U> requires (std::is_same_v<typename U::model_type, point_model_xy> && !std::is_same_v<typename U::value_type, value_type>)
        explicit constexpr vector(const U & pt, T z = T(0), T w = T(0)) noexcept :
            base_type(static_cast<T>(pt.data[0]), static_cast<T>(pt.data[1]), z, w) {}

        template <point_like U> requires (std::is_same_v<typename U::model_type, point_model_xz> && !std::is_same_v<typename U::value_type, value_type>)
        explicit constexpr vector(const U & pt, T w = T(0)) noexcept :
            base_type(static_cast<T>(pt.data[0]), T(0), static_cast<T>(pt.data[1]), w) {}

        template <point_like U> requires (std::is_same_v<typename U::model_type, point_model_xyz> && !std::is_same_v<typename U::value_type, value_type>)
        explicit constexpr vector(const U & pt, T w = T(0)) noexcept :
            base_type(static_cast<T>(pt.data[0]), static_cast<T>(pt.data[1]), static_cast<T>(pt.data[2]), w) {}

        template <class Model>
        constexpr vector(const swizzle_point<T, Model> & v) noexcept :
            vector(v.point()) {}

        template <class U, class Model> requires(!std::is_same_v<T, U>)
        explicit constexpr vector(const swizzle_point<U, Model> & v) noexcept :
            vector(v.point()) {}

        template <class U>
            requires tag_convertible<U, vector>
        explicit constexpr vector(const U & v) noexcept(noexcept(convert<vector>(v))) : base_type(convert<vector>(v)) {}

        constexpr vector(const T * v) :
            vector(simd<T, 4>::load(v)) {}

        constexpr vector(T value) noexcept :
            base_type(value, value, value, T(1)) {}

        constexpr vector(T x, T y, T z = T(0), T w = T(0)) noexcept :
            base_type(x, y, z, w) {}

        template <class U> requires(!std::is_same_v<T, U> && math::is_scalar_v<U>)
        explicit constexpr vector(U value) noexcept :
            base_type(static_cast<T>(value), static_cast<T>(value), static_cast<T>(value), T(1)) {}

        template <class U> requires(!std::is_same_v<T, U>)
        explicit constexpr vector(U x, U y, U z = T(0), U w = T(0)) noexcept :
            base_type(static_cast<T>(x), static_cast<T>(y), static_cast<T>(z), static_cast<T>(w)) {}

        template <class S>
        constexpr vector & operator = (const vector<T, S> & vec) noexcept {
            Storage::assign(vec);
            return *this;
        }

        template <class U>
            requires tag_convertible<U, vector>
        constexpr vector & operator = (const U & v) noexcept {
            return *this = convert<vector>(v);
        }

        // can cast aligned storage to common without copy
        template <class S = common_vector_storage<T>, class Self = vector> requires(std::is_same_v<Self, aligned_vector>)
        constexpr operator const vector<T, S> & () const {
            return *static_cast<const vector<T, S> *>(static_cast<const void *>(&this->data));
        }

        template <class S = common_vector_storage<T>, class Self = vector> requires(std::is_same_v<Self, aligned_vector>)
        constexpr operator vector<T, S> & () {
            return *static_cast<vector<T, S> *>(static_cast<void *>(&this->data));
        }

        using base_type::assign;

        constexpr vector & assign(T x, T y, T z, T w) {
            this->x = x, this->y = y, this->z = z, this->w = w;
            return *this;
        }

        constexpr vector & fill(T value) {
            return *this = simd<T, 4>::fill(value);
        }

        constexpr aligned_vector operator + () const {
            return *this;
        }

        constexpr aligned_vector operator - () const {
            return simd<T, 4>::negate(*this);
        }

        constexpr vector & operator += (const vector & v) {
            Storage::assign(simd<T, 4>::add(*this, v));
            return *this;
        }

        constexpr vector & operator -= (const vector & v) {
            Storage::assign(simd<T, 4>::sub(*this, v));
            return *this;
        }

        constexpr vector & operator *= (const vector & v) {
            Storage::assign(simd<T, 4>::mul(*this, v));
            return *this;
        }

        constexpr vector & operator /= (const vector & v) {
            Storage::assign(simd<T, 4>::div(*this, v));
            return *this;
        }

        template <math::scalar U>
        constexpr vector & operator += (U value) {
            return *this = simd<T, 4>::add(*this, simd<T, 4>::fill(static_cast<T>(value)));
        }

        template <math::scalar U>
        constexpr vector & operator -= (U value) {
            return *this = simd<T, 4>::sub(*this, simd<T, 4>::fill(static_cast<T>(value)));
        }

        template <math::scalar U>
        constexpr vector & operator *= (U value) {
            return *this = simd<T, 4>::mul(*this, simd<T, 4>::fill(static_cast<T>(value)));
        }

        template <math::scalar U>
        constexpr vector & operator /= (U value) {
            if constexpr (std::is_floating_point_v<T>) {
                if (math::abs(value) <= math::constants<U>::eps) {
                    return *this = simd<T, 4>::fill(std::numeric_limits<T>::infinity());
                }
            }

            return *this = simd<T, 4>::div(*this, simd<T, 4>::fill(static_cast<T>(value)));
        }

        template <class S>
        constexpr bool operator == (const vector<T, S> & v) const {
            return simd<T, 4>::equal(*this, v);
        }

        template <class S>
        constexpr bool operator != (const vector<T, S> & v) const {
            return simd<T, 4>::notequal(*this, v);
        }

        constexpr size_type size() const {
            return 4;
        }

        constexpr bool empty() const {
            return false;
        }

        constexpr reference operator [] (size_t index) {
            return this->data[index];
        }

        constexpr const_reference operator [] (size_t index) const {
            return this->data[index];
        }

        constexpr reference operator [] (int index) {
            return this->data[index];
        }

        constexpr const_reference operator [] (int index) const {
            return this->data[index];
        }

        constexpr iterator begin() {
            return this->data.begin();
        }

        constexpr iterator end() {
            return this->data.end();
        }

        constexpr const_iterator begin() const {
            return this->data.begin();
        }

        constexpr const_iterator end() const {
            return this->data.end();
        }

        constexpr void swap(vector & v) {
            std::swap(this->data, v.data);
        }

        template<class U = T> requires (!std::is_same_v<underlying_type, std::array<U, 4>>)
        explicit constexpr operator const std::array<U, 4> &() const noexcept {
            return this->data;
        }

        template <class F>
        constexpr vector & apply(F f) noexcept {
            f(this->data[0]);
            f(this->data[1]);
            f(this->data[2]);
            f(this->data[3]);

            return *this;
        }

        template <class F, class U, class US>
        constexpr vector & apply(F f, const vector<U, US> & v) noexcept {
            f(this->data[0], static_cast<T>(v.data[0]));
            f(this->data[1], static_cast<T>(v.data[1]));
            f(this->data[2], static_cast<T>(v.data[2]));
            f(this->data[3], static_cast<T>(v.data[3]));

            return *this;
        }

        template <class F, math::scalar U>
        constexpr vector & apply(F f, U value) noexcept {
            f(this->data[0], value);
            f(this->data[1], value);
            f(this->data[2], value);
            f(this->data[3], value);

            return *this;
        }

        template <class F>
        constexpr auto map(F f) const noexcept {
            using R = decltype(std::declval<F>()(std::declval<T>()));
            return vector<R, Storage>{f(this->data[0]), f(this->data[1]), f(this->data[2]), f(this->data[3])};
        }

        template <class R, class F>
        constexpr vector<R, Storage> map(F f) const noexcept {
            return {static_cast<R>(f(this->data[0])), static_cast<R>(f(this->data[1])), static_cast<R>(f(this->data[2])), static_cast<R>(f(this->data[3]))};
        }

        template <class F, class U, class US>
        constexpr auto map(F f, const vector<U, US> & v) const noexcept {
            using R = decltype(std::declval<F>()(std::declval<T>(), std::declval<U>().data[0]));
            return vector<R, Storage>{f(this->data[0], v.data[0]), f(this->data[1], v.data[1]), f(this->data[2], v.data[2]), f(this->data[3], v.data[3])};
        }

        template <class R, class F, class U, class US>
        constexpr vector<R, Storage> map(F f, const vector<U, US> & v) const noexcept {
            return {static_cast<R>(f(this->data[0], v.data[0])), static_cast<R>(f(this->data[1], v.data[1])), static_cast<R>(f(this->data[2], v.data[2])), static_cast<R>(f(this->data[3], v.data[3]))};
        }

        template <class F, math::scalar U>
        constexpr auto map(F f, U v) const noexcept {
            using R = decltype(std::declval<F>()(std::declval<T>(), std::declval<U>()));
            return vector<R, Storage>{f(this->data[0], v), f(this->data[1], v), f(this->data[2], v), f(this->data[3], v)};
        }

        template <class R, class F, math::scalar U>
        constexpr vector<R, Storage> map(F f, U v) const noexcept {
            return {static_cast<R>(f(this->data[0], v)), static_cast<R>(f(this->data[1], v)), static_cast<R>(f(this->data[2], v)), static_cast<R>(f(this->data[3], v))};
        }

        template <class F>
        constexpr auto reduce(F f) const noexcept {
            return f(f(f(this->data[0], this->data[1]), this->data[2]), this->data[3]);
        }

        template <class M, class R>
        constexpr auto map_reduce(M m, R r) const noexcept {
            return r(r(r(m(this->data[0]), m(this->data[1])), m(this->data[2])), m(this->data[3]));
        }

        template <class M, class R, class U, class US>
        constexpr auto map_reduce(M m, R r, const vector<U, US> & v) const noexcept {
            return r(r(r(m(this->data[0], v.data[0]), m(this->data[1], v.data[1])), m(this->data[2], v.data[2])), m(this->data[3], v.data[3]));
        }

        template <class M, class R, math::scalar U>
        constexpr auto map_reduce(M m, R r, U v) const noexcept {
            return r(r(r(m(this->data[0], v), m(this->data[1], v)), m(this->data[2], v)), m(this->data[3], v));
        }
    };

//---------------------------------------------------------------------------

    template <class T, int... Mask>
    struct swizzle_vector<T, swizzle_sequence<Mask...>, true>
    {
        using simd_type = asd::simd<T, 4>;
        using underlying_type = typename simd_type::type;
        using S = swizzle_sequence<Mask...>;

        constexpr swizzle_vector & operator = (const vector<T> & v) noexcept {
            simd_type::store(
                simd_type::template blend<S::BX, S::BY, S::BZ, S::BW>(*this, simd_type::template shuffle<S::AX, S::AY, S::AZ, S::AW>(v)),
                this->data.data()
            );

            return *this;
        }

        template <class U> requires (std::is_same_v<U, T>)
        constexpr operator vector<U>() const {
            return operator vector<T>();
        }

        constexpr operator vector<T>() const {
            return operator underlying_type();
        }

        constexpr operator underlying_type() const {
            return simd_type::template mask<mk_mask4(S::MX, S::MX, S::MX, S::MX)>(simd_type::template shuffle<S::SX, S::SY, S::SZ, S::SW>(simd_type::load(v.data())));
        }

        constexpr operator const std::array<T, 4> & () const noexcept {
            return v;
        }

        constexpr operator std::array<T, 3>() const noexcept {
            return {v[0], v[1], v[2]};
        }

    protected:
        std::array<T, 4> v;
    };

    template <class T, int... Mask>
    struct swizzle_vector<T, swizzle_sequence<Mask...>, false>
    {
        using simd_type = asd::simd<T, 4>;
        using underlying_type = typename simd_type::type;
        using S = swizzle_sequence<Mask...>;

        template <class U> requires(!std::is_same_v<U, T>)
        constexpr operator vector<U>() const {
            return operator vector<T>();
        }

        constexpr operator vector<T>() const {
            return operator underlying_type();
        }

        constexpr operator underlying_type() const {
            return simd_type::template mask<mk_mask4(S::MX, S::MX, S::MX, S::MX)>(simd_type::template shuffle<S::SX, S::SY, S::SZ, S::SW>(simd_type::load(v.data())));
        }

        constexpr operator const std::array<T, 4> & () const noexcept {
            return v;
        }

        constexpr operator std::array<T, 3>() const noexcept {
            return {v[0], v[1], v[2]};
        }

    protected:
        std::array<T, 4> v;
    };

    template <class T, bool Aligned>
    struct alignas(sizeof(T) * 4) point_storage<T, vector_model<Aligned>>
    {
        using implementation = simd<T, 4>;
        using underlying_type = typename implementation::type;

        member_cast(data, std::array<T, 4>);

        constexpr point_storage(underlying_type simd_value) noexcept requires (!Aligned) {
            assign(simd_value);
        }

        constexpr point_storage(underlying_type simd_value) noexcept requires (Aligned)
            : simd_storage(simd_value)
        {}

        constexpr point_storage(T x, T y, T z, T w) noexcept
            : data {x, y, z, w} {}

        template<class U = T> requires(!std::is_same_v<underlying_type, std::array<U, 4>>)
        constexpr point_storage(const std::array<U, 4> & data) noexcept
            : data(data)
        {}

        template <class Storage> requires(!std::is_same_v<Storage, point_storage>)
        constexpr void assign(const Storage & s) noexcept {
            this->data = s.data;
        }

        constexpr void assign(const point_storage & vec) noexcept {
            if constexpr (Aligned) {
                this->simd_storage = vec.simd_storage;
            } else {
                this->data = vec.data;
            }
        }

        constexpr void assign(const underlying_type & simd_value) noexcept {
            if constexpr (Aligned) {
                this->simd_storage = simd_value;
            } else {
                implementation::store(simd_value, this->data.data());
            }
        }

        template<class U = T> requires(!std::is_same_v<underlying_type, std::array<U, 4>>)
        constexpr operator underlying_type() const noexcept {
            if constexpr (Aligned) {
                return this->simd_storage;
            } else {
                return implementation::load(x, y, z, w);
            }
        }

        union
        {
            struct
            {
                T x, y, z, w;
            };

            std::array<T, 4> data;
            underlying_type simd_storage;

            swizzle_pack<T, 0, 2, 1> xzy;
            swizzle_pack<T, 1, 2, 0> yzx;
            swizzle_pack<T, 1, 0, 2> yxz;
            swizzle_pack<T, 2, 0, 1> zxy;
            swizzle_pack<T, 2, 1, 0> zyx;

            swizzle_point<T, point_model_xy> xy;
            swizzle_point<T, point_model_xz> xz;
            swizzle_point<T, point_model_yz> yz;
            swizzle_point<T, point_model_xyz> xyz;
        };
    };

//---------------------------------------------------------------------------

    using byte_vector = vector<uint8>;
    using int_vector = vector<int>;
    using float_vector = vector<float>;

    template <class T>
    using aligned_vector = vector<T, aligned_vector_storage<T>>;

    template <class T>
    using local_vector = vector<T, aligned_vector_storage<T>>;

    using local_byte_vector = local_vector<uint8>;
    using local_int_vector = local_vector<int>;
    using local_float_vector = local_vector<float>;

#ifdef ASD_SIMD_DOUBLE
    using double_vector = vector<double>;
    using local_double_vector = local_vector<double>;
#endif

    static_assert(sizeof(vector<float>) == 4 * 4, "Size of vector is incorrect");
    static_assert(sizeof(aligned_vector<float>) == 4 * 4, "Size of aligned vector is incorrect");

//---------------------------------------------------------------------------

    template <class TX, class TY, class TZ, class T = std::common_type_t<TX, TY, TZ>>
    vector(TX, TY, TZ) -> aligned_vector<T>;

    template <class TX, class TY, class TZ, class TW, class T = std::common_type_t<TX, TY, TZ, TW>>
    vector(TX, TY, TZ, TW) -> aligned_vector<T>;

    template <class T, size_t N>
    vector(const std::array<T, N> &) -> aligned_vector<T>;

    template <class T, class S>
    vector(const math::vector<T, S> &) -> vector<T, S>;

    template <class T, class S>
    vector(const math::vector<T, S> &, T) -> vector<T, S>;

    template <class T, class S>
    vector(const math::vector<T, S> &, T, T) -> vector<T, S>;

    template <class T, class S>
    vector(T, const math::vector<T, S> &) -> vector<T, S>;

    template <class T, class S>
    vector(T, T, const math::vector<T, S> &) -> vector<T, S>;

    template <class T, class S>
    vector(T, const math::vector<T, S> &, T) -> vector<T, S>;

//---------------------------------------------------------------------------

    template <class T, class S>
    struct is_point_like<vector<T, S>> : std::true_type {};

//---------------------------------------------------------------------------

    template <class T, class SA, class SB>
    constexpr aligned_vector<T> operator & (const vector<T, SA> & a, const vector<T, SB> & b) {
        return simd<T, 4>::bit_and(a, b);
    }

    template <class T, class SA, class SB>
    constexpr aligned_vector<T> operator | (const vector<T, SA> & a, const vector<T, SB> & b) {
        return simd<T, 4>::bit_or(a, b);
    }

    template <class T, class SA, class SB>
    constexpr aligned_vector<T> operator ^ (const vector<T, SA> & a, const vector<T, SB> & b) {
        return simd<T, 4>::bit_xor(a, b);
    }

    template <class T, class SA, class SB> requires(std::is_integral<T>::value)
    constexpr aligned_vector<T> operator >> (const vector<T, SA> & a, const vector<T, SB> & b) {
        return simd<T, 4>::bit_shr(a, b);
    }

    template <class T, class SA, class SB> requires(std::is_integral<T>::value)
    constexpr aligned_vector<T> operator << (const vector<T, SA> & a, const vector<T, SB> & b) {
        return simd<T, 4>::bit_shl(a, b);
    }

    template <class T, class SA, class SB>
    constexpr aligned_vector<T> bit_and(const vector<T, SA> & a, const vector<T, SB> & b) {
        return simd<T, 4>::bit_and(a, b);
    }

    template <class T, class SA, class SB>
    constexpr aligned_vector<T> bit_or(const vector<T, SA> & a, const vector<T, SB> & b) {
        return simd<T, 4>::bit_or(a, b);
    }

    template <class T, class SA, class SB>
    constexpr aligned_vector<T> bit_xor(const vector<T, SA> & a, const vector<T, SB> & b) {
        return simd<T, 4>::bit_xor(a, b);
    }

    template <class T, class SA, class SB> requires(std::is_integral<T>::value)
    constexpr aligned_vector<T> shift(const vector<T, SA> & a, const vector<T, SB> & b) { /* >> */
        return simd<T, 4>::bit_shr(a, b);
    }

    template <class T, class SA, class SB> requires(std::is_integral<T>::value)
    constexpr aligned_vector<T> unshift(const vector<T, SA> & a, const vector<T, SB> & b) { /* << */
        return simd<T, 4>::bit_shl(a, b);
    }

    template <int I, class T, class S> requires(std::is_integral<T>::value)
    constexpr aligned_vector<T> shift(const vector<T, S> & v) { /* >> */
        return simd<T, 4>::template bit_shr<I>(v);
    }

    template <int I, class T, class S> requires(std::is_integral<T>::value)
    constexpr aligned_vector<T> unshift(const vector<T, S> & v) { /* << */
        return simd<T, 4>::template bit_shl<I>(v);
    }

    template <class T, class SA, class SB>
    constexpr aligned_vector<T> dot_vector(const vector<T, SA> & a, const vector<T, SB> & b) {
        return simd<T, 4>::fill_sum(a * b);
    }

    template <class T, class SA, class SB>
    constexpr T dot(const vector<T, SA> & a, const vector<T, SB> & b) {
        return simd<T, 4>::sum(a * b);
    }

    template <class T, class SA, class SB>
    constexpr aligned_vector<T> cross(const vector<T, SA> & a, const vector<T, SB> & b) {
        return shuffle<1, 2, 0, 3>(a) * shuffle<2, 0, 1, 3>(b) - shuffle<2, 0, 1, 3>(a) * shuffle<1, 2, 0, 3>(b);
    }

    template <class T, class S>
    constexpr T max_axis_distance(const math::vector<T, S> & v) {
        aligned_vector<T> abs = simd<T, 4>::abs(v);
        return std::max({abs.x, abs.y, abs.z});
    }

    template <class T, class S>
    constexpr T sum(const math::vector<T, S> & v) {
        return simd<T, 4>::sum(v);
    }

    template <class T, class S>
    constexpr aligned_vector<T> fill_sum(const math::vector<T, S> & v) {
        return simd<T, 4>::fill_sum(v);
    }

    template <class T, class S, class SL, class SH>
    constexpr aligned_vector<T> clamp(const vector<T, S> & v, const non_deduced<vector<T, SL>> & low, const non_deduced<vector<T, SH>> & high) {
        return simd<T, 4>::min(simd<T, 4>::max(v, low), high);
    }

    template <class T, class S>
    constexpr aligned_vector<T> clamp(const vector<T, S> & v, non_deduced<T> low, non_deduced<T> high) {
        return math::clamp(v, aligned_vector(low), aligned_vector(high));
    }

    template <uint8 A, uint8 B, uint8 C, uint8 D, class T, class S> requires((A < 4 && B < 4 && C < 4 && D < 4))
    constexpr aligned_vector<T> shuffle(const math::vector<T, S> & v) {
        return simd<T, 4>::template shuffle<A, B, C, D>(v);
    }

    template <uint8 A, uint8 B, uint8 C, uint8 D, class T, class SA, class SB> requires((A < 4 && B < 4 && C < 4 && D < 4))
    constexpr aligned_vector<T> shuffle(const vector<T, SA> & a, const vector<T, SB> & b) {
        return simd<T, 4>::template shuffle2<A, B, C, D>(a, b);
    }

    template <uint8 A, uint8 B, uint8 C, uint8 D, class T, class SA, class SB> requires((A < 2 && B < 2 && C < 2 && D < 2))
    constexpr aligned_vector<T> blend(const vector<T, SA> & a, const vector<T, SB> & b) {
        return simd<T, 4>::template blend<A, B, C, D>(a, b);
    }

    template <uint8 X, uint8 Y, uint8 Z, uint8 W, class T, class S> requires((X < 2 && Y < 2 && Z < 2 && W < 2))
    aligned_vector<T> mask(const math::vector<T, S> & v) { // select some components (e.g. if X == 1 then result.x = v.x else result.x = 0)
        return simd<T, 4>::template mask<mk_mask4(X, Y, Z, W)>(v);
    }

    template <uint32 Axis, class T, class S> requires((Axis < 4))
    aligned_vector<T> mask_axis(const math::vector<T, S> & v) { // set all components of a vector to zero except of one
        return simd<T, 4>::template mask<1 << Axis>(v);
    }

    template <class T, class S>
    aligned_vector<T> mask_x(const math::vector<T, S> & v) {
        return mask_axis<0>(v);
    }

    template <class T, class S>
    aligned_vector<T> mask_y(const math::vector<T, S> & v) {
        return mask_axis<1>(v);
    }

    template <class T, class S>
    aligned_vector<T> mask_z(const math::vector<T, S> & v) {
        return mask_axis<2>(v);
    }

    template <class T, class S>
    aligned_vector<T> mask_w(const math::vector<T, S> & v) {
        return mask_axis<3>(v);
    }

    template <uint32 Axis, class T, class S> requires((Axis < 4))
    aligned_vector<T> clear_axis(const math::vector<T, S> & v) { // set a single component to zero
        return simd<T, 4>::template mask<0xF ^ (1 << Axis)>(v);
    }

    template <class T, class S>
    aligned_vector<T> clear_x(const math::vector<T, S> & v) {
        return clear_axis<0>(v);
    }

    template <class T, class S>
    aligned_vector<T> clear_y(const math::vector<T, S> & v) {
        return clear_axis<1>(v);
    }

    template <class T, class S>
    aligned_vector<T> clear_z(const math::vector<T, S> & v) {
        return clear_axis<2>(v);
    }

    template <class T, class S>
    aligned_vector<T> clear_w(const math::vector<T, S> & v) {
        return clear_axis<3>(v);
    }

    // negate some components (e.g. if X == 1 then result.x = -v.x else result.x = v.x)
    template <uint8 X, uint8 Y, uint8 Z, uint8 W, class T, class S> requires((X < 2 && Y < 2 && Z < 2 && W < 2))
    aligned_vector<T> negate(const math::vector<T, S> & v) {
        return simd<T, 4>::template negate_mask<mk_mask4(X, Y, Z, W)>(v);
    }

    // negate one component
    template <uint32 Axis, class T, class S> requires((Axis < 4))
    aligned_vector<T> negate_axis(const math::vector<T, S> & v) {
        return simd<T, 4>::template negate_mask<1 << Axis>(v);
    }

    template <class T, class S>
    aligned_vector<T> negate_x(const math::vector<T, S> & v) {
        return negate_axis<0>(v);
    }

    template <class T, class S>
    aligned_vector<T> negate_y(const math::vector<T, S> & v) {
        return negate_axis<1>(v);
    }

    template <class T, class S>
    aligned_vector<T> negate_z(const math::vector<T, S> & v) {
        return negate_axis<2>(v);
    }

    template <class T, class S>
    aligned_vector<T> negate_w(const math::vector<T, S> & v) {
        return negate_axis<3>(v);
    }

    template <uint32 Axis, class T, class S> requires((Axis < 4))
    aligned_vector<T> spread_axis(const math::vector<T, S> & v) { // get a vector filled with a single component of a src vector
        return shuffle<Axis, Axis, Axis, Axis>(v);
    }

    template <class T, class S>
    aligned_vector<T> spread_x(const math::vector<T, S> & v) {
        return spread_axis<0>(v);
    }

    template <class T, class S>
    aligned_vector<T> spread_y(const math::vector<T, S> & v) {
        return spread_axis<1>(v);
    }

    template <class T, class S>
    aligned_vector<T> spread_z(const math::vector<T, S> & v) {
        return spread_axis<2>(v);
    }

    template <class T, class S>
    aligned_vector<T> spread_w(const math::vector<T, S> & v) {
        return spread_axis<3>(v);
    }

    template <int Axis, class T, class S, class F>
    aligned_vector<T> modify_axis(const math::vector<T, S> & v, F f) noexcept {
        aligned_vector<T> out(v);
        out[Axis] = f(out[Axis]);

        return out;
    }

    //---------------------------------------------------------------------------

    template <class T, class SA, class SB>
    constexpr int compare(const vector<T, SA> & a, const vector<T, SB> & b) {
        return static_cast<int>(simd<T, 4>::sum(simd<T, 4>::sub(a, b)));
    }

    template <class T, class SA, class SB>
    constexpr aligned_vector<T> operator + (const vector<T, SA> & a, const vector<T, SB> & b) {
        return simd<T, 4>::add(a, b);
    }

    template <class T, class SA, class SB>
    constexpr aligned_vector<T> operator - (const vector<T, SA> & a, const vector<T, SB> & b) {
        return simd<T, 4>::sub(a, b);
    }

    template <class T, class SA, class SB>
    constexpr aligned_vector<T> operator * (const vector<T, SA> & a, const vector<T, SB> & b) {
        return simd<T, 4>::mul(a, b);
    }

    template <class T, class SA, class SB>
    constexpr aligned_vector<T> operator / (const vector<T, SA> & a, const vector<T, SB> & b) {
        return simd<T, 4>::div(a, b);
    }

    template <class T, class S>
    constexpr aligned_vector<T> operator + (const vector<T, S> & a, const typename asd::simd<T, 4>::type & b) {
        return simd<T, 4>::add(a, b);
    }

    template <class T, class S>
    constexpr aligned_vector<T> operator - (const vector<T, S> & a, const typename asd::simd<T, 4>::type & b) {
        return simd<T, 4>::sub(a, b);
    }

    template <class T, class S>
    constexpr aligned_vector<T> operator * (const vector<T, S> & a, const typename asd::simd<T, 4>::type & b) {
        return simd<T, 4>::mul(a, b);
    }

    template <class T, class S>
    constexpr aligned_vector<T> operator / (const vector<T, S> & a, const typename asd::simd<T, 4>::type & b) {
        return simd<T, 4>::div(a, b);
    }

    template <class T, class S>
    constexpr aligned_vector<T> operator + (const typename asd::simd<T, 4>::type & a, const vector<T, S> & b) {
        return simd<T, 4>::add(a, b);
    }

    template <class T, class S>
    constexpr aligned_vector<T> operator - (const typename asd::simd<T, 4>::type & a, const vector<T, S> & b) {
        return simd<T, 4>::sub(a, b);
    }

    template <class T, class S>
    constexpr aligned_vector<T> operator * (const typename asd::simd<T, 4>::type & a, const vector<T, S> & b) {
        return simd<T, 4>::mul(a, b);
    }

    template <class T, class S>
    constexpr aligned_vector<T> operator / (const typename asd::simd<T, 4>::type & a, const vector<T, S> & b) {
        return simd<T, 4>::div(a, b);
    }

//---------------------------------------------------------------------------

    template <class TA, class TB, class SA, class SB, class R = std::common_type_t<TA, TB>> requires(!std::is_same_v<TA, TB>)
    constexpr vector<R, aligned_vector_storage<R>> operator + (const vector<TA, SA> & a, const vector<TB, SB> & b) {
        if constexpr (std::is_same_v<R, TA>) {
            return simd<R, 4>::add(a, vector<R, aligned_vector_storage<R>>(b));
        }

        if constexpr (std::is_same_v<R, TB>) {
            return simd<R, 4>::add(vector<R, aligned_vector_storage<R>>(a), b);
        }

        return simd<R, 4>::add(vector<R, aligned_vector_storage<R>>(a), vector<R, aligned_vector_storage<R>>(b));
    }

    template <class TA, class TB, class SA, class SB, class R = std::common_type_t<TA, TB>> requires(!std::is_same_v<TA, TB>)
    constexpr vector<R, aligned_vector_storage<R>> operator - (const vector<TA, SA> & a, const vector<TB, SB> & b) {
        if constexpr (std::is_same_v<R, TA>) {
            return simd<R, 4>::sub(a, vector<R, aligned_vector_storage<R>>(b));
        }

        if constexpr (std::is_same_v<R, TB>) {
            return simd<R, 4>::sub(vector<R, aligned_vector_storage<R>>(a), b);
        }

        return simd<R, 4>::sub(vector<R, aligned_vector_storage<R>>(a), vector<R, aligned_vector_storage<R>>(b));
    }

    template <class TA, class TB, class SA, class SB, class R = std::common_type_t<TA, TB>> requires(!std::is_same_v<TA, TB>)
    constexpr vector<R, aligned_vector_storage<R>> operator * (const vector<TA, SA> & a, const vector<TB, SB> & b) {
        if constexpr (std::is_same_v<R, TA>) {
            return simd<R, 4>::mul(a, vector<R, aligned_vector_storage<R>>(b));
        }

        if constexpr (std::is_same_v<R, TB>) {
            return simd<R, 4>::mul(vector<R, aligned_vector_storage<R>>(a), b);
        }

        return simd<R, 4>::mul(vector<R, aligned_vector_storage<R>>(a), vector<R, aligned_vector_storage<R>>(b));
    }

    template <class TA, class TB, class SA, class SB, class R = std::common_type_t<TA, TB>> requires(!std::is_same_v<TA, TB>)
    constexpr vector<R, aligned_vector_storage<R>> operator / (const vector<TA, SA> & a, const vector<TB, SB> & b) {
        if constexpr (std::is_same_v<R, TA>) {
            return simd<R, 4>::div(a, vector<R, aligned_vector_storage<R>>(b));
        }

        if constexpr (std::is_same_v<R, TB>) {
            return simd<R, 4>::div(vector<R, aligned_vector_storage<R>>(a), b);
        }

        return simd<R, 4>::div(vector<R, aligned_vector_storage<R>>(a), vector<R, aligned_vector_storage<R>>(b));
    }

    template <class T, class S, math::scalar U, class R = std::common_type_t<T, U>>
    constexpr aligned_vector<R> operator + (const vector<T, S> & vec, U a) {
        if constexpr (std::is_same_v<T, R>) {
            return simd<R, 4>::add(vec, simd<R, 4>::fill(static_cast<R>(a)));
        } else {
            return simd<R, 4>::add(aligned_vector<R>(vec), simd<R, 4>::fill(static_cast<R>(a)));
        }
    }

    template <class T, class S, math::scalar U, class R = std::common_type_t<T, U>>
    constexpr aligned_vector<R> operator + (U a, const vector<T, S> & vec) {
        if constexpr (std::is_same_v<T, R>) {
            return simd<R, 4>::add(simd<R, 4>::fill(static_cast<R>(a)), vec);
        } else {
            return simd<R, 4>::add(simd<R, 4>::fill(static_cast<R>(a)), aligned_vector<R>(vec));
        }
    }

    template <class T, class S, math::scalar U, class R = std::common_type_t<T, U>>
    constexpr aligned_vector<R> operator - (const vector<T, S> & vec, U a) {
        if constexpr (std::is_same_v<T, R>) {
            return simd<R, 4>::sub(vec, simd<R, 4>::fill(static_cast<R>(a)));
        } else {
            return simd<R, 4>::sub(aligned_vector<R>(vec), simd<R, 4>::fill(static_cast<R>(a)));
        }
    }

    template <class T, class S, math::scalar U, class R = std::common_type_t<T, U>>
    constexpr aligned_vector<R> operator - (U a, const vector<T, S> & vec) {
        if constexpr (std::is_same_v<T, R>) {
            return simd<R, 4>::sub(simd<R, 4>::fill(static_cast<R>(a)), vec);
        } else {
            return simd<R, 4>::sub(simd<R, 4>::fill(static_cast<R>(a)), aligned_vector<R>(vec));
        }
    }

    template <class T, class S, math::scalar U, class R = std::common_type_t<T, U>>
    constexpr aligned_vector<R> operator * (const vector<T, S> & vec, U a) {
        if constexpr (std::is_same_v<T, R>) {
            return simd<R, 4>::mul(vec, simd<R, 4>::fill(static_cast<R>(a)));
        } else {
            return simd<R, 4>::mul(aligned_vector<R>(vec), simd<R, 4>::fill(static_cast<R>(a)));
        }
    }

    template <class T, class S, math::scalar U, class R = std::common_type_t<T, U>>
    constexpr aligned_vector<R> operator * (U a, const vector<T, S> & vec) {
        if constexpr (std::is_same_v<T, R>) {
            return simd<R, 4>::mul(simd<R, 4>::fill(static_cast<R>(a)), vec);
        } else {
            return simd<R, 4>::mul(simd<R, 4>::fill(static_cast<R>(a)), aligned_vector<R>(vec));
        }
    }

    template <class T, class S, math::scalar U, class R = std::common_type_t<T, U>>
    constexpr aligned_vector<R> operator / (const vector<T, S> & vec, U a) {
        if constexpr (std::is_same_v<T, R>) {
            return simd<R, 4>::div(vec, simd<R, 4>::fill(static_cast<R>(a)));
        } else {
            return simd<R, 4>::div(aligned_vector<R>(vec), simd<R, 4>::fill(static_cast<R>(a)));
        }
    }

    template <class T, class S, math::scalar U, class R = std::common_type_t<T, U>>
    constexpr aligned_vector<R> operator / (U a, const vector<T, S> & vec) {
        if constexpr (std::is_same_v<T, R>) {
            return simd<R, 4>::div(simd<R, 4>::fill(static_cast<R>(a)), vec);
        } else {
            return simd<R, 4>::div(simd<R, 4>::fill(static_cast<R>(a)), aligned_vector<R>(vec));
        }
    }

    template <class T>
    using vector_constants = math::constants<vector<T>>;

//---------------------------------------------------------------------------

#define implement_vector_constants(type, constant)  \
static constexpr vector<type> constant = {      \
    math::constants<type>::constant,            \
    math::constants<type>::constant,            \
    math::constants<type>::constant,            \
    math::constants<type>::constant             \
};

#define implement_vector_constants_set(type)                \
implement_vector_constants(type, pi);                   \
implement_vector_constants(type, pi2);                  \
implement_vector_constants(type, half_pi);              \
implement_vector_constants(type, half_pi3);             \
implement_vector_constants(type, inv_pi);               \
implement_vector_constants(type, inv_pi2);              \
implement_vector_constants(type, degree180);            \
implement_vector_constants(type, degree360);            \
implement_vector_constants(type, eps);                  \
implement_vector_constants(type, eps2);                 \
implement_vector_constants(type, eps3);                 \
implement_vector_constants(type, infinity);             \
implement_vector_constants(type, degrees2radians);      \
implement_vector_constants(type, radians2degrees);

    template <>
    struct constants<float_vector>
    {
        implement_vector_constants_set(float)

        static constexpr float_vector positive_x = {1.f, 0.f, 0.f, 0.f};
        static constexpr float_vector positive_y = {0.f, 1.f, 0.f, 0.f};
        static constexpr float_vector positive_z = {0.f, 0.f, 1.f, 0.f};
        static constexpr float_vector positive_w = {0.f, 0.f, 0.f, 1.f};
        static constexpr float_vector negative_x = {-1.f, 0.f, 0.f, 0.f};
        static constexpr float_vector negative_y = {0.f, -1.f, 0.f, 0.f};
        static constexpr float_vector negative_z = {0.f, 0.f, -1.f, 0.f};
        static constexpr float_vector negative_w = {0.f, 0.f, 0.f, -1.f};

        static constexpr float_vector right = {1.f, 0.f, 0.f, 0.f};
        static constexpr float_vector up = {0.f, 1.f, 0.f, 0.f};
        static constexpr float_vector forward = {0.f, 0.f, 1.f, 0.f};

        static constexpr float_vector left = {-1.f, 0.f, 0.f, 0.f};
        static constexpr float_vector down = {0.f, -1.f, 0.f, 0.f};
        static constexpr float_vector back = {0.f, 0.f, -1.f, 0.f};

        static constexpr float_vector zero = {0.f, 0.f, 0.f, 0.f};
        static constexpr float_vector one = {1.f, 1.f, 1.f, 1.f};
        static constexpr float_vector two = {2.f, 2.f, 2.f, 2.f};
        static constexpr float_vector one_xyz = {1.f, 1.f, 1.f, 0.f};
        static constexpr float_vector two_xyz = {2.f, 2.f, 2.f, 0.f};
        static constexpr float_vector minus_one = {-1.f, -1.f, -1.f, -1.f};

        static constexpr float_vector identity = {0.f, 0.f, 0.f, 0.f};

        static constexpr float_vector half = {.5f, .5f, .5f, .5f};
    };

    template <>
    struct constants<int_vector>
    {
        static constexpr int_vector positive_x = {1, 0, 0, 0};
        static constexpr int_vector positive_y = {0, 1, 0, 0};
        static constexpr int_vector positive_z = {0, 0, 1, 0};
        static constexpr int_vector positive_w = {0, 0, 0, 1};
        static constexpr int_vector negative_x = {-1, 0, 0, 0};
        static constexpr int_vector negative_y = {0, -1, 0, 0};
        static constexpr int_vector negative_z = {0, 0, -1, 0};
        static constexpr int_vector negative_w = {0, 0, 0, -1};

        static constexpr int_vector right = {1, 0, 0, 0};
        static constexpr int_vector up = {0, 1, 0, 0};
        static constexpr int_vector forward = {0, 0, 1, 0};

        static constexpr int_vector left = {-1, 0, 0, 0};
        static constexpr int_vector down = {0, -1, 0, 0};
        static constexpr int_vector back = {0, 0, -1, 0};

        static constexpr int_vector zero = {0, 0, 0, 0};
        static constexpr int_vector one = {1, 1, 1, 1};
        static constexpr int_vector two = {2, 2, 2, 2};
        static constexpr int_vector one_xyz = {1, 1, 1, 0};
        static constexpr int_vector two_xyz = {2, 2, 2, 0};
        static constexpr int_vector minus_one = {-1, -1, -1, -1};

        static constexpr int_vector identity = {0, 0, 0, 0};
    };

#ifdef ASD_SIMD_DOUBLE
    template <>
    struct constants<double_vector>
    {
        implement_vector_constants_set(double)

        static constexpr double_vector positive_x = {1.0, 0.0, 0.0, 0.0};
        static constexpr double_vector positive_y = {0.0, 1.0, 0.0, 0.0};
        static constexpr double_vector positive_z = {0.0, 0.0, 1.0, 0.0};
        static constexpr double_vector positive_w = {0.0, 0.0, 0.0, 1.0};
        static constexpr double_vector negative_x = {-1.0, 0.0, 0.0, 0.0};
        static constexpr double_vector negative_y = {0.0, -1.0, 0.0, 0.0};
        static constexpr double_vector negative_z = {0.0, 0.0, -1.0, 0.0};
        static constexpr double_vector negative_w = {0.0, 0.0, 0.0, -1.0};

        static constexpr double_vector right = {1.0, 0.0, 0.0, 0.0};
        static constexpr double_vector up = {0.0, 1.0, 0.0, 0.0};
        static constexpr double_vector forward = {0.0, 0.0, 1.0, 0.0};

        static constexpr double_vector left = {-1.0, 0.0, 0.0, 0.0};
        static constexpr double_vector down = {0.0, -1.0, 0.0, 0.0};
        static constexpr double_vector back = {0.0, 0.0, -1.0, 0.0};

        static constexpr double_vector zero = {0.0, 0.0, 0.0, 0.0};
        static constexpr double_vector one = {1.0, 1.0, 1.0, 1.0};
        static constexpr double_vector two = {2.0, 2.0, 2.0, 2.0};
        static constexpr double_vector one_xyz = {1.0, 1.0, 1.0, 0.0};
        static constexpr double_vector two_xyz = {2.0, 2.0, 2.0, 0.0};
        static constexpr double_vector minus_one = {-1.0, -1.0, -1.0, -1.0};

        static constexpr double_vector half = {.5, .5, .5, .5};

        static constexpr double_vector identity = {0.0, 0.0, 0.0, 0.0};
    };
#endif

    template
    struct vector<float>;

#ifdef ASD_SIMD_DOUBLE
    template
    struct vector<double>;
#endif

#undef implement_vector_constants
#undef implement_vector_constants_set

    template <class T, class Storage>
    struct coefficients<vector<T, Storage>>
    {
        static constexpr vector<T, Storage> sin[] = {
            {coefficients<T>::sin[0], coefficients<T>::sin[0], coefficients<T>::sin[0], coefficients<T>::sin[0]},
            {coefficients<T>::sin[1], coefficients<T>::sin[1], coefficients<T>::sin[1], coefficients<T>::sin[1]},
            {coefficients<T>::sin[2], coefficients<T>::sin[2], coefficients<T>::sin[2], coefficients<T>::sin[2]},
            {coefficients<T>::sin[3], coefficients<T>::sin[3], coefficients<T>::sin[3], coefficients<T>::sin[3]},
            {coefficients<T>::sin[4], coefficients<T>::sin[4], coefficients<T>::sin[4], coefficients<T>::sin[4]},
        };

        static constexpr vector<T, Storage> cos[] = {
            {coefficients<T>::cos[0], coefficients<T>::cos[0], coefficients<T>::cos[0], coefficients<T>::cos[0]},
            {coefficients<T>::cos[1], coefficients<T>::cos[1], coefficients<T>::cos[1], coefficients<T>::cos[1]},
            {coefficients<T>::cos[2], coefficients<T>::cos[2], coefficients<T>::cos[2], coefficients<T>::cos[2]},
            {coefficients<T>::cos[3], coefficients<T>::cos[3], coefficients<T>::cos[3], coefficients<T>::cos[3]},
            {coefficients<T>::cos[4], coefficients<T>::cos[4], coefficients<T>::cos[4], coefficients<T>::cos[4]},
            {coefficients<T>::cos[5], coefficients<T>::cos[5], coefficients<T>::cos[5], coefficients<T>::cos[5]},
        };
    };

    //---------------------------------------------------------------------------

    template <class T, class S>
    aligned_vector<T> abs(const vector<T, S> & x) {
        return simd<T, 4>::abs(x);
    }

    template <class T, class S>
    aligned_vector<T> sqr(const vector<T, S> & x) {
        return simd<T, 4>::sqr(x);
    }

    template <class T, class S>
    aligned_vector<T> sqrt(const vector<T, S> & x) {
        return simd<T, 4>::sqrt(x);
    }

    template <class T, class S>
    aligned_vector<T> mod(const vector<T, S> & x, const vector<T, S> & y) {
        return simd<T, 4>::mod(x, y);
    }

    template <class T, class SA, class SB> requires(!std::is_same_v<SA, SB>)
    aligned_vector<T> mod(const vector<T, SA> & x, const vector<T, SB> & y) {
        return simd<T, 4>::mod(x, y);
    }

    template <class T, class S>
    aligned_vector<T> avg(const vector<T, S> & x, const vector<T, S> & y) {
        return vector_constants<T>::half * (x + y);
    }

    template <class T, class SA, class SB> requires(!std::is_same_v<SA, SB>)
    aligned_vector<T> avg(const vector<T, SA> & x, const vector<T, SB> & y) {
        return vector_constants<T>::half * (x + y);
    }

    template <class TA, class TB, class SA, class SB, class E = float>
    constexpr bool fuzzy_equal(const vector<TA, SA> & a, const vector<TB, SB> & b, E eps = math::eps) noexcept {
        return
            math::fuzzy_equal(a.x, b.x, eps) &&
            math::fuzzy_equal(a.y, b.y, eps) &&
            math::fuzzy_equal(a.z, b.z, eps) &&
            math::fuzzy_equal(a.w, b.w, eps);
    }

    template <class T, class S>
    aligned_vector<T> min(const vector<T, S> & a, const vector<T, S> & b) {
        return simd<T, 4>::min(a, b);
    }

    template <class T, class SA, class SB> requires(!std::is_same_v<SA, SB>)
    aligned_vector<T> min(const vector<T, SA> & a, const vector<T, SB> & b) {
        return simd<T, 4>::min(a, b);
    }

    template <class T, class S>
    aligned_vector<T> max(const vector<T, S> & a, const vector<T, S> & b) {
        return simd<T, 4>::max(a, b);
    }

    template <class T, class SA, class SB> requires(!std::is_same_v<SA, SB>)
    aligned_vector<T> max(const vector<T, SA> & a, const vector<T, SB> & b) {
        return simd<T, 4>::max(a, b);
    }

    template <class T, class S>
    aligned_vector<T> invert(const vector<T, S> & v) {
        return simd<T, 4>::invert(v);
    }

    template <class T, class S>
    aligned_vector<T> trunc(const vector<T, S> & v) {
        return simd<T, 4>::trunc(v);
    }

    template <class T, class S> requires(std::is_floating_point_v<T>)
    aligned_vector<T> floor(const vector<T, S> & v) {
        return simd<T, 4>::floor(v);
    }

    template <class T, class S> requires(std::is_integral_v<T>)
    aligned_vector<T> floor(const vector<T, S> & v) {
        return v;
    }

    template <class R, class T, class S> requires(!std::is_same_v<R, T>)
    aligned_vector<R> floor(const vector<T, S> & v) {
        return aligned_vector<R>{aligned_vector<T>{simd<T, 4>::floor(v)}};
    }

    inline aligned_vector<float> floor(const typename simd_type<float, 4>::type & v) {
        return aligned_vector<float>{simd<float, 4>::floor(v)};
    }

#ifdef ASD_SIMD_DOUBLE
    inline aligned_vector<double> floor(const typename simd_type<double, 4>::type & v) {
        return aligned_vector<double>{simd<double, 4>::floor(v)};
    }
#endif

    template <class T, class S, class U, math::scalar R = std::common_type_t<T, U>> requires(std::is_floating_point_v<T>)
    aligned_vector<R> floor_div(const vector<T, S> & a, U b) {
        return math::floor(a / b);
    }

    template <class TA, class SA, class TB = TA, class SB = SA, class T = std::common_type_t<TA, TB>> requires(std::is_floating_point_v<T>)
    aligned_vector<T> floor_div(const vector<TA, SA> & a, const vector<TB, SB> b) {
        return math::floor(a / b);
    }

    template <class T, class S, class U, math::scalar R = std::common_type_t<T, U>> requires(std::is_integral_v<T>)
    aligned_vector<R> floor_div(const vector<T, S> & a, U b) {
        if constexpr (std::is_same_v<float, R>) {
            return math::floor(aligned_vector<float>(a) / aligned_vector<float>(b));
        } else {
            return aligned_vector<R>(math::floor(aligned_vector<float>(a) / aligned_vector<float>(b)));
        }
    }

    template <class TA, class SA, class TB = TA, class SB = SA, class T = std::common_type_t<TA, TB>> requires(std::is_integral_v<T>)
    aligned_vector<T> floor_div(const vector<TA, SA> & a, const vector<TB, SB> b) {
        return math::floor(aligned_vector<float>(a) / aligned_vector<float>(b));
    }

    template <class R, class T, class S, math::scalar U> requires(!std::is_same_v<R, T> && std::is_floating_point_v<T>)
    aligned_vector<R> floor_div(const vector<T, S> & a, U b) {
        return aligned_vector<R>{math::floor(a / b)};
    }

    template <math::scalar R, class TA, class SA, class TB = TA, class SB = SA, class T = std::common_type_t<TA, TB>> requires(!std::is_same_v<R, TA> && std::is_floating_point_v<T>)
    aligned_vector<R> floor_div(const vector<TA, SA> & a, const vector<TB, SB> b) {
        return aligned_vector<R>{math::floor(a / b)};
    }

    template <math::scalar R, class T, class S, class U> requires(!std::is_same_v<R, T> && std::is_integral_v<T>)
    aligned_vector<R> floor_div(const vector<T, S> & a, U b) {
        if constexpr (std::is_same_v<float, R>) {
            return math::floor(aligned_vector<float>(a) / aligned_vector<float>(b));
        } else {
            return aligned_vector<R>(math::floor(aligned_vector<float>(a) / aligned_vector<float>(b)));
        }
    }

    template <math::scalar R, class TA, class SA, class TB = TA, class SB = SA> requires(!std::is_same_v<R, TA> && std::is_integral_v<R>)
    aligned_vector<R> floor_div(const vector<TA, SA> & a, const vector<TB, SB> b) {
        return aligned_vector<R>(math::floor(aligned_vector<float>(a) / aligned_vector<float>(b)));
    }

    template <class T, class S, class U, math::scalar R = std::common_type_t<T, U>>
    aligned_vector<R> floor_mod(const vector<T, S> & a, U b) {
        return {a - math::floor_div(a, b) * b};
    }

    template <class T, class S>
    aligned_vector<T> floor_mod(const vector<T, S> & a, const non_deduced<vector<T, S>> & b) {
        return a - math::floor_div(a, b) * b;
    }

    template <class TA, class SA, class TB, class SB, class T = std::common_type_t<TA, TB>> requires((!std::is_same_v<TA, TB> || !std::is_same_v<SA, SB>) && std::is_floating_point_v<T>)
    aligned_vector<T> floor_mod(const vector<TA, SA> & a, const vector<TB, SB> b) {
        return {a - math::floor_div(a, b) * b};
    }

    template <class T, class S>
    aligned_vector<T> ceil(const vector<T, S> & v) {
        return simd<T, 4>::ceil(v);
    }

    template <class T, class S>
    aligned_vector<T> round(const vector<T, S> & v) {
        return simd<T, 4>::round(v);
    }

    // trigonometric functions are implemented via partial Tailor's series expansion computation

    template <class T, class S>
    aligned_vector<T> partial_tailor_series_sine(const vector<T, S> & x) {
        using coeffs = coefficients<vector<T>>;
        return x * (coeffs::sin[0] + x * (coeffs::sin[1] + x * (coeffs::sin[2] + x * (coeffs::sin[3] + x * (coeffs::sin[4])))));
    }

    template <class T, class S>
    aligned_vector<T> partial_tailor_series_cosine(const vector<T, S> & x) {
        using coeffs = coefficients<vector<T>>;
        return x * (coeffs::cos[0] + x * (coeffs::cos[1] + x * (coeffs::cos[2] + x * (coeffs::cos[3] + x * (coeffs::cos[4] + x * (coeffs::cos[5]))))));
    }

    template <class T, class S>
    aligned_vector<T> sin(const vector<T, S> & angle) {
        using simd_type = simd<T, 4>;

        const auto x = normalize_angle(angle);
        const auto sign = simd_type::sign(x);
        const auto comp = simd_type::cmple(simd_type::bit_andnot(sign, x), constants<vector<T>>::half_pi);

        const auto x0 = simd_type::bit_or(simd_type::bit_and(comp, x), simd_type::bit_andnot(comp, simd_type::sub(simd_type::bit_or(constants<vector<T>>::pi, sign), x)));
        const aligned_vector<T> xs = simd_type::sqr(x0);

        return x0 + x0 * partial_tailor_series_sine(xs);
    }

    template <class T, class S>
    aligned_vector<T> cos(const vector<T, S> & angle) {
        using simd_type = simd<T, 4>;

        const auto x = normalize_angle(angle);
        const auto sign = simd_type::sign(x);
        const auto comp = simd_type::cmple(simd_type::bit_andnot(sign, x), constants<vector<T>>::half_pi);

        const auto c0 = simd_type::bit_or(simd_type::bit_and(comp, vector_constants<T>::one), simd_type::bit_andnot(comp, vector_constants<T>::minus_one));
        const auto x0 = simd_type::bit_or(simd_type::bit_and(comp, x), simd_type::bit_andnot(comp, simd_type::sub(simd_type::bit_or(constants<vector<T>>::pi, sign), x)));
        const aligned_vector<T> xs = simd_type::sqr(x0);

        return c0 + c0 * partial_tailor_series_cosine(xs);
    }

    template <class T, class SA, class SB, class SC>
    void sincos(const vector<T, SA> & angle, vector<T, SB> & s, vector<T, SC> & c) {
        using simd_type = simd<T, 4>;

        const auto x = normalize_angle(angle);
        const auto sign = simd_type::sign(x);
        const auto comp = simd_type::cmple(simd_type::bit_andnot(sign, x), constants<vector<T>>::half_pi);

        const auto c0 = simd_type::bit_or(simd_type::bit_and(comp, vector_constants<T>::one), simd_type::bit_andnot(comp, vector_constants<T>::minus_one));
        const auto x0 = simd_type::bit_or(simd_type::bit_and(comp, x), simd_type::bit_andnot(comp, simd_type::sub(simd_type::bit_or(constants<vector<T>>::pi, sign), x)));
        const aligned_vector<T> xs = simd_type::sqr(x0);

        s = x0 + x0 * partial_tailor_series_sine(xs);
        c = c0 + c0 * partial_tailor_series_cosine(xs);
    }

    // put an angle into [-pi; pi] range
    template <class T, class S>
    constexpr aligned_vector<T> normalize_angle(const vector<T, S> & angle) {
        return angle - round(angle * constants<vector<T>>::inv_pi2) * constants<vector<T>>::pi2;
    }

    // returns [ sin(angle), cos(angle), -sin(angle), 0 ]
    template <class T>
    constexpr aligned_vector<T> trigon(T angle) {
        return sin(aligned_vector<T>{angle, angle + constants<T>::half_pi, -angle, 0});
    }

    //---------------------------------------------------------------------------

    constexpr aligned_vector<float> vec() {
        return vector_constants<float>::zero;
    }

    constexpr aligned_vector<float> vec(float x, float y, float z) {
        return {x, y, z, 1};
    }

    constexpr aligned_vector<float> vec(float x, float y, float z, float w) {
        return {x, y, z, w};
    }

#ifdef ASD_SIMD_DOUBLE
    constexpr aligned_vector<double> vecd() {
        return vector_constants<double>::zero;
    }

    constexpr aligned_vector<double> vecd(double x, double y, double z) {
        return {x, y, z, 1};
    }

    constexpr aligned_vector<double> vecd(double x, double y, double z, double w) {
        return {x, y, z, w};
    }
#endif

    template <class T, class S = aligned_vector_storage<T>>
    constexpr T square_magnitude(const vector<T, S> & v) {
        return math::dot(v, v);
    }

    template <class T, class S = aligned_vector_storage<T>>
    constexpr T square_length(const vector<T, S> & v) noexcept {
        return math::square_magnitude(v);
    }

    template <class T, class S = aligned_vector_storage<T>>
    constexpr aligned_vector<T> square_magnitude_vector(const vector<T, S> & v) {
        return math::dot_vector(v, v);
    }

    template <class T, class S = aligned_vector_storage<T>>
    constexpr T magnitude(const vector<T, S> & v) {
        return math::sqrt(math::square_magnitude(v));
    }

    template <class T, class S = aligned_vector_storage<T>>
    constexpr T length(const vector<T, S> & v) noexcept {
        return math::magnitude(v);
    }

    template <class T, class S = aligned_vector_storage<T>>
    constexpr aligned_vector<T> magnitude_vector(const vector<T, S> & v) {
        return simd<T, 4>::sqrt(math::square_magnitude_vector(v));
    }

    template <class T, class S = aligned_vector_storage<T>>
    aligned_vector<T> normalize(const vector<T, S> & v) noexcept {
        return simd<T, 4>::div(v, math::magnitude_vector(v));
    }

    template <class T, class S = aligned_vector_storage<T>>
    aligned_vector<T> unit(const vector<T, S> & v) noexcept {
        return simd<T, 4>::div(v, math::magnitude_vector(v));
    }

    template <class TX, class TY, class TZ, class TW = TX, class T = std::common_type_t<TX, TY, TZ, TW>>
    aligned_vector<T> unit_vector(TX x, TY y, TZ z, TW w = TW{}) noexcept {
        return unit(aligned_vector<T>(x, y, z, w));
    }

    template <class T, class S = aligned_vector_storage<T>>
    std::pair<aligned_vector<T>, T> decompose(const vector<T, S> & v) noexcept {
        const auto m = math::magnitude_vector(v);
        return {simd<T, 4>::div(v, m), m.x};
    }

    template <class T, class SA, class SB>
    constexpr T square_distance_to(const vector<T, SA> & a, const vector<T, SB> & b) {
        return simd<T, 4>::sum(simd<T, 4>::sqr(simd<T, 4>::sub(a, b)));
    }

    template <class T, class SA, class SB>
    constexpr T distance_to(const vector<T, SA> & a, const vector<T, SB> & b) {
        return math::sqrt(math::square_distance_to(a, b));
    }

    template <class T, class SA, class SB>
    constexpr aligned_vector<T> direction_to(const vector<T, SA> & a, const vector<T, SB> & b) {
        return math::unit(b - a);
    }

    template <class T, class SA, class SB>
    constexpr T distance_to_axis(const vector<T, SA> & a, const vector<T, SB> & b, int axis) {
        return math::abs(a[axis] - b[axis]);
    }

    template <class T, class S>
    constexpr int main_axis(const vector<T, S> & v) {
        return v.x >= v.y ? 0 : v.y >= v.z ? 1 : 2;
    }

    template <class T, class SA = aligned_vector_storage<T>, class SB = SA>
    constexpr T angle(const vector<T, SA> & a, const vector<T, SB> & b) noexcept {
        return math::acos(math::dot(a, b));
    }

    template <class T, class S>
    constexpr T pitch(const vector<T, S> & v) noexcept {
        return -math::asin(v.y / math::length(v));
    }

    template <class T, class S>
    constexpr T yaw(const vector<T, S> & v, const math::point_xz<T> & forward = math::point_xz<T>{0.0f, 1.0f}) noexcept {
        return math::signed_angle(math::normalize(v.xz.point()), forward);
    }

    template <class T, class S>
    constexpr math::point<T> pitch_yaw(const vector<T, S> & v, const math::point_xz<T> & forward = math::point_xz<T>{0.0f, 1.0f}) noexcept {
        return {
            -math::asin(v.y / math::length(v)),
            math::signed_angle(math::normalize(v.xz.point()), forward)
        };
    }

    template <class T, class SA = aligned_vector_storage<T>, class SB = SA, class SC = SA>
    constexpr auto mixed_product(const vector<T, SA> & a, const vector<T, SB> & b, const vector<T, SC> & c) {
        return math::dot(a, math::cross(b, c));
    }

    template <class T, class SA = aligned_vector_storage<T>, class SB = SA, class SC = SA>
    constexpr bool are_collinear(const vector<T, SA> & a, const vector<T, SB> & b, const vector<T, SC> & c) {
        return math::square_magnitude(math::cross(b - a, c - a)) < constants<T>::eps2;
    }

    template <class T, class SA = aligned_vector_storage<T>, class SB = SA, class SC = SA, class S4 = SA>
    constexpr bool are_complanar(const vector<T, SA> & a, const vector<T, SB> & b, const vector<T, SC> & c, const vector<T, S4> & d) {
        return abs(mixed_product(b - a, c - a, d - a)) < constants<T>::eps3;
    }

    template <class T, class SA = aligned_vector_storage<T>, class SB = SA>
    constexpr aligned_vector<T> rotate_around(const vector<T, SA> & direction, const vector<T, SB> & axis, T angle) noexcept {
        const auto v = math::shuffle<0, 0, 0, 1>(trigon(angle * 0.5f)) * math::blend<0, 0, 0, 1>(axis, vector_constants<T>::positive_w);
        const auto xyz = math::clear_w(v);
        const auto t = 2 * math::cross(xyz, direction);

        return direction + v.w * t + cross(xyz, t);
    }
}

namespace asd
{
    inline namespace vector_literals
    {
        constexpr math::aligned_vector<float> operator"" _v(long double v) {
            return {static_cast<float>(v)};
        }

    #ifdef ASD_SIMD_DOUBLE
        constexpr math::aligned_vector<double> operator"" _vd(long double v) {
            return {static_cast<double>(v)};
        }
    #endif
    }
}

namespace std
{
    template <size_t Idx, class T, class S>
    struct tuple_element<Idx, asd::math::vector<T, S>>
    {
        using type = T;
    };

    template <class T, class S>
    struct tuple_size<asd::math::vector<T, S>> : integral_constant<size_t, 4> {};

    template <size_t Idx, class T, class S>
    constexpr T get(const asd::math::vector<T, S> & v) {
        return v[Idx];
    }

    template <size_t Idx, class T, class S>
    constexpr T & get(asd::math::vector<T, S> & v) {
        return v[Idx];
    }
}
