//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <array>
#include <type_traits>

#include <math/math.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace math
    {
        template <class T>
        concept point_model = requires {
            typename T::grid_type;
            T::dimensions;
        };

        template <class T, class Model>
        struct point_storage {};

//---------------------------------------------------------------------------

        struct cartesian_grid{};

        struct point_model_xy
        {
            using grid_type = cartesian_grid;
            static constexpr size_t dimensions = 2;
        };

        struct point_model_xz
        {
            using grid_type = cartesian_grid;
            static constexpr size_t dimensions = 2;
        };

        struct point_model_yz
        {
            using grid_type = cartesian_grid;
            static constexpr size_t dimensions = 2;
        };

        struct point_model_xyz
        {
            using grid_type = cartesian_grid;
            static constexpr size_t dimensions = 3;
        };

        template <class T>
        struct point_storage<T, point_model_xy>
        {
            constexpr point_storage(T x, T y) noexcept : data{x, y} {}

            union {
                std::array<T, 2> data;

                struct {
                    T x, y;
                };
            };
        };

        template <class T>
        struct point_storage<T, point_model_xz>
        {
            constexpr point_storage(T x, T z) noexcept : data{x, z} {}

            union {
                std::array<T, 2> data;

                struct {
                    T x, z;
                };
            };
        };

        template <class T>
        struct point_storage<T, point_model_yz>
        {
            constexpr point_storage(T y, T z) noexcept : data{y, z} {}

            union {
                std::array<T, 2> data;

                struct {
                    T y, z;
                };
            };
        };

        template <class T>
        struct point_storage<T, point_model_xyz>
        {
            constexpr point_storage(T x, T y, T z) noexcept : data{x, y, z} {}

            union {
                std::array<T, 3> data;

                struct {
                    T x, y, z;
                };
            };
        };

//---------------------------------------------------------------------------

        template <class T, class Model>
        struct swizzle_point
        {};

        template <class T, class Model = point_model_xy>
        struct point;

        template <template <class, class> class Point, class T, math::point_model Model, size_t Dimensions = Model::dimensions>
        struct point_base {};

        template <class T>
        struct is_point_like : std::false_type {};

        template <template <class, class> class Point, class T, math::point_model Model>
        struct is_point_like<Point<T, Model>> :
            std::is_base_of<point_base<Point, T, Model>, Point<T, Model>> {};

        template <class T>
        constexpr auto is_point_like_v = is_point_like<T>::value;

        template <class T>
        concept point_like = is_point_like_v<T>;

        template <class A, class B>
        struct has_compatible_model : std::false_type {};

        template <template <class, class> class PointA, template <class, class> class PointB, class A, class B, class Model>
        struct has_compatible_model<PointA<A, Model>, PointB<B, Model>> : std::true_type {};

        template <class A, class B>
        constexpr auto has_compatible_model_v = has_compatible_model<A, B>::value;

        template <class A, class B>
        struct is_compatible_point_like : std::bool_constant<
            is_point_like_v<A> &&
            is_point_like_v<B> &&
            has_compatible_model_v<A, B>
        > {};

        template <class A, class B>
        constexpr auto is_compatible_point_like_v = is_compatible_point_like<A, B>::value;

        template <template <class, class> class Point, class T, class Model>
        struct point_base<Point, T, Model, 2> : point_storage<T, Model>
        {
            static_assert(point_model<Model>, "Point model should declare grid_type");

            using coord_type = T;
            using value_type = T;
            using base_type = point_storage<T, Model>;
            using model_type = Model;

            constexpr point_base() noexcept :
                base_type{static_cast<T>(0), static_cast<T>(0)} {}

            constexpr point_base(const point_base & pt) noexcept : base_type{pt} {}

            template <class U> requires (std::is_constructible_v<T, U>)
            explicit constexpr point_base(U val) noexcept :
                base_type{static_cast<T>(val), static_cast<T>(val)} {}

            template <class U, class V> requires (std::is_constructible_v<T, U> && std::is_constructible_v<T, V>)
            constexpr point_base(U a, V b) noexcept :
                base_type{static_cast<T>(a), static_cast<T>(b)} {}

            template <class U> requires (std::is_constructible_v<T, U>)
            constexpr point_base(const std::initializer_list<U> & pt) noexcept :
                base_type{
                    pt.size() > 0 ? static_cast<T>(*pt.begin()) : static_cast<T>(0),
                    pt.size() > 1 ? static_cast<T>(*(pt.begin() + 1)) : pt.size() > 0 ? static_cast<T>(*pt.begin()) : static_cast<T>(0)
                } {}

            template <class U> requires (std::is_constructible_v<T, U>)
            constexpr point_base(const U (& pt)[2]) noexcept :
                base_type{static_cast<T>(pt[0]), static_cast<T>(pt[1])} {}

            template <template <class, class> class PointLike, class U> requires (!std::is_same_v<Point<T, Model>, PointLike<U, Model>>)
            explicit constexpr point_base(const PointLike<U, Model> & pt) noexcept :
                base_type{static_cast<T>(pt.data[0]), static_cast<T>(pt.data[1])} {}

            constexpr point_base & operator = (const point_base & pt) noexcept = default;

            template <class U> requires (!std::is_same_v<T, U> && std::is_constructible_v<T, U>)
            constexpr point_base & operator = (const Point<U, Model> & pt) noexcept {
                this->data[0] = static_cast<T>(pt.data[0]);
                this->data[1] = static_cast<T>(pt.data[1]);

                return *this;
            }

            template <class U>
            constexpr int compare(const Point<U, Model> & pt) const noexcept {
                if (auto i = math::compare(this->data[0], pt.data[0]); i != 0) {
                    return i;
                }

                return math::compare(this->data[1], pt.data[1]);
            }

            template <class U>
            constexpr int compare(const swizzle_point<U, Model> & pt) const noexcept {
                if (auto i = math::compare(this->data[0], pt[0]); i != 0) {
                    return i;
                }

                return math::compare(this->data[1], pt[1]);
            }

            friend constexpr bool operator == (const Point<T, Model> & a, const Point<T, Model> & b) noexcept {
                return math::equal(a.data[0], b.data[0]) && math::equal(a.data[1], b.data[1]);
            }

            friend constexpr bool operator != (const Point<T, Model> & a, const Point<T, Model> & b) noexcept {
                return math::not_equal(a.data[0], b.data[0]) || math::not_equal(a.data[1], b.data[1]);
            }

            friend constexpr bool operator == (const Point<T, Model> & a, const swizzle_point<T, Model> & b) noexcept {
                return math::equal(a.data[0], b[0]) && math::equal(a.data[1], b[1]);
            }

            friend constexpr bool operator != (const Point<T, Model> & a, const swizzle_point<T, Model> & b) noexcept {
                return math::not_equal(a.data[0], b[0]) || math::not_equal(a.data[1], b[1]);
            }

            template <class F>
            constexpr Point<T, Model> & apply(F f) noexcept {
                f(this->data[0]);
                f(this->data[1]);

                return static_cast<Point<T, Model> &>(*this);
            }

            template <class F, class U> requires is_compatible_point_like_v<Point<T, Model>, U>
            constexpr Point<T, Model> & apply(F f, const U & pt) noexcept {
                f(this->data[0], static_cast<T>(pt.data[0]));
                f(this->data[1], static_cast<T>(pt.data[1]));

                return static_cast<Point<T, Model> &>(*this);
            }

            template <class F, math::scalar U>
            constexpr Point<T, Model> & apply(F f, U value) noexcept {
                f(this->data[0], value);
                f(this->data[1], value);

                return static_cast<Point<T, Model> &>(*this);
            }

            template <class F>
            constexpr auto map(F f) const noexcept {
                using R = decltype(std::declval<F>()(std::declval<T>()));
                return Point<R, Model>{f(this->data[0]), f(this->data[1])};
            }

            template <class R, class F>
            constexpr Point<R, Model> map(F f) const noexcept {
                return {static_cast<R>(f(this->data[0])), static_cast<R>(f(this->data[1]))};
            }

            template <class F, class U> requires is_compatible_point_like_v<Point<T, Model>, U>
            constexpr auto map(F f, const U & v) const noexcept {
                using R = decltype(std::declval<F>()(std::declval<T>(), std::declval<U>().data[0]));
                return Point<R, Model>{f(this->data[0], v.data[0]), f(this->data[1], v.data[1])};
            }

            template <class R, class F, class U> requires is_compatible_point_like_v<Point<T, Model>, U>
            constexpr Point<R, Model> map(F f, const U & v) const noexcept {
                return {static_cast<R>(f(this->data[0], v.data[0])), static_cast<R>(f(this->data[1], v.data[1]))};
            }

            template <class F, math::scalar U>
            constexpr auto map(F f, U v) const noexcept {
                using R = decltype(std::declval<F>()(std::declval<T>(), std::declval<U>()));
                return Point<R, Model>{f(this->data[0], v), f(this->data[1], v)};
            }

            template <class R, class F, math::scalar U>
            constexpr Point<R, Model> map(F f, U v) const noexcept {
                return {static_cast<R>(f(this->data[0], v)), static_cast<R>(f(this->data[1], v))};
            }

            template <class F>
            constexpr auto reduce(F f) const noexcept {
                return f(this->data[0], this->data[1]);
            }

            template <class M, class R>
            constexpr auto map_reduce(M m, R r) const noexcept {
                return r(m(this->data[0]), m(this->data[1]));
            }

            template <class M, class R, class U> requires is_compatible_point_like_v<Point<T, Model>, U>
            constexpr auto map_reduce(M m, R r, const U & v) const noexcept {
                return r(m(this->data[0], v.data[0]), m(this->data[1], v.data[1]));
            }

            template <class M, class R, math::scalar U>
            constexpr auto map_reduce(M m, R r, U v) const noexcept {
                return r(m(this->data[0], v), m(this->data[1], v));
            }
        };

        template <template <class, class> class Point, class T, class Model>
        struct point_base<Point, T, Model, 3> : point_storage<T, Model>
        {
            static_assert(point_model<Model>, "Point model should declare grid_type");

            using coord_type = T;
            using value_type = T;
            using base_type = point_storage<T, Model>;
            using model_type = Model;

            constexpr point_base() noexcept :
                base_type{static_cast<T>(0), static_cast<T>(0), static_cast<T>(0)} {}

            constexpr point_base(const point_base & pt) noexcept : base_type{pt} {}

            template <class U> requires (std::is_constructible_v<T, U>)
            explicit constexpr point_base(U val) noexcept :
                base_type{static_cast<T>(val), static_cast<T>(val), static_cast<T>(val)} {}

            template <class U, class V, class W> requires (std::is_constructible_v<T, U> && std::is_convertible_v<V, T> && std::is_convertible_v<W, T>)
            constexpr point_base(U a, V b, V c) noexcept :
                base_type{static_cast<T>(a), static_cast<T>(b), static_cast<T>(c)} {}

            template <class U> requires (std::is_constructible_v<T, U>)
            constexpr point_base(const std::initializer_list<U> & pt) noexcept :
                base_type{
                    pt.size() > 0 ? static_cast<T>(*pt.begin()) : static_cast<T>(0),
                    pt.size() > 1 ? static_cast<T>(*(pt.begin() + 1)) : pt.size() > 0 ? static_cast<T>(*pt.begin()) : static_cast<T>(0),
                    pt.size() > 2 ? static_cast<T>(*(pt.begin() + 2)) : pt.size() > 0 ? static_cast<T>(*pt.begin()) : static_cast<T>(0)
                } {}

            template <class U> requires (std::is_constructible_v<T, U>)
            constexpr point_base(const U (& pt)[3]) noexcept :
                base_type{static_cast<T>(pt[0]), static_cast<T>(pt[1]), static_cast<T>(pt[2])} {}

            template <class U> requires (!std::is_same_v<T, U> && std::is_constructible_v<T, U>)
            explicit constexpr point_base(const Point<U, Model> & pt) noexcept :
                base_type{static_cast<T>(pt.data[0]), static_cast<T>(pt.data[1]), static_cast<T>(pt.data[2])} {}

            constexpr point_base & operator = (const point_base & pt) noexcept = default;

            template <class U> requires (!std::is_same_v<T, U> && std::is_constructible_v<T, U>)
            constexpr point_base & operator = (const Point<U, Model> & pt) noexcept {
                this->data[0] = static_cast<T>(pt.data[0]);
                this->data[1] = static_cast<T>(pt.data[1]);
                this->data[2] = static_cast<T>(pt.data[2]);

                return *this;
            }

            template <class U>
            constexpr int compare(const Point<U, Model> & pt) const noexcept {
                if (auto i = math::compare(this->data[0], pt.data[0]); i != 0) {
                    return i;
                }

                if (auto i = math::compare(this->data[1], pt.data[1]); i != 0) {
                    return i;
                }

                return math::compare(this->data[2], pt.data[2]);
            }

            template <class U>
            constexpr int compare(const swizzle_point<U, Model> & pt) const noexcept {
                if (auto i = math::compare(this->data[0], pt[0]); i != 0) {
                    return i;
                }

                if (auto i = math::compare(this->data[1], pt[1]); i != 0) {
                    return i;
                }

                return math::compare(this->data[2], pt[2]);
            }

            friend constexpr bool operator == (const Point<T, Model> & a, const Point<T, Model> & b) noexcept {
                return math::equal(a.data[0], b.data[0]) && math::equal(a.data[1], b.data[1]) && math::equal(a.data[2], b.data[2]);
            }

            friend constexpr bool operator != (const Point<T, Model> & a, const Point<T, Model> & b) noexcept {
                return math::not_equal(a.data[0], b.data[0]) || math::not_equal(a.data[1], b.data[1]) || math::not_equal(a.data[2], b.data[2]);
            }

            friend constexpr bool operator == (const Point<T, Model> & a, const swizzle_point<T, Model> & b) noexcept {
                return math::equal(a.data[0], b[0]) && math::equal(a.data[1], b[1]) && math::equal(a.data[2], b[2]);
            }

            friend constexpr bool operator != (const Point<T, Model> & a, const swizzle_point<T, Model> & b) noexcept {
                return math::not_equal(a.data[0], b[0]) || math::not_equal(a.data[1], b[1]) || math::not_equal(a.data[2], b[2]);
            }

            template <class F>
            constexpr Point<T, Model> & apply(F f) noexcept {
                f(this->data[0]);
                f(this->data[1]);
                f(this->data[2]);

                return static_cast<Point<T, Model> &>(*this);
            }

            template <class F, class U> requires is_compatible_point_like_v<Point<T, Model>, U>
            constexpr Point<T, Model> & apply(F f, const U & pt) noexcept {
                f(this->data[0], static_cast<T>(pt.data[0]));
                f(this->data[1], static_cast<T>(pt.data[1]));
                f(this->data[2], static_cast<T>(pt.data[2]));

                return static_cast<Point<T, Model> &>(*this);
            }

            template <class F, math::scalar U>
            constexpr Point<T, Model> & apply(F f, U value) noexcept {
                f(this->data[0], value);
                f(this->data[1], value);
                f(this->data[2], value);

                return static_cast<Point<T, Model> &>(*this);
            }

            template <class F>
            constexpr auto map(F f) const noexcept {
                using R = decltype(std::declval<F>()(std::declval<T>()));
                return Point<R, Model>{f(this->data[0]), f(this->data[1]), f(this->data[2])};
            }

            template <class R, class F>
            constexpr Point<R, Model> map(F f) const noexcept {
                return {static_cast<R>(f(this->data[0])), static_cast<R>(f(this->data[1])), static_cast<R>(f(this->data[2]))};
            }

            template <class F, class U> requires is_compatible_point_like_v<Point<T, Model>, U>
            constexpr auto map(F f, const U & v) const noexcept {
                using R = decltype(std::declval<F>()(std::declval<T>(), std::declval<U>().data[0]));
                return Point<R, Model>{f(this->data[0], v.data[0]), f(this->data[1], v.data[1]), f(this->data[2], v.data[2])};
            }

            template <class R, class F, class U> requires is_compatible_point_like_v<Point<T, Model>, U>
            constexpr Point<R, Model> map(F f, const U & v) const noexcept {
                return {static_cast<R>(f(this->data[0], v.data[0])), static_cast<R>(f(this->data[1], v.data[1])), static_cast<R>(f(this->data[2], v.data[2]))};
            }

            template <class F, math::scalar U>
            constexpr auto map(F f, U v) const noexcept {
                using R = decltype(std::declval<F>()(std::declval<T>(), std::declval<U>()));
                return Point<R, Model>{f(this->data[0], v), f(this->data[1], v), f(this->data[2], v)};
            }

            template <class R, class F, math::scalar U>
            constexpr Point<R, Model> map(F f, U v) const noexcept {
                return {static_cast<R>(f(this->data[0], v)), static_cast<R>(f(this->data[1], v)), static_cast<R>(f(this->data[2], v))};
            }

            template <class F>
            constexpr auto reduce(F f) const noexcept {
                return f(f(this->data[0], this->data[1]), this->data[2]);
            }

            template <class M, class R>
            constexpr auto map_reduce(M m, R r) const noexcept {
                return r(r(m(this->data[0]), m(this->data[1])), m(this->data[2]));
            }

            template <class M, class R, class U> requires is_compatible_point_like_v<Point<T, Model>, U>
            constexpr auto map_reduce(M m, R r, const U & v) const noexcept {
                return r(r(m(this->data[0], v.data[0]), m(this->data[1], v.data[1])), m(this->data[2], v.data[2]));
            }

            template <class M, class R, math::scalar U>
            constexpr auto map_reduce(M m, R r, U v) const noexcept {
                return r(r(m(this->data[0], v), m(this->data[1], v)), m(this->data[2], v));
            }
        };

//---------------------------------------------------------------------------

        template <class T, class Model>
        struct point : point_base<point, T, Model>
        {
            using coord_type = T;
            using value_type = T;
            using base_type = point_base<point, T, Model>;
            using model_type = Model;

            static constexpr auto dimensions = Model::dimensions;

            using point_base<point, T, Model>::point_base;

            constexpr point(const swizzle_point<T, Model> & pt) noexcept :
                point{pt.point()} {}

            template <class U> requires (!std::is_same_v<T, U> && std::is_constructible_v<T, U>)
            explicit constexpr point(const swizzle_point<U, Model> & pt) noexcept :
                point{pt.point()} {}

            constexpr point & operator = (const point & pt) noexcept = default;

            template <class U> requires (!std::is_same_v<T, U> && std::is_constructible_v<T, U>)
            constexpr point & operator = (const point<U, Model> & pt) noexcept {
                point_base::operator = (pt);
                return *this;
            }

            constexpr T & operator [](int axis) noexcept {
                return this->data[axis];
            }

            constexpr const T & operator [](int axis) const noexcept {
                return this->data[axis];
            }

            explicit constexpr operator std::array<T, dimensions> & () & noexcept {
                return this->data;
            }

            explicit constexpr operator const std::array<T, dimensions> & () const & noexcept {
                return this->data;
            }

            explicit constexpr operator std::array<T, dimensions> && () && noexcept {
                return std::move(this->data);
            }

            constexpr point & negate() noexcept {
                this->apply(math::op::negate_inplace);
                return *this;
            }

            template <class U>
            constexpr point middle(const point<U, Model> & pt) const noexcept {
                return this->map<T>(math::op::avg, pt);
            }

            constexpr point operator - () const noexcept {
                return this->map<T>(math::op::negate);
            }

            constexpr point & operator += (const point & pt) noexcept {
                return this->apply(math::op::add_inplace, pt);
            }

            template <class U> requires is_compatible_point_like_v<point, U>
            constexpr point & operator += (const U & pt) noexcept {
                return this->apply(math::op::add_inplace, pt);
            }

            template <math::scalar U>
            constexpr point & operator += (U value) noexcept {
                return this->apply(math::op::add_inplace, value);
            }

            constexpr point & operator -= (const point & pt) noexcept {
                return this->apply(math::op::sub_inplace, pt);
            }

            template <class U> requires is_compatible_point_like_v<point, U>
            constexpr point & operator -= (const U & pt) noexcept {
                return this->apply(math::op::sub_inplace, pt);
            }

            template <math::scalar U>
            constexpr point & operator -= (U value) noexcept {
                return this->apply(math::op::sub_inplace, value);
            }

            constexpr point & operator *= (const point & pt) noexcept {
                return this->apply(math::op::mul_inplace, pt);
            }

            template <class U> requires is_compatible_point_like_v<point, U>
            constexpr point & operator *= (const U & pt) noexcept {
                return this->apply(math::op::mul_inplace, pt);
            }

            template <math::scalar U>
            constexpr point & operator *= (U value) noexcept {
                return this->apply(math::op::mul_inplace, value);
            }

            constexpr point & operator /= (const point & pt) noexcept {
                return this->apply(math::op::div_inplace, pt);
            }

            template <class U> requires is_compatible_point_like_v<point, U>
            constexpr point & operator /= (const U & pt) noexcept {
                return this->apply(math::op::div_inplace, pt);
            }

            template <math::scalar U>
            constexpr point & operator /= (U value) noexcept {
                return this->apply(math::op::div_inplace, value);
            }

            friend constexpr bool operator > (const point & a, const point & b) noexcept {
                return a.compare(b) > 0;
            }

            friend constexpr bool operator < (const point & a, const point & b) noexcept {
                return a.compare(b) < 0;
            }

            friend constexpr bool operator >= (const point & a, const point & b) noexcept {
                return a.compare(b) >= 0;
            }

            friend constexpr bool operator <= (const point & a, const point & b) noexcept {
                return a.compare(b) <= 0;
            }

            template <class U> requires (!std::is_same_v<T, U>)
            friend constexpr bool operator > (const point & a, const point<U, Model> & b) noexcept {
                return a.compare(b) > 0;
            }

            template <class U> requires (!std::is_same_v<T, U>)
            friend constexpr bool operator < (const point & a, const point<U, Model> & b) noexcept {
                return a.compare(b) < 0;
            }

            template <class U> requires (!std::is_same_v<T, U>)
            friend constexpr bool operator >= (const point & a, const point<U, Model> & b) noexcept {
                return a.compare(b) >= 0;
            }

            template <class U> requires (!std::is_same_v<T, U>)
            friend constexpr bool operator <= (const point & a, const point<U, Model> & b) noexcept {
                return a.compare(b) <= 0;
            }

            friend constexpr point operator + (const point & a, const point & b) noexcept {
                return a.map<T>(math::op::add, b);
            }

            friend constexpr point operator - (const point & a, const point & b) noexcept {
                return a.map<T>(math::op::sub, b);
            }

            friend constexpr point operator * (const point & a, const point & b) noexcept {
                return a.map<T>(math::op::mul, b);
            }

            friend constexpr point operator / (const point & a, const point & b) noexcept {
                return a.map<T>(math::op::div, b);
            }

            template <class U> requires (!std::is_same_v<T, U>)
            friend constexpr auto operator + (const point & a, const point<U, Model> & b) noexcept {
                return a.map(math::op::add, b);
            }

            template <class U> requires (!std::is_same_v<T, U>)
            friend constexpr auto operator - (const point & a, const point<U, Model> & b) noexcept {
                return a.map(math::op::sub, b);
            }

            template <class U> requires (!std::is_same_v<T, U>)
            friend constexpr auto operator * (const point & a, const point<U, Model> & b) noexcept {
                return a.map(math::op::mul, b);
            }

            template <class U> requires (!std::is_same_v<T, U>)
            friend constexpr auto operator / (const point & a, const point<U, Model> & b) noexcept {
                return a.map(math::op::div, b);
            }

            template <math::scalar U>
            friend constexpr auto operator + (const point & a, U b) noexcept {
                return a.map(math::op::add, b);
            }

            template <math::scalar U>
            friend constexpr auto operator - (const point & a, U b) noexcept {
                return a.map(math::op::sub, b);
            }

            template <math::scalar U>
            friend constexpr auto operator * (const point & a, U b) noexcept {
                return a.map(math::op::mul, b);
            }

            template <math::scalar U>
            friend constexpr auto operator / (const point & a, U b) noexcept {
                return a.map(math::op::div, b);
            }

            template <math::scalar U>
            friend constexpr auto operator + (U a, const point & b) noexcept {
                return b.map(math::op::add, a);
            }

            template <math::scalar U>
            friend constexpr auto operator - (U a, const point & b) noexcept {
                return b.map(math::op::sub_reverse, a);
            }

            template <math::scalar U>
            friend constexpr auto operator * (U a, const point & b) noexcept {
                return b.map(math::op::mul, a);
            }

            template <math::scalar U>
            friend constexpr auto operator / (U a, const point & b) noexcept {
                return b.map(math::op::div_reverse, a);
            }

            template <math::scalar U>
            friend constexpr auto operator + (const point<T, Model> & a, const swizzle_point<U, Model> & b) noexcept {
                return a + b.point();
            }

            template <math::scalar U>
            friend constexpr auto operator - (const point<T, Model> & a, const swizzle_point<U, Model> & b) noexcept {
                return a - b.point();
            }

            template <math::scalar U>
            friend constexpr auto operator * (const point<T, Model> & a, const swizzle_point<U, Model> & b) noexcept {
                return a * b.point();
            }

            template <math::scalar U>
            friend constexpr auto operator / (const point<T, Model> & a, const swizzle_point<U, Model> & b) noexcept {
                return a / b.point();
            }

            template <math::scalar U>
            friend constexpr auto operator + (const swizzle_point<T, Model> & a, const point<U, Model> & b) noexcept {
                return a.point() + b;
            }

            template <math::scalar U>
            friend constexpr auto operator - (const swizzle_point<T, Model> & a, const point<U, Model> & b) noexcept {
                return a.point() - b;
            }

            template <math::scalar U>
            friend constexpr auto operator * (const swizzle_point<T, Model> & a, const point<U, Model> & b) noexcept {
                return a.point() * b;
            }

            template <math::scalar U>
            friend constexpr auto operator / (const swizzle_point<T, Model> & a, const point<U, Model> & b) noexcept {
                return a.point() / b;
            }
        };

//---------------------------------------------------------------------------

        template <class T>
        struct swizzle_point<T, point_model_xy>
        {
            constexpr swizzle_point & operator = (const math::point<T, point_model_xy> & v) noexcept {
                this->data[0] = v.x;
                this->data[1] = v.y;

                return *this;
            }

            constexpr math::point<T, point_model_xy> point() const noexcept {
                return {data[0], data[1]};
            }

            constexpr T & operator [] (size_t index) noexcept {
                return data[index];
            }

            constexpr const T & operator [] (size_t index) const noexcept {
                return data[index];
            }

            union
            {
                std::array<T, 4> data;
            };
        };

        template <class T>
        struct swizzle_point<T, point_model_xz>
        {
            constexpr swizzle_point & operator = (const math::point<T, point_model_xz> & v) noexcept {
                this->data[0] = v.x;
                this->data[2] = v.z;

                return *this;
            }

            constexpr math::point<T, point_model_xz> point() const noexcept {
                return {data[0], data[2]};
            }

            constexpr T & operator [] (size_t index) noexcept {
                return data[2 * index];
            }

            constexpr const T & operator [] (size_t index) const noexcept {
                return data[2 * index];
            }

            union
            {
                std::array<T, 4> data;
            };
        };

        template <class T>
        struct swizzle_point<T, point_model_yz>
        {
            constexpr swizzle_point & operator = (const math::point<T, point_model_yz> & v) noexcept {
                this->data[1] = v.y;
                this->data[2] = v.z;

                return *this;
            }

            constexpr math::point<T, point_model_yz> point() const noexcept {
                return {data[1], data[2]};
            }

            constexpr T & operator [] (size_t index) noexcept {
                return data[index + 1];
            }

            constexpr const T & operator [] (size_t index) const noexcept {
                return data[index + 1];
            }

            union
            {
                std::array<T, 4> data;
            };
        };

        template <class T>
        struct swizzle_point<T, point_model_xyz>
        {
            constexpr swizzle_point & operator = (const math::point<T, point_model_xyz> & v) noexcept {
                this->data[0] = v.x;
                this->data[1] = v.y;
                this->data[2] = v.z;

                return *this;
            }

            constexpr math::point<T, point_model_xyz> point() const noexcept {
                return {data[0], data[1], data[2]};
            }

            constexpr T & operator [] (size_t index) noexcept {
                return data[index];
            }

            constexpr const T & operator [] (size_t index) const noexcept {
                return data[index];
            }

            union
            {
                std::array<T, 4> data;
            };
        };

//---------------------------------------------------------------------------

        template <class T>
        point(T x, T y) -> point<T, point_model_xy>;

        template <class T>
        point(T x, T y, T z) -> point<T, point_model_xyz>;

        template <class T, class Model>
        point(const swizzle_point<T, Model> &) -> point<T, Model>;

        template <template <class, class> class PointLike, class T, class Model>
        point(const PointLike<T, Model> &) -> point<T, Model>;

//---------------------------------------------------------------------------

        template <class T, math::scalar U, class Model>
        constexpr auto operator == (const swizzle_point<T, Model> & a, const swizzle_point<U, Model> & b) noexcept {
            return a.point() == b.point();
        }

        template <class T, math::scalar U, class Model>
        constexpr auto operator <=> (const swizzle_point<T, Model> & a, const swizzle_point<U, Model> & b) noexcept {
            return a.point() <=> b.point();
        }

        template <class T, math::scalar U, class Model>
        constexpr auto operator + (const swizzle_point<T, Model> & a, const swizzle_point<U, Model> & b) noexcept {
            return a.point() + b.point();
        }

        template <class T, math::scalar U, class Model>
        constexpr auto operator - (const swizzle_point<T, Model> & a, const swizzle_point<U, Model> & b) noexcept {
            return a.point() - b.point();
        }

        template <class T, math::scalar U, class Model>
        constexpr auto operator * (const swizzle_point<T, Model> & a, const swizzle_point<U, Model> & b) noexcept {
            return a.point() * b.point();
        }

        template <class T, math::scalar U, class Model>
        constexpr auto operator / (const swizzle_point<T, Model> & a, const swizzle_point<U, Model> & b) noexcept {
            return a.point() / b.point();
        }

        template <class T, math::scalar U, class Model>
        constexpr auto operator + (const swizzle_point<T, Model> & a, U b) noexcept {
            return a.point() + b;
        }

        template <class T, math::scalar U, class Model>
        constexpr auto operator - (const swizzle_point<T, Model> & a, U b) noexcept {
            return a.point() - b;
        }

        template <class T, math::scalar U, class Model>
        constexpr auto operator * (const swizzle_point<T, Model> & a, U b) noexcept {
            return a.point() * b;
        }

        template <class T, math::scalar U, class Model>
        constexpr auto operator / (const swizzle_point<T, Model> & a, U b) noexcept {
            return a.point() / b;
        }

        template <class T, math::scalar U, class Model>
        constexpr auto operator + (U a, const swizzle_point<T, Model> & b) noexcept {
            return a + b.point();
        }

        template <class T, math::scalar U, class Model>
        constexpr auto operator - (U a, const swizzle_point<T, Model> & b) noexcept {
            return a - b.point();
        }

        template <class T, math::scalar U, class Model>
        constexpr auto operator * (U a, const swizzle_point<T, Model> & b) noexcept {
            return a * b.point();
        }

        template <class T, math::scalar U, class Model>
        constexpr auto operator / (U a, const swizzle_point<T, Model> & b) noexcept {
            return a / b.point();
        }

        template <class T> requires(is_point_like_v<T>)
        constexpr auto abs(const T & p) noexcept {
            return p.map(math::op::abs);
        }

        template <class T> requires(is_point_like_v<T>)
        constexpr auto sqr(const T & p) noexcept {
            return p.map(math::op::sqr);
        }

        template <class T> requires(is_point_like_v<T>)
        constexpr auto sqrt(const T & p) noexcept {
            return p.map(math::op::sqrt);
        }

        template <class T, math::scalar U> requires(is_point_like_v<T>)
        constexpr auto floor_div(const T & a, U b) noexcept {
            return a.map(math::op::floor_div, b);
        }

        template <class T> requires(is_point_like_v<T>)
        constexpr auto floor_div(const T & a, const T & b) noexcept {
            return a.map(math::op::floor_div, b);
        }

        template <class T> requires(is_point_like_v<T>)
        constexpr auto mod(const T & a, const T & b) noexcept {
            return a.map(math::op::mod, b);
        }

        template <class T> requires(is_point_like_v<T>)
        constexpr auto floor_mod(const T & a, const T & b) noexcept {
            return a.map(math::op::floor_mod, b);
        }

        template <class T> requires(is_point_like_v<T>)
        constexpr auto avg(const T & a, const T & b) noexcept {
            return T(0.5) * (a + b);
        }

        template <class T> requires(is_point_like_v<T>)
        constexpr auto min(const T & a, const T & b) noexcept {
            return a.map(math::op::min, b);
        }

        template <class T> requires(is_point_like_v<T>)
        constexpr auto max(const T & a, const T & b) noexcept {
            return a.map(math::op::max, b);
        }

        template <class T> requires(is_point_like_v<T>)
        constexpr T clamp(const T & p, const T & low, const T & high) noexcept {
            if constexpr (T::dimensions == 2) {
                return {math::clamp(p.data[0], low.data[0], high.data[0]), math::clamp(p.data[1], low.data[1], high.data[1])};
            } else {
                return {math::clamp(p.data[0], low.data[0], high.data[0]), math::clamp(p.data[1], low.data[1], high.data[1]), math::clamp(p.data[2], low.data[2], high.data[2])};
            }
        }

        template <class T> requires(is_point_like_v<T>)
        constexpr auto invert(const T & p) noexcept {
            return p.map(math::op::invert);
        }

        template <class T> requires(is_point_like_v<T>)
        constexpr auto trunc(const T & p) noexcept {
            return p.map(math::op::trunc);
        }

        template <class T> requires(is_point_like_v<T>)
        constexpr auto floor(const T & p) noexcept {
            return p.map(math::op::floor);
        }

        template <class T> requires(is_point_like_v<T>)
        constexpr auto ceil(const T & p) noexcept {
            return p.map(math::op::ceil);
        }

        template <class T> requires(is_point_like_v<T>)
        constexpr auto round(const T & p) noexcept {
            return p.map(math::op::round);
        }

        template <class T> requires(is_point_like_v<T>)
        constexpr T next_power_of_two(const T & p) noexcept {
            return p.map(math::op::next_power_of_two);
        }

//---------------------------------------------------------------------------

        template <class R, class T, math::scalar U> requires (is_point_like_v<T>)
        constexpr auto floor_div(const T & a, U b) noexcept {
            return a.template map<R>(math::op::floor_div, b);
        }

        template <class R, class T> requires (is_point_like_v<T> && !std::is_same_v<R, T>)
        constexpr auto floor_div(const T & a, const T & b) noexcept {
            return a.template map<R>(math::op::floor_div, b);
        }

        template <class R, class T> requires (is_point_like_v<T> && !std::is_same_v<R, T>)
        constexpr auto trunc(const T & p) noexcept {
            return p.template map<R>(math::op::trunc);
        }

        template <class R, class T> requires (is_point_like_v<T> && !std::is_same_v<R, T>)
        constexpr auto floor(const T & p) noexcept {
            return p.template map<R>(math::op::floor);
        }

        template <class R, class T> requires (is_point_like_v<T> && !std::is_same_v<R, T>)
        constexpr auto ceil(const T & p) noexcept {
            return p.template map<R>(math::op::ceil);
        }

        template <class R, class T> requires (is_point_like_v<T> && !std::is_same_v<R, T>)
        constexpr auto round(const T & p) noexcept {
            return p.template map<R>(math::op::round);
        }

//---------------------------------------------------------------------------

        template <class T, class U, class Model> requires (!std::is_same_v<T, U>)
        constexpr auto floor_div(const point<T, Model> & a, const point<U, Model> & b) noexcept {
            return a.map(math::op::floor_div, b);
        }

        template <class R, class T, class U, class Model> requires (!std::is_same_v<T, U>)
        constexpr auto floor_div(const point<T, Model> & a, const point<U, Model> & b) noexcept {
            return a.template map<R>(math::op::floor_div, b);
        }

//---------------------------------------------------------------------------

        template <class T, class Model>
        constexpr auto square_length(const point<T, Model> & p) noexcept {
            return p.map_reduce(math::op::sqr, math::op::add);
        }

        template <class T, class Model>
        constexpr auto length(const point<T, Model> & p) noexcept {
            return math::sqrt(square_length(p));
        }

        template <class T, class Model>
        constexpr auto manhattan_length(const point<T, Model> & p) noexcept {
            return p.map_reduce(math::op::abs, math::op::add);
        }

        template <class T, class Model>
        constexpr auto square_distance(const point<T, Model> & a, const point<T, Model> & b) noexcept {
            return square_length(b - a);
        }

        template <class T, class Model>
        constexpr auto distance(const point<T, Model> & a, const point<T, Model> & b) noexcept {
            return length(b - a);
        }

        template <class T, class Model>
        constexpr auto manhattan_distance(const point<T, Model> & a, const point<T, Model> & b) noexcept {
            return manhattan_length(b - a);
        }

        template <class T, class Model>
        constexpr auto normalize(const point<T, Model> & p) noexcept {
            return p.map(math::op::div, length(p));
        }

        template <int X, int Y, class T, class Model> requires (Model::dimensions == 2)
        constexpr point<T, Model> shuffle(const point<T, Model> & p) noexcept {
            return {p[X], p[Y]};
        }

        template <int X, int Y, int Z, class T, class Model> requires (Model::dimensions == 3)
        constexpr point<T, Model> shuffle(const point<T, Model> & p) noexcept {
            return {p[X], p[Y], p[Z]};
        }

        template <class T, class Model>
        constexpr T dot(const point<T, Model> & a, const point<T, Model> & b) noexcept {
            return a.map_reduce(math::op::mul, math::op::add, b);
        }

        template <class T, class Model> requires (Model::dimensions > 2) // cross product is not defined in 2D space
        constexpr auto cross(const point<T, Model> & a, const point<T, Model> & b) noexcept {
            return shuffle<1, 2, 0>(a) * shuffle<2, 0, 1>(b) - shuffle<2, 0, 1>(a) * shuffle<1, 2, 0>(b);
        }

        template <class T, class Model>
        constexpr T cross_length(const point<T, Model> & a, const point<T, Model> & b) noexcept {
            if constexpr (Model::dimensions == 2) {
                return a.map_reduce(math::op::mul, math::op::sub, shuffle<1, 0>(b));
            } else {
                return math::length(math::cross(a, b));
            }
        }

        template <class T, class Model>
        constexpr auto mixed_product(const point<T, Model> & a, const point<T, Model> & b, const point<T, Model> & c) noexcept {
            return math::dot(a, math::cross(b, c));
        }

        template <class T, class Model>
        constexpr auto line_square_distance(const point<T, Model> & p, const point<T, Model> & a, const point<T, Model> & b) noexcept {
            auto v = b - a;
            auto c = math::cross_length(v, p - a);
            return c * c / math::square_length(v);
        }

        template <class T, class Model>
        constexpr auto line_distance(const point<T, Model> & p, const point<T, Model> & a, const point<T, Model> & b) noexcept {
            auto v = b - a;
            return math::abs(math::cross_length(v, p - a) / math::length(v));
        }

        template <class T, class Model>
        constexpr auto line_segment_square_distance(const point<T, Model> & p, const point<T, Model> & a, const point<T, Model> & b) noexcept {
            auto v = b - a;
            auto line_length_2 = math::square_length(v);

            if (line_length_2 <= 0) {
                return math::square_distance(p, a);
            }

            auto t = math::clamp(math::dot(p - a, v) / line_length_2, 0.0f, 1.0f);
            return math::square_distance(p, a + t * v);
        }

        template <class T, class Model>
        constexpr auto line_segment_distance(const point<T, Model> & p, const point<T, Model> & a, const point<T, Model> & b) noexcept {
            return math::sqrt(math::line_segment_square_distance(p, a, b));
        }

        template <class T, class Model>
        constexpr auto ray_square_distance(const point<T, Model> & p, const point<T, Model> & a, const point<T, Model> & b) noexcept {
            auto v = b - a;
            auto line_length_2 = math::square_length(v);

            if (line_length_2 <= 0) {
                return math::square_distance(p, a);
            }

            auto t = math::max(math::dot(p - a, v) / line_length_2, 0.0f);
            return math::square_distance(p, a + t * v);
        }

        template <class T, class Model>
        constexpr auto ray_distance(const point<T, Model> & p, const point<T, Model> & a, const point<T, Model> & b) noexcept {
            return math::sqrt(math::ray_square_distance(p, a, b));
        }

        // returns [0;pi]
        template <class T, class Model>
        constexpr T angle(const point<T, Model> & a, const point<T, Model> & b) noexcept {
            return math::acos(dot(a, b));
        }

        // returns [-pi;pi]
        template <class T, class Model>
        constexpr T signed_angle(const point<T, Model> & a, const point<T, Model> & b) noexcept {
            return math::atan(cross_length(a, b), dot(a, b));
        }

        // returns [-pi;pi]
        template <class T, class Model>
        constexpr T signed_angle(const point<T, Model> & a, const point<T, Model> & b, const point<T, Model> & plane_normal) noexcept {
            return math::atan(math::mixed_product(plane_normal, a, b), dot(a, b));
        }

//---------------------------------------------------------------------------

        template <class T, class Model>
        constexpr point<T, Model> line_normal(const point<T, Model> & direction) noexcept {
            if constexpr (Model::dimensions == 3) {
                return math::normalize(point<T, Model>{-direction[1], direction[0], 0});
            } else {
                return {-direction[1], direction[0]};
            }
        }

        template <class T, class Model>
        constexpr point<T, Model> tangent_normal(const point<T, Model> & in_direction, const point<T, Model> & out_direction) noexcept {
            auto tangent = math::normalize(in_direction + out_direction);
            auto normal = math::line_normal(tangent);

            return normal / dot(normal, math::line_normal(in_direction));
        }

        template <class T, class Model, class Cos, class U>
        constexpr point<T, Model> slerp_cos(const point<T, Model> & a, const point<T, Model> & b, Cos cos, U t) noexcept {
            auto angle = math::acos(cos);
            auto inv_sin = rsqrt(1.0f - cos * cos);
            return (math::sin((1.0f - t) * angle) * a + math::sin(t * angle) * b) * inv_sin;
        }

        template <class T, class Model, class Sin, class U>
        constexpr point<T, Model> slerp_sin(const point<T, Model> & a, const point<T, Model> & b, Sin sin, U t) noexcept {
            auto angle = math::asin(sin);
            return (math::sin((1.0f - t) * angle) * a + math::sin(t * angle) * b) / sin;
        }

        template <class T, class Model, class U>
        constexpr point<T, Model> slerp(const point<T, Model> & a, const point<T, Model> & b, U t) noexcept {
            return slerp_cos(a, b, math::dot(a, b), t);
        }

        template <class T, class Model, class Angle, class U>
        constexpr point<T, Model> slerp(const point<T, Model> & a, const point<T, Model> & b, Angle angle, U t) noexcept {
            return (math::sin((1.0f - t) * angle) * a + math::sin(t * angle) * b) / math::sin(angle);
        }

        template <class T, class Model, class U>
        constexpr point<T, Model> rotate(const point<float, Model> & direction, U angle) noexcept {
            auto [sin, cos] = math::sincos(angle);

            if constexpr (Model::dimensions == 2) {
                return { direction[0] * cos - direction[1] * sin, direction[0] * sin + direction[1] * cos };
            } else {
                return { direction[0] * cos - direction[1] * sin, direction[0] * sin + direction[1] * cos, direction[2] };
            }
        }

        template <class Model = point_model_xy, class T>
        constexpr point<T, Model> direction(T angle) noexcept {
            auto [sin, cos] = math::sincos(angle);
            return {cos, sin};
        }

        template <class T, class Model = point_model_xy, class U> requires (!std::is_same_v<T, U>)
        constexpr point<T, Model> direction(U angle) noexcept {
            auto [sin, cos] = math::sincos(angle);
            return {cos, sin};
        }

        namespace fast
        {
            template <class Model>
            inline auto length(const point<float, Model> & p) noexcept {
                return fast::sqrt(math::square_length(p));
            }

            template <class Model>
            inline auto distance(const point<float, Model> & a, const point<float, Model> & b) noexcept {
                return fast::length(b - a);
            }

            template <class Model>
            inline auto normalize(const point<float, Model> & a) noexcept {
                return a / fast::length(a);
            }

            template <class Model>
            inline auto slerp_cos(const point<float, Model> & a, const point<float, Model> & b, float cos, float t) noexcept {
                float angle = math::acos(cos);
                return fast::normalize((fast::sin((1.0f - t) * angle) * a + fast::sin(t * angle) * b) * fast::rsqrt(1.0f - cos * cos));
            }

            template <class Model>
            inline auto slerp_sin(const point<float, Model> & a, const point<float, Model> & b, float sin, float t) noexcept {
                float angle = math::asin(sin);
                return fast::normalize((fast::sin((1.0f - t) * angle) * a + fast::sin(t * angle) * b) / sin);
            }

            template <class Model>
            inline auto slerp(const point<float, Model> & a, const point<float, Model> & b, float t) noexcept {
                return fast::slerp_cos(a, b, math::dot(a, b), t);
            }

            template <class Model>
            inline auto slerp(const point<float, Model> & a, const point<float, Model> & b, float angle, float t) noexcept {
                return (fast::sin((1.0f - t) * angle) * a + fast::sin(t * angle) * b) / fast::sin(angle);
            }

            template <class Model>
            inline point<float, Model> rotate(const point<float, Model> & direction, float angle) noexcept {
                auto [sin, cos] = fast::sincos(angle);

                if constexpr (Model::dimensions == 2) {
                    return { direction[0] * cos - direction[1] * sin, direction[0] * sin + direction[1] * cos };
                } else {
                    return { direction[0] * cos - direction[1] * sin, direction[0] * sin + direction[1] * cos, direction[2] };
                }
            }

            template <class Model = point_model_xy>
            inline point<float, Model> direction(float angle) noexcept {
                auto [sin, cos] = fast::sincos(angle);
                return {cos, sin};
            }

            template <class Model>
            inline auto line_distance(const point<float, Model> & p, const point<float, Model> & a, const point<float, Model> & b) noexcept {
                return fast::sqrt(math::line_square_distance(p, a, b));
            }

            template <class Model>
            inline auto line_segment_distance(const point<float, Model> & p, const point<float, Model> & a, const point<float, Model> & b) noexcept {
                return fast::sqrt(math::line_segment_square_distance(p, a, b));
            }

            template <class Model>
            inline auto ray_distance(const point<float, Model> & p, const point<float, Model> & a, const point<float, Model> & b) noexcept {
                return fast::sqrt(math::ray_square_distance(p, a, b));
            }
        }

//---------------------------------------------------------------------------

        template <class T>
        using point2 = point<T, point_model_xy>;

        template <class T>
        using point3 = point<T, point_model_xyz>;

        template <class T>
        using point_xy = point<T, point_model_xy>;

        template <class T>
        using point_xz = point<T, point_model_xz>;

        template <class T>
        using point_yz = point<T, point_model_yz>;

        template <class T>
        using point_xyz = point<T, point_model_xyz>;

        using byte_point = point<uint8>;
        using int_point = point<int>;
        using uint_point = point<uint32>;
        using long_point = point<long>;
        using float_point = point<float>;

        static_assert(std::is_standard_layout_v<float_point>, "Point is not standard layout");

        using byte_point_xy = point_xy<uint8>;
        using int_point_xy = point_xy<int>;
        using uint_point_xy = point_xy<uint32>;
        using long_point_xy = point_xy<long>;
        using float_point_xy = point_xy<float>;

        using byte_point_xz = point_xz<uint8>;
        using int_point_xz = point_xz<int>;
        using uint_point_xz = point_xz<uint32>;
        using long_point_xz = point_xz<long>;
        using float_point_xz = point_xz<float>;

        using byte_point_yz = point_yz<uint8>;
        using int_point_yz = point_yz<int>;
        using uint_point_yz = point_yz<uint32>;
        using long_point_yz = point_yz<long>;
        using float_point_yz = point_yz<float>;

        using byte_point_xyz = point_xyz<uint8>;
        using int_point_xyz = point_xyz<int>;
        using uint_point_xyz = point_xyz<uint32>;
        using long_point_xyz = point_xyz<long>;
        using float_point_xyz = point_xyz<float>;
    }
}

namespace std
{
    template <size_t Idx, class T, class Model>
    struct tuple_element<Idx, asd::math::point<T, Model>>
    {
        using type = T;
    };

    template <class T, class Model>
    struct tuple_size<asd::math::point<T, Model>> : integral_constant<size_t, Model::dimensions> {};

    template <size_t Idx, class T, class Model>
    constexpr T get(const asd::math::point<T, Model> & pt) {
        return pt[Idx];
    }

    template <size_t Idx, class T, class Model>
    constexpr T & get(asd::math::point<T, Model> & pt) {
        return pt[Idx];
    }
}
