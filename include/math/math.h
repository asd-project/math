//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#define _USE_MATH_DEFINES

#include <meta/common.h>
#include <simd/simd.h>

#include <cmath>
#include <algorithm>
#include <numeric>
#include <float.h>

#include <stdlib.h>
#include <gcem.hpp>

#undef min
#undef max

//---------------------------------------------------------------------------

namespace asd
{
    namespace math
    {
        template <class T>
        struct constants {};

        template <class T>
        struct coefficients {};

//---------------------------------------------------------------------------

        template <>
        struct constants<float>
        {
            static constexpr float one = 1.0f;
            static constexpr float pi = 3.141592653f;
            static constexpr float pi2 = 6.283185307f;
            static constexpr float half_pi = 1.570796326f;
            static constexpr float half_pi3 = 4.712388980f;
            static constexpr float inv_pi = 0.318309886f;
            static constexpr float inv_pi2 = 0.159154943f;
            static constexpr float degree180 = 180.0f;
            static constexpr float degree360 = 360.0f;
            static constexpr float eps = std::numeric_limits<float>::epsilon();
            static constexpr float eps2 = eps * eps;
            static constexpr float eps3 = eps * eps * eps;
            static constexpr float infinity = std::numeric_limits<float>::infinity();
            static constexpr float degrees2radians = pi / degree180;
            static constexpr float radians2degrees = degree180 / pi;
        };

        template <>
        struct constants<double>
        {
            static constexpr double one = 1.0;
            static constexpr double pi = 3.14159265358979323846;
            static constexpr double pi2 = 6.28318530717958647692;
            static constexpr double half_pi = 1.57079632679489661923;
            static constexpr double half_pi3 = 4.71238898038468985769;
            static constexpr double inv_pi = 0.318309886183790671538;
            static constexpr double inv_pi2 = 0.159154943091895335769;
            static constexpr double degree180 = 180.0;
            static constexpr double degree360 = 360.0;
            static constexpr double eps = std::numeric_limits<double>::epsilon();
            static constexpr double eps2 = eps * eps;
            static constexpr double eps3 = eps * eps * eps;
            static constexpr double infinity = std::numeric_limits<double>::infinity();
            static constexpr double degrees2radians = pi / degree180;
            static constexpr double radians2degrees = degree180 / pi;
        };

        template <>
        struct constants<long double>
        {
            static constexpr long double one = 1.0;
            static constexpr long double pi = 3.14159265358979323846;
            static constexpr long double pi2 = 6.28318530717958647692;
            static constexpr long double half_pi = 1.57079632679489661923;
            static constexpr long double half_pi3 = 4.71238898038468985769;
            static constexpr long double inv_pi = 0.318309886183790671538;
            static constexpr long double inv_pi2 = 0.159154943091895335769;
            static constexpr long double degree180 = 180.0;
            static constexpr long double degree360 = 360.0;
            static constexpr long double eps = std::numeric_limits<long double>::epsilon();
            static constexpr long double eps2 = eps * eps;
            static constexpr long double eps3 = eps * eps * eps;
            static constexpr long double infinity = std::numeric_limits<long double>::infinity();
            static constexpr long double degrees2radians = pi / degree180;
            static constexpr long double radians2degrees = degree180 / pi;
        };

        constexpr float one = constants<float>::one;
        constexpr float pi = constants<float>::pi;
        constexpr float pi2 = constants<float>::pi2;
        constexpr float half_pi = constants<float>::half_pi;
        constexpr float half_pi3 = constants<float>::half_pi3;
        constexpr float inv_pi = constants<float>::inv_pi;
        constexpr float inv_pi2 = constants<float>::inv_pi2;
        constexpr float degree180 = constants<float>::degree180;
        constexpr float degree360 = constants<float>::degree360;
        constexpr float eps = constants<float>::eps;
        constexpr float eps2 = constants<float>::eps2;
        constexpr float eps3 = constants<float>::eps3;
        constexpr float infinity = constants<float>::infinity;
        constexpr float inf = constants<float>::infinity;
        constexpr float degrees2radians = constants<float>::degrees2radians;
        constexpr float radians2degrees = constants<float>::radians2degrees;
        constexpr float deg2rad = constants<float>::degrees2radians;
        constexpr float rad2deg = constants<float>::radians2degrees;

        template <>
        struct coefficients<float>
        {
            static constexpr float sin[] = {
                -0.16666666666666666666666666666667f,
                +0.00833333333333333333333333333333f,
                -1.984126984126984126984126984127e-4f,
                +2.7557319223985890652557319223986e-6f,
                -2.5052108385441718775052108385442e-8f
            };

            // cosine needs a bit more precision
            static constexpr float cos[] = {
                -0.5f,
                +0.04166666666666666666666666666667f,
                -0.00138888888888888888888888888889f,
                +2.4801587301587301587301587301587e-5f,
                -2.7557319223985890652557319223986e-7f,
                +2.08757008419747316778e-9f
            };
        };

        template <>
        struct coefficients<double>
        {
            static constexpr double sin[] = {
                -0.16666666666666666666666666666667,
                +0.00833333333333333333333333333333,
                -1.984126984126984126984126984127e-4,
                +2.7557319223985890652557319223986e-6,
                -2.5052108385441718775052108385442e-8
            };

            static constexpr double cos[] = {
                -0.5,
                +0.04166666666666666666666666666667,
                -0.00138888888888888888888888888889,
                +2.4801587301587301587301587301587e-5,
                -2.7557319223985890652557319223986e-7,
                +2.08757008419747316778e-9
            };
        };

        template <>
        struct coefficients<long double>
        {
            static constexpr long double sin[] = {
                -0.16666666666666666666666666666667,
                +0.00833333333333333333333333333333,
                -1.984126984126984126984126984127e-4,
                +2.7557319223985890652557319223986e-6,
                -2.5052108385441718775052108385442e-8
            };

            static constexpr long double cos[] = {
                -0.5,
                +0.04166666666666666666666666666667,
                -0.00138888888888888888888888888889,
                +2.4801587301587301587301587301587e-5,
                -2.7557319223985890652557319223986e-7,
                +2.08757008419747316778e-9
            };
        };

//---------------------------------------------------------------------------

        template <class T>
        using cfs = coefficients<T>;

        using float_cfs = cfs<float>;
        using double_cfs = cfs<double>;

        template <class T>
        using fp = std::conditional_t<std::is_integral_v<T>, float, T>;

        template <class T>
        constexpr bool is_scalar_v = std::is_scalar_v<T>;

        template <class T>
        constexpr bool is_real_v = is_scalar_v<T> && !std::is_integral_v<T>;

        template <class T>
        concept scalar = math::is_scalar_v<T>;

        template <class T>
        concept real = math::is_real_v<T>;

        template <class T>
        concept integral = std::is_integral_v<T>;

        template <class T>
        concept signed_type = std::is_signed_v<T>;

        template <class T>
        concept unsigned_type = !std::is_signed_v<T>;

        template <math::signed_type T>
        constexpr T abs(T x) noexcept {
            if (std::is_constant_evaluated()) {
                return gcem::abs(x);
            } else {
                return std::abs(x);
            }
        }

        template <math::signed_type T>
        constexpr T sign(T x) noexcept {
            return T(T(0) < x) - T(x < T(0));
        }

        template <class T>
        constexpr auto sqr(T x) noexcept -> decltype(x * x) {
            return x * x;
        }

        template <math::real T>
        constexpr T pow(T x, T pow) noexcept {
            if (std::is_constant_evaluated()) {
                return static_cast<T>(gcem::pow(static_cast<long double>(x), pow));
            } else {
                return std::pow(x, pow);
            }
        }

        template <math::integral T>
        constexpr T pow(T x, T pow) noexcept {
            return pow == 0 ? 1 : x * math::pow(x, pow - 1);
        }

        template <class R, class T> requires (!std::is_same_v<R, T>)
        constexpr R pow(T x, T pow) noexcept {
            return static_cast<R>(math::pow(x, pow));
        }

        template <math::real T>
        constexpr T sqrt(T x) noexcept {
            if (std::is_constant_evaluated()) {
                return static_cast<T>(gcem::sqrt(static_cast<long double>(x)));
            } else {
                return std::sqrt(x);
            }
        }

        template <math::integral T>
        constexpr fp<T> sqrt(T x) noexcept {
            return math::sqrt(static_cast<fp<T>>(x));
        }

        template <class R, class T>
        constexpr R sqrt(T x) noexcept {
            return static_cast<R>(math::sqrt(x));
        }

        template <math::real T>
        constexpr auto rsqrt(T x) noexcept {
            return 1.0f / math::sqrt(x);
        }

        template <class R, math::real T>
        constexpr auto rsqrt(T x) noexcept {
            return static_cast<R>(1.0f / math::sqrt(x));
        }

        template <class T, class U, math::real R = std::common_type_t<T, U>>
        constexpr R mod(T x, U y) noexcept {
            if (std::is_constant_evaluated()) {
                return gcem::fmod(x, y);
            } else {
                return static_cast<R>(std::fmod(static_cast<R>(x), static_cast<R>(y)));
            }
        }

        template <class T, class U, math::integral R = std::common_type_t<T, U>>
        constexpr R mod(T x, U y) noexcept {
            return static_cast<R>(x % y);
        }

        template <math::scalar T>
        constexpr auto avg(T x, T y) noexcept {
            return std::midpoint(x, y);
        }

        template <math::scalar T, math::real U>
        constexpr T lerp(T a, T b, U t) noexcept {
            return std::lerp(a, b, t);
        }

        template <class T, math::real U> requires (!math::is_scalar_v<T>)
        constexpr T lerp(const T & a, const T & b, U t) noexcept {
            return T(a + (b - a) * t);
        }

        template <class T, class U = T, class R = std::common_type_t<T, U>> requires (math::is_scalar_v<R> && !std::is_floating_point_v<R>)
        constexpr bool equal(T x, U y) noexcept {
            return x == y;
        }

        template <class T, class U = T, class R = std::common_type_t<T, U>> requires std::is_floating_point_v<T> && std::is_floating_point_v<R>
        constexpr bool equal(T x, U y) noexcept {
            return x == y || math::abs(x - y) <= math::constants<T>::eps;
        }

        template <class T, class U = T, class R = std::common_type_t<T, U>> requires (!math::is_scalar_v<T> || !math::is_scalar_v<R>)
        constexpr bool equal(const T & x, const U & y) noexcept {
            return x == y;
        }

        template <class T, class U = T, class R = std::common_type_t<T, U>> requires (math::is_scalar_v<R> && !std::is_floating_point_v<R>)
        constexpr bool not_equal(T x, U y) noexcept {
            return x != y;
        }

        template <class T, class U = T, class R = std::common_type_t<T, U>> requires std::is_floating_point_v<T> && std::is_floating_point_v<R>
        constexpr bool not_equal(T x, U y) noexcept {
            return x != y && math::abs(x - y) > math::constants<T>::eps;
        }

        template <class T, class U = T, class R = std::common_type_t<T, U>> requires (!math::is_scalar_v<T> || !math::is_scalar_v<R>)
        constexpr bool not_equal(const T & x, const U & y) noexcept {
            return x != y;
        }

        template <class T, class U = T, class E = float, class R = std::common_type_t<T, U>> requires (math::is_scalar_v<R> && !std::is_floating_point_v<R>)
        constexpr bool fuzzy_equal(T x, U y, E eps = math::constants<E>::eps) noexcept {
            return x == y;
        }

        template <class T, class U = T, class E = float, class R = std::common_type_t<T, U>> requires std::is_floating_point_v<T> && std::is_floating_point_v<R>
        constexpr bool fuzzy_equal(T x, U y, E eps = math::constants<E>::eps) noexcept {
            return x == y || math::abs(x - y) <= eps;
        }

        template <class T, class U = T>
        constexpr int compare(T x, U y) noexcept {
            return equal(x, y) ? 0 : x > y ? 1 : -1; // ADL
        }

        template <class T, class U = T, math::scalar R = std::common_type_t<T, U>>
        constexpr R min(T x, U y) noexcept {
            return std::min(static_cast<R>(x), static_cast<R>(y));
        }

        template <class R, class T, class U = T> requires (!std::is_same_v<std::common_type_t<T, U>, R>)
        constexpr R min(T x, U y) noexcept {
            return std::min(static_cast<R>(x), static_cast<R>(y));
        }

        template <class T, class U = T, math::scalar R = std::common_type_t<T, U>>
        constexpr R max(T x, U y) noexcept {
            return std::max(static_cast<R>(x), static_cast<R>(y));
        }

        template <class R, class T, class U = T> requires (!std::is_same_v<std::common_type_t<T, U>, R>)
        constexpr R max(T x, U y) noexcept {
            return std::max(static_cast<R>(x), static_cast<R>(y));
        }

        template <math::scalar T>
        constexpr T clamp(T x, non_deduced<T> low, non_deduced<T> high) noexcept {
            return high < x ? high : x < low ? low : x;
        }

        template <math::real T>
        constexpr T invert(const T & x) noexcept {
            return static_cast<T>(1) / x;
        }

        template <math::integral T>
        constexpr fp<T> invert(T x) noexcept {
            return math::invert(static_cast<fp<T>>(x));
        }

        template <math::real T>
        constexpr T trunc(T v) noexcept {
            return static_cast<decltype(v)>(static_cast<long long>(v));
        }

        template <class T, math::real U> requires (!std::is_same_v<T, U>)
        constexpr T trunc(U v) noexcept {
            return static_cast<T>(math::trunc(v));
        }

        template <math::integral T>
        constexpr T trunc(T x) noexcept {
            return x;
        }

        template <math::real T>
        constexpr T floor(T v) noexcept {
            if (std::is_constant_evaluated()) {
                return gcem::floor(v);
            } else {
                return std::floor(v);
            }
        }

        template <class T, math::real U> requires (!std::is_same_v<T, U>)
        constexpr T floor(U v) noexcept {
            return static_cast<T>(math::floor(v));
        }

        template <math::integral T>
        constexpr T floor(T x) noexcept {
            return x;
        }

        template <math::real T>
        constexpr T ceil(T v) noexcept {
            if (std::is_constant_evaluated()) {
                return gcem::ceil(v);
            } else {
                return std::ceil(v);
            }
        }

        template <class T, math::real U> requires (!std::is_same_v<T, U>)
        constexpr T ceil(U v) noexcept {
            return static_cast<T>(math::ceil(v));
        }

        template <math::integral T>
        constexpr T ceil(T x) noexcept {
            return x;
        }

        template <math::real T>
        constexpr T round(T v) noexcept {
            if (std::is_constant_evaluated()) {
                return gcem::round(v);
            } else {
                return std::round(v);
            }
        }

        template <class T, math::real U> requires (!std::is_same_v<T, U>)
        constexpr T round(U v) noexcept {
            return static_cast<T>(math::round(v));
        }

        template <math::integral T>
        constexpr T round(T x) noexcept {
            return x;
        }

        template <class T, class U, math::real R = std::common_type_t<T, U>>
        constexpr R floor_div(T a, U b) noexcept {
            return math::floor(a / b);
        }

        template <class V, class T, class U, math::real R = std::common_type_t<T, U>> requires (!std::is_same_v<V, R>)
        constexpr V floor_div(T a, U b) noexcept {
            return static_cast<V>(math::floor(a / b));
        }

        template <class T, class U, math::integral R = std::common_type_t<T, U>>
        constexpr R floor_div(T a, U b) noexcept {
            if (std::is_constant_evaluated()) {
                return static_cast<R>(math::floor_div<R>(static_cast<float>(a), b));
            } else {
                div_t r = std::div(a, b);
                return r.rem != 0 && ((a < 0) ^ (b < 0)) ? r.quot - 1 : r.quot;
            }
        }

        template <class T, class U, class R = std::common_type_t<T, U>>
        constexpr R floor_mod(T x, U y) noexcept {
            return static_cast<R>(x - floor_div(x, y) * y);
        }

        template <math::integral T>
        constexpr T next_power_of_two(T x) {
            if (x <= 0) {
                return 0;
            }

            --x;

            for(size_t i = 1; i < sizeof(T) * 8; i <<= 1) {
                x |= x >> i;
            }

            return ++x;
        }

        template <math::real T>
        constexpr T sin(T angle) {
            if (std::is_constant_evaluated()) {
                return static_cast<T>(gcem::sin(static_cast<long double>(angle)));
            } else {
                return std::sin(angle);
            }
        }

        template <math::real T>
        constexpr T cos(T angle) {
            if (std::is_constant_evaluated()) {
                return static_cast<T>(gcem::cos(static_cast<long double>(angle)));
            } else {
                return std::cos(angle);
            }
        }

        template <math::real T>
        constexpr T asin(T sin) {
            if (std::is_constant_evaluated()) {
                return static_cast<T>(gcem::asin(static_cast<long double>(sin)));
            } else {
                return std::asin(sin);
            }
        }

        template <math::real T>
        constexpr T acos(T cos) {
            if (std::is_constant_evaluated()) {
                return static_cast<T>(gcem::acos(static_cast<long double>(cos)));
            } else {
                return std::acos(cos);
            }
        }

        template <math::real T>
        constexpr T atan(T y, T x) {
            if (std::is_constant_evaluated()) {
                return static_cast<T>(gcem::atan2(static_cast<long double>(y), x));
            } else {
                return std::atan2(y, x);
            }
        }

        template <math::real T>
        constexpr void sincos(T angle, T & sine, T & cosine) {
            sine = math::sin(angle);
            cosine = math::cos(angle);
        }

        template <math::real T>
        constexpr std::pair<T, T> sincos(T angle) {
            std::pair<T, T> sc;
            sincos(angle, sc.first, sc.second); // ADL

            return sc;
        }

        template <class T> requires (!math::scalar<T>)
        constexpr std::pair<T, T> sincos(const T & angle) {
            std::pair<T, T> sc;
            sincos(angle, sc.first, sc.second); // ADL

            return sc;
        }

        // convert degrees to radians
        template <math::real T>
        constexpr T radians(T degrees) {
            return constants<T>::degrees2radians * degrees;
        }

        // convert radians to degrees
        template <math::real T>
        constexpr T degrees(T radians) {
            return constants<T>::radians2degrees * radians;
        }

        // put an angle into [-pi; pi] range
        template <math::real T>
        constexpr T normalize_angle(T angle) {
            return std::remainder(angle, constants<T>::pi2);
        }

        template <math::real T>
        constexpr T align(T value, T step) {
            return math::ceil(value / step) * step;
        }

        // aliases

        template <class T, class U = T>
        constexpr bool eq(const T & x, const U & y) noexcept {
            return equal(x, y); // ADL
        }

        template <class T, class U = T>
        constexpr bool neq(const T & x, const U & y) noexcept {
            return not_equal(x, y); // ADL
        }

        template <class T, class U = T, class E = float>
        constexpr bool fuzzy_eq(const T & x, const U & y, E eps = math::eps) noexcept {
            return fuzzy_equal(x, y, eps); // ADL
        }

        namespace fast
        {
            inline float rsqrt(float x) noexcept {
#if defined(ASD_SIMD_X86)
                return _mm_cvtss_f32(_mm_rsqrt_ss(_mm_load_ss(&x)));
#else
                return math::rsqrt(x);
#endif
            }

            inline float sqrt(float x) noexcept {
#if defined(ASD_SIMD_X86)
                return x * _mm_cvtss_f32(_mm_rsqrt_ss(_mm_load_ss(&x)));
#else
                return math::sqrt(x);
#endif
            }

            inline float sin_emulated(float x) noexcept {
                x *= math::inv_pi2;
                x -= 0.5f + static_cast<int>(x);
                x *= 16.0f * (math::abs(x) - 0.5f);

                return x * (0.775f + 0.225f * math::abs(x));
            }

            inline float cos_emulated(float x) noexcept {
                x *= math::inv_pi2;
                x -= 0.25f + static_cast<int>(x + 0.25f);
                x *= 16.0f * (math::abs(x) - 0.5f);

                return x * (0.775f + 0.225f * math::abs(x));
            }

            inline float sin(float x) noexcept {
#ifdef ASD_FAST_MATH_EMULATE_SIN_COS
                return sin_emulated(x);
#else
                return std::sin(x); // usually, std::sin is fast enough
#endif
            }

            inline float cos(float x) noexcept {
#ifdef ASD_FAST_MATH_EMULATE_SIN_COS
                return cos_emulated(x);
#else
                return std::cos(x); // usually, std::cos is fast enough
#endif
            }

            inline void sincos(float angle, float & sine, float & cosine) {
                // emulated sine + cosine actually perform better together, than std::sin + std::cos
                sine = fast::sin_emulated(angle);
                cosine = fast::cos_emulated(angle);
            }

            inline std::pair<float, float> sincos(float angle) {
                std::pair<float, float> sc;
                fast::sincos(angle, sc.first, sc.second);

                return sc;
            }
        }

        constexpr long double operator ""_rad(long double radians) {
            return radians;
        }

        constexpr long double operator ""_deg(long double degrees) {
            return radians(degrees);
        }

        enum class relative_pos
        {
            inside = 0x00,
            before = 0x01,
            after = 0x02,
            in_back = static_cast<int>(before),
            in_front = static_cast<int>(after),
            around = 0x03
        };

        enum class axis
        {
            x = 0x00,
            y = 0x01,
            z = 0x02
        };

        enum class axis_mask
        {
            none = 0x00,
            x = 1 << static_cast<int>(axis::x),
            y = 1 << static_cast<int>(axis::y),
            z = 1 << static_cast<int>(axis::z),
            xy = static_cast<int>(x) | static_cast<int>(y),
            xz = static_cast<int>(x) | static_cast<int>(z),
            yz = static_cast<int>(y) | static_cast<int>(z),
            xyz = static_cast<int>(x) | static_cast<int>(y) | static_cast<int>(z),
        };

        template <typename T>
        using limits = std::numeric_limits<T>;

        namespace op // lambda wrappers
        {
            constexpr auto abs = [](auto x) noexcept {
                return math::abs(x);
            };
            constexpr auto sqr = [](auto x) noexcept {
                return math::sqr(x);
            };
            constexpr auto sqrt = [](auto x) noexcept {
                return math::sqrt(x);
            };
            constexpr auto invert = [](auto x) noexcept {
                return math::invert(x);
            };
            constexpr auto mod = [](auto a, auto b) noexcept {
                return math::mod(a, b);
            };
            constexpr auto trunc = [](auto x) noexcept {
                return math::trunc(x);
            };
            constexpr auto floor = [](auto x) noexcept {
                return math::floor(x);
            };
            constexpr auto ceil = [](auto x) noexcept {
                return math::ceil(x);
            };
            constexpr auto round = [](auto x) noexcept {
                return math::round(x);
            };
            constexpr auto floor_div = [](auto a, auto b) noexcept {
                return math::floor_div(a, b);
            };
            constexpr auto floor_mod = [](auto a, auto b) noexcept {
                return math::floor_mod(a, b);
            };
            constexpr auto avg = [](auto a, auto b) noexcept {
                return math::avg(a, b);
            };
            constexpr auto min = [](auto a, auto b) noexcept {
                return math::min(a, b);
            };
            constexpr auto max = [](auto a, auto b) noexcept {
                return math::max(a, b);
            };
            constexpr auto eq = [](auto a, auto b) noexcept {
                return math::eq(a, b);
            };
            constexpr auto neq = [](auto a, auto b) noexcept {
                return math::neq(a, b);
            };
            constexpr auto fuzzy_eq = [](auto a, auto b, auto e = math::eps) noexcept {
                return math::fuzzy_eq(a, b);
            };
            constexpr auto lt = [](auto a, auto b) noexcept {
                return math::compare(a, b) < 0;
            };
            constexpr auto gt = [](auto a, auto b) noexcept {
                return math::compare(a, b) > 0;
            };
            constexpr auto le = [](auto a, auto b) noexcept {
                return math::compare(a, b) <= 0;
            };
            constexpr auto ge = [](auto a, auto b) noexcept {
                return math::compare(a, b) >= 0;
            };

            constexpr auto bool_and = [](auto a, auto b) noexcept {
                return a && b;
            };
            constexpr auto bool_or = [](auto a, auto b) noexcept {
                return a || b;
            };

            constexpr auto bit_and = [](auto a, auto b) noexcept {
                return a & b;
            };
            constexpr auto bit_or = [](auto a, auto b) noexcept {
                return a | b;
            };

            constexpr auto negate = [](auto x) noexcept {
                return -x;
            };
            constexpr auto negate_inplace = [](auto & x) noexcept {
                x = -x;
            };
            constexpr auto add = [](auto a, auto b) noexcept {
                return a + b;
            };
            constexpr auto add_inplace = [](auto & a, auto b) noexcept {
                a += b;
            };
            constexpr auto sub = [](auto a, auto b) noexcept {
                return a - b;
            };
            constexpr auto sub_reverse = [](auto a, auto b) noexcept {
                return b - a;
            };
            constexpr auto sub_inplace = [](auto & a, auto b) noexcept {
                a -= b;
            };
            constexpr auto mul = [](auto a, auto b) noexcept {
                return a * b;
            };
            constexpr auto mul_inplace = [](auto & a, auto b) noexcept {
                a *= b;
            };
            constexpr auto div = [](auto a, auto b) noexcept {
                return a / b;
            };
            constexpr auto div_reverse = [](auto a, auto b) noexcept {
                return b / a;
            };
            constexpr auto div_inplace = [](auto & a, auto b) noexcept {
                a /= b;
            };

            constexpr auto next_power_of_two = [](auto x) noexcept {
                return math::next_power_of_two(x);
            };

            constexpr auto sin = [](auto x) noexcept {
                return math::sin(x);
            };
            constexpr auto cos = [](auto x) noexcept {
                return math::cos(x);
            };
            constexpr auto asin = [](auto x) noexcept {
                return math::asin(x);
            };
            constexpr auto acos = [](auto x) noexcept {
                return math::acos(x);
            };
            constexpr auto atan = [](auto x) noexcept {
                return math::atan(x);
            };
        }
    }
}
