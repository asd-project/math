//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <math/math.h>

#include <meta/concepts.h>
#include <algorithm/compare.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace math
    {
        template <class T, class Storage>
        struct range;

        template <class T>
        class range_iterator
        {
            static_assert(std::is_integral<T>::value, "Can iterate only integral range!");

        public:
            using iterator_category = std::random_access_iterator_tag;

            range_iterator(T val) : val(val) {}

            bool operator ==(const range_iterator & it) const {
                return val == it.val;
            }

            bool operator !=(const range_iterator & it) const {
                return val != it.val;
            }

            T operator *() const {
                return val;
            }

            range_iterator & operator ++() {
                ++val;
                return *this;
            }

            range_iterator & operator --() {
                --val;
                return *this;
            }

            range_iterator operator ++(int) {
                return {val++};
            }

            range_iterator operator --(int) {
                return {val--};
            }

            range_iterator & operator +=(T diff) {
                val += diff;
                return *this;
            }

            range_iterator & operator -=(T diff) {
                val -= diff;
                return *this;
            }

            range_iterator operator +(T diff) {
                return {val + diff};
            }

            range_iterator operator -(T diff) {
                return {val - diff};
            }

            T operator [](T diff) const {
                return val + diff;
            }

        protected:
            T val;
        };

        template <class T>
        struct common_range_storage
        {
            T min, max;
        };

        template <class T, class Storage = common_range_storage<T>>
        struct range : Storage
        {
            using iterator = range_iterator<T>;
            using const_iterator = range_iterator<T>;

            using reverse_iterator = std::reverse_iterator<iterator>;
            using const_reverse_iterator = std::reverse_iterator<const_iterator>;

            constexpr range() noexcept : Storage{0, 0} {}

            constexpr range(T min, T max) noexcept : Storage{min, max} {}

            template <class U, class S>
            constexpr range(const range<U, S> & range) noexcept : Storage{range.min, range.max} {}

            constexpr bool empty() const noexcept {
                return this->max == this->min;
            }

            constexpr T size() const noexcept {
                return this->max - this->min;
            }

            constexpr T center() const noexcept {
                return math::avg(this->min, this->max);
            }

            constexpr void assign(T min, T max) noexcept {
                this->min = min;
                this->max = max;
            }

            template <class U = T, class S = Storage>
            constexpr void assign(const range<U, S> & range) noexcept {
                this->min = range.min;
                this->max = range.max;
            }

            constexpr void resize(T value) noexcept {
                this->max = this->min + value;
            }

            template <class U = T, class S = Storage>
            constexpr void intersect(const range<U, S> & range) noexcept {
                if (range.min > this->min) {
                    this->min = range.min;
                }

                if (range.max < this->max) {
                    this->max = range.max;
                }

                if (size() < 0) {
                    this->max = this->min;
                }
            }

            template <class U = T, class S = Storage>
            constexpr void include(const range<U, S> & range) noexcept {
                if (range.min < this->min) {
                    this->min = range.min;
                }

                if (range.max > this->max) {
                    this->max = range.max;
                }
            }

            template <class U = T, class S = Storage>
            constexpr int compare(const range<U, S> & range) const noexcept {
                return (this->min == range.min && this->max == range.max) ? 0 : icomp(size(), range.size());
            }

            constexpr int compare(T value) const noexcept {
                return (value > this->max) ? 1 : (value < this->min) ? -1 : 0;
            }

            template <class U, class S>
            constexpr bool contains(const range<U, S> & range) const noexcept {
                return (compare(range.min) & compare(range.max)) == 0;
            }

            constexpr bool contains(T value) const noexcept {
                return between(value, this->min, this->max);
            }

            template <class U, class S>
            constexpr bool intersects(const range<U, S> & range) const noexcept {
                return range.max >= this->min && range.min <= this->max;
            }

            template <class U, class S>
            constexpr range & operator =(const range<U, S> & range) noexcept {
                assign(range);
                return *this;
            }

            template <class U, class S>
            constexpr range & operator &=(const range <U, S> & range) noexcept {
                intersect(range);
                return *this;
            }

            template <class U, class S>
            constexpr range & operator |=(const range <U, S> & range) noexcept {
                include(range);
                return *this;
            }

            template <class U, class S>
            constexpr bool operator == (const range <U, S> & range) noexcept {
                return compare(range) == 0;
            }

            template <class U, class S>
            constexpr bool operator !=(const range <U, S> & range) noexcept {
                return compare(range) != 0;
            }

            template <class U, class S>
            constexpr bool operator >=(const range <U, S> & range) noexcept {
                return compare(range) >= 0;
            }

            template <class U, class S>
            constexpr bool operator <=(const range <U, S> & range) noexcept {
                return compare(range) <= 0;
            }

            template <class U, class S>
            constexpr bool operator >(const range <U, S> & range) noexcept {
                return compare(range) > 0;
            }

            template <class U, class S>
            constexpr bool operator <(const range <U, S> & range) noexcept {
                return compare(range) < 0;
            }

            template <class U, class S>
            constexpr range operator &(const range <U, S> & range) noexcept {
                return range(*this) &= range;
            }

            template <class U, class S>
            constexpr range operator |(const range <U, S> & range) noexcept {
                return range(*this) |= range;
            }

            template <convertible_to<T> U>
            constexpr range & operator += (U delta) noexcept {
                this->min += static_cast<T>(delta);
                this->max += static_cast<T>(delta);

                return *this;
            }

            template <convertible_to<T> U>
            constexpr range & operator -= (U delta) noexcept {
                this->min -= static_cast<T>(delta);
                this->max -= static_cast<T>(delta);

                return *this;
            }

            template <convertible_to<T> U>
            constexpr range & operator *= (U scale) noexcept {
                this->min = static_cast<T>(this->min * scale);
                this->max = static_cast<T>(this->max * scale);

                return *this;
            }

            template <convertible_to<T> U>
            constexpr range & operator /= (U scale) noexcept {
                this->min = static_cast<T>(this->min / scale);
                this->max = static_cast<T>(this->max / scale);

                return *this;
            }

            template <convertible_to<T> U>
            constexpr range operator + (U delta) const noexcept {
                return {static_cast<T>(this->min + delta), static_cast<T>(this->max + delta)};
            }

            template <convertible_to<T> U>
            constexpr range operator - (U delta) const noexcept {
                return {static_cast<T>(this->min - delta), static_cast<T>(this->max - delta)};
            }

            template <convertible_to<T> U>
            constexpr range operator * (U scale) const noexcept {
                return {static_cast<T>(this->min * scale), static_cast<T>(this->max * scale)};
            }

            template <convertible_to<T> U>
            constexpr range operator / (U scale) const noexcept {
                return {static_cast<T>(this->min / scale), static_cast<T>(this->max / scale)};
            }

            constexpr iterator begin() noexcept {
                return {this->min};
            }

            constexpr const_iterator begin() const noexcept {
                return {this->min};
            }

            constexpr iterator end() noexcept {
                return {this->max};
            }

            constexpr const_iterator end() const noexcept {
                return {this->max};
            }

            constexpr reverse_iterator rbegin() noexcept {
                return end();
            }

            constexpr const_reverse_iterator rbegin() const noexcept {
                return end();
            }

            constexpr reverse_iterator rend() noexcept {
                return begin();
            }

            constexpr const_reverse_iterator rend() const noexcept {
                return begin();
            }

            constexpr const_iterator cbegin() const noexcept {
                return begin();
            }

            constexpr const_iterator cend() const noexcept {
                return end();
            }

            constexpr const_reverse_iterator crbegin() const noexcept {
                return rbegin();
            }

            constexpr const_reverse_iterator crend() const noexcept {
                return rend();
            }
        };

        using int_range = range<int>;
        using uint_range = range<unsigned int>;
        using long_range = range<long>;
        using float_range = range<float>;

        template <class T, class U, class Storage, class R = std::common_type_t<T, U>>
        constexpr range<R> mod(const range<T, Storage> & a, U b) noexcept {
            return {math::mod(a.min, b), math::mod(a.max, b)};
        }

        template <class T, class U, class Storage, class R = std::common_type_t<T, U>>
        constexpr T wrap(T a, const range<U, Storage> & b) noexcept {
            return b.min + math::mod(a - b.min, b.size());
        }

        template <class T, class Storage, class U, class R = std::common_type_t<T, U>>
        constexpr range<R> floor_div(const range<T, Storage> & a, U b) {
            return {math::floor_div(a.min, b), math::floor_div(a.max, b)};
        }

        template <class T, class U, class Storage, class R = std::common_type_t<T, U>>
        constexpr range<R> floor_mod(const range<T, Storage> & a, U b) noexcept {
            return {math::floor_mod(a.min, b), math::floor_mod(a.max, b)};
        }

        template <class T, class U, class Storage, class R = std::common_type_t<T, U>>
        constexpr T floor_wrap(T a, const range<U, Storage> & b) noexcept {
            return b.min + math::floor_mod(a - b.min, b.size());
        }

        template <class T, class Storage>
        constexpr range<T> avg(const range<T, Storage> & a, const range<T, Storage> & b) noexcept {
            return T(0.5) * (a + b);
        }

        template <class T, class Storage>
        constexpr range<T> min(const range<T, Storage> & a, const range<T, Storage> & b) noexcept {
            return {math::min(a.min, b.min), math::min(a.max, b.max)};
        }

        template <class T, class Storage>
        constexpr range<T> max(const range<T, Storage> & a, const range<T, Storage> & b) noexcept {
            return {math::max(a.min, b.min), math::max(a.max, b.max)};
        }

        template <class T, class Storage>
        constexpr range<T> clamp(const range<T, Storage> & r, const range<T, Storage> & low, const range<T, Storage> & high) noexcept {
            return {math::clamp(r.min, low.min, high.min), math::clamp(r.max, low.max, high.max)};
        }

        template <class T, class Storage>
        constexpr range<T> clamp(const range<T, Storage> & r, non_deduced<T> low, non_deduced<T> high) noexcept {
            return {math::clamp(r.min, low, high), math::clamp(r.max, low, high)};
        }

        template <class T, class Storage>
        constexpr range<T> invert(const range<T, Storage> & r) noexcept {
            return {math::invert(r.min), math::invert(r.max)};
        }

        template <class T, class Storage>
        constexpr range<T> trunc(const range<T, Storage> & r) noexcept {
            return {math::trunc(r.min), math::trunc(r.max)};
        }

        template <class T, class Storage>
        constexpr range<T> floor(const range<T, Storage> & r) noexcept {
            return {math::floor(r.min), math::floor(r.max)};
        }

        template <class T, class Storage>
        constexpr range<T> ceil(const range<T, Storage> & r) noexcept {
            return {math::ceil(r.min), math::ceil(r.max)};
        }

        template <class T, class Storage>
        constexpr range<T> round(const range<T, Storage> & r) noexcept {
            return {math::round(r.min), math::round(r.max)};
        }

        template <class R, class T, class Storage, class U> requires (!std::is_same_v<R, std::common_type_t<T, U>>)
        constexpr range<R> floor_div(const range<T, Storage> & a, U b) {
            return {math::floor_div(a.min, b), math::floor_div(a.max, b)};
        }

        template <class R, class T, class Storage> requires (!std::is_same_v<R, T>)
        constexpr range<R> trunc(const range<T, Storage> & r) noexcept {
            return {static_cast<T>(math::trunc(r.min)), static_cast<T>(math::trunc(r.max))};
        }

        template <class R, class T, class Storage> requires (!std::is_same_v<R, T>)
        constexpr range<R> floor(const range<T, Storage> & r) noexcept {
            return {static_cast<T>(math::floor(r.min)), static_cast<T>(math::floor(r.max))};
        }

        template <class R, class T, class Storage> requires (!std::is_same_v<R, T>)
        constexpr range<R> ceil(const range<T, Storage> & r) noexcept {
            return {static_cast<T>(math::ceil(r.min)), static_cast<T>(math::ceil(r.max))};
        }

        template <class R, class T, class Storage> requires (!std::is_same_v<R, T>)
        constexpr range<R> round(const range<T, Storage> & r) noexcept {
            return {static_cast<T>(math::round(r.min)), static_cast<T>(math::round(r.max))};
        }
    }

    template <class T>
    math::range<T> make_range(T min, T max) {
        return {min, max};
    }
}
