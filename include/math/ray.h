//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <math/vector.h>

//---------------------------------------------------------------------------

namespace asd::math
{
    template <class OriginType, class DirectionType = OriginType>
    struct generic_ray
    {
        using origin_type = OriginType;
        using direction_type = DirectionType;

        constexpr generic_ray() noexcept  = default;

        template <std::convertible_to<origin_type> O, std::convertible_to<direction_type> D>
        constexpr generic_ray(const O & origin, const D & direction) noexcept :
            origin(origin), direction(direction) {}

        constexpr generic_ray & operator = (const generic_ray &) noexcept  = default;

        origin_type origin;
        direction_type direction;
    };

//---------------------------------------------------------------------------

    template <class T, class VectorStorage = default_vector_storage<T>>
    using ray = generic_ray<math::vector<T, VectorStorage>>;

    template <class T>
    using ray2 = generic_ray<math::point2<T>>;

    template <class T>
    using ray3 = generic_ray<math::point3<T>>;

    template <class T>
    using ray_xy = generic_ray<math::point_xy<T>>;

    template <class T>
    using ray_xz = generic_ray<math::point_xz<T>>;

    template <class T>
    using ray_xyz = generic_ray<math::point_xyz<T>>;

//---------------------------------------------------------------------------

    template <class A, class B, class E = float>
    constexpr bool fuzzy_equal(const generic_ray<A> & a, const generic_ray<B> & b, E eps = math::eps) noexcept {
        return math::fuzzy_equal(a.origin, b.origin, eps) &&
            math::fuzzy_equal(a.direction, b.direction, eps);
    }
}
