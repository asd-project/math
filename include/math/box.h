//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <math/vector.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace math
    {
        template <class T>
        struct box
        {
            vector<T> min, max;
        };

        template <class T, class S>
        bool contains(const box<T> & b, const vector<T, S> & v) {
            return v >= b.min && v < b.max;
        }
    }
}
